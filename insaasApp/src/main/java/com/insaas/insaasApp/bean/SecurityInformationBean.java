/**
 * @author BrainWave Consulting
 * @since Feb 2, 2019
 * 
 */
package com.insaas.insaasApp.bean;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.springframework.stereotype.Component;

import com.insaas.insaasApp.pojo.SecurityContext;


/**
 * Represents an ApplicationBeans will handles User requests
 * @author BrainWave Consulting
 * @since Feb 2, 2019
 */
@Component
public class SecurityInformationBean {
	
	private static Map<String, SecurityContext> userSecurityContext = new HashMap<String, SecurityContext>(10);
	

	/**
	 * @return the userSecurityContext
	 */
	
	public void addNewSecuirtyContext(String accessToken, SecurityContext securityContext) {
		userSecurityContext.put(accessToken, securityContext);
	}

	/**
	 * @param userSecurityContext the userSecurityContext to set
	 */
	public SecurityContext getSecuirtyContext(String accessToken) {
		System.out.println("userSecurityContext :: "+ userSecurityContext);
		return userSecurityContext.get(accessToken);
	}
	public void removeSecurityContext(String accessToken) {
		userSecurityContext.remove(accessToken);
	}
	
	/**
	 * This method will get all security context object keys.
	 * @author BrainWave Consulting
	 * @since Feb 18, 2019
	 * @return Set<String>
	 *
	 */
	public Set<String> getSecurityContextKeys() {
		return userSecurityContext.keySet();
	}
}
