/**
 * 
 */
package com.insaas.insaasApp.exception;

/**
 * @author Administrator
 *
 */
public class SolrException extends Exception {

	public SolrException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SolrException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public SolrException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public SolrException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public SolrException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
