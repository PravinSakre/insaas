//package com.insaas.insaasApp.model;
//
//import org.apache.commons.lang.time.StopWatch;
//
///**
// * User: Surya
// * Date: 2/2/15
// * Time: 2:51 PM
// */
//public class IndexingMetrics {
//    public static final IndexingMetrics SINGLETON = new IndexingMetrics();
//
//    private long filesToIndex = 0; //all files to be indexed
//    private long filesIndexed = 0; //all files already indexed
//    private long filesInQueue = 0; //all files that are waiting in queue to be indexed
//    private long filesProcessing = 0;  //all files being indexed at this time
//    private long filesIgnored = 0; //all files that are ignored due to config properties
//    private long foldersIgnored = 0; //all folders that are ignored due to config properties
//    private StopWatch stopWatch = new StopWatch(); //elapsed time
//
//    private final String LOCK = "lock";
//
//    public long incrementFilesToIndex() {
//        synchronized (LOCK) {
//            filesToIndex++;
//            return filesToIndex;
//        }
//    }
//
//    public long decrementFilesToIndex() {
//        synchronized (LOCK) {
//            filesToIndex--;
//            return filesToIndex;
//        }
//    }
//
//    public long incrementFilesIndexed() {
//        synchronized (LOCK) {
//            filesIndexed++;
//            return filesIndexed;
//        }
//    }
//
//    public long incrementFilesInQueue() {
//        synchronized (LOCK) {
//            filesInQueue++;
//            return filesInQueue;
//        }
//    }
//
//    public long decrementFilesInQueue() {
//        synchronized (LOCK) {
//            filesInQueue--;
//            return filesInQueue;
//        }
//    }
//
//    public long incrementFilesProcessing() {
//        synchronized (LOCK) {
//            filesProcessing++;
//            return filesProcessing;
//        }
//    }
//
//    public long decrementFilesProcessing() {
//        synchronized (LOCK) {
//            filesProcessing--;
//            return filesProcessing;
//        }
//    }
//
//    public long incrementFilesIgnored() {
//        synchronized (LOCK) {
//            filesIgnored++;
//            return filesIgnored;
//        }
//    }
//
//    public long incrementFoldersIgnored() {
//        synchronized (LOCK) {
//            foldersIgnored++;
//            return foldersIgnored;
//        }
//    }
//
//    public long getFilesToIndex() {
//        return filesToIndex;
//    }
//
//    public long getFilesIndexed() {
//        return filesIndexed;
//    }
//
//    public long getFilesInQueue() {
//        return filesInQueue;
//    }
//
//    public long getFilesProcessing() {
//        return filesProcessing;
//    }
//
//    public void setFilesProcessing(long filesProcessing) {
//        this.filesProcessing = filesProcessing;
//    }
//
//    public long getFilesIgnored() {
//        return filesIgnored;
//    }
//
//    public void setFilesIgnored(long filesIgnored) {
//        this.filesIgnored = filesIgnored;
//    }
//
//    public long getFoldersIgnored() {
//        return foldersIgnored;
//    }
//
//    public void setFoldersIgnored(long foldersIgnored) {
//        this.foldersIgnored = foldersIgnored;
//    }
//
//    public String getElapsedTime() {
//        return stopWatch.toString();
//    }
//
//    public void  setUpIndexingMatrix(){
//        this.filesToIndex = 0;
//        this.filesIndexed = 0;
//        this.filesInQueue = 0;
//        this.filesProcessing = 0;
//        this.filesIgnored = 0;
//        this.foldersIgnored = 0;
//        this.stopWatch = new StopWatch();
//        this.stopWatch.start();
//    }
//
//    @Override
//    public String toString() {
//        String separator = "; ";
//        StringBuilder sb = new StringBuilder();
//        sb.append("fileToIndex=").append(getFilesToIndex()).append(separator);
//        sb.append("filesProcessing=").append(getFilesProcessing()).append(separator);
//        sb.append("filesInQueue=").append(getFilesInQueue()).append(separator);
//        sb.append("filesIgnored=").append(getFilesIgnored()).append(separator);
//        sb.append("foldersIgnored=").append(getFoldersIgnored()).append(separator);
//        sb.append("filesIndexed=").append(getFilesIndexed()).append(separator);
//        sb.append("stopWatch=").append(getElapsedTime()).append(separator);
//        return sb.toString();
//    }
//}
