//package com.insaas.insaasApp.model;
//
//import org.apache.commons.lang.StringUtils;
//
//import java.util.HashMap;
//import java.util.Map;
//
///**
// * User: Surya
// * Date: 1/17/15
// * Time: 11:17 PM
// * To change this template use File | Settings | File Templates.
// */
//public class SolrResult {
//    private String id;
//    private String fileName;
//    private String fileNameHash;
//    private String filePath;
//    private String fullFileName;
//    private String extension;
//    private String fileType;
//    private String lastModifiedDate;
//    private String lastModifiedBy;
//    private String createdDate;
//    private String createdBy;
//    private String content;
//    private String category1_s;
//    private String category2_s;
//    private String category3_s;
//
//    public String getAttributesAccessedDate() {
//        return attributesAccessedDate;
//    }
//
//    public void setAttributesAccessedDate(String attributesAccessedDate) {
//        this.attributesAccessedDate = attributesAccessedDate;
//    }
//
//    private String attributesAccessedDate;
//    private String hash_id;
//
//    public String getDerivedFileType() {
//        return derivedFileType;
//    }
//
//    public void setDerivedFileType(String derivedFileType) {
//        this.derivedFileType = derivedFileType;
//    }
//
//    private String derivedFileType;
//    private float score, size;
//    private Map<String, Integer> termCounts = new HashMap<String, Integer>();
//
//    public String getId() {
//        return id;
//    }
//
//    public void setId(String id) {
//        this.id = id;
//    }
//
//    public String getLastModifiedDate() {
//        return lastModifiedDate;
//    }
//
//    public void setLastModifiedDate(String lastModifiedDate) {
//        this.lastModifiedDate = lastModifiedDate;
//    }
//
//    public String getLastModifiedBy() {
//        return lastModifiedBy;
//    }
//
//    public void setLastModifiedBy(String lastModifiedBy) {
//        this.lastModifiedBy = lastModifiedBy;
//    }
//
//    public String getCreatedDate() {
//        return createdDate;
//    }
//
//    public void setCreatedDate(String createdDate) {
//        this.createdDate = createdDate;
//    }
//
//    public String getCreatedBy() {
//        return createdBy;
//    }
//
//    public void setCreatedBy(String createdBy) {
//        this.createdBy = createdBy;
//    }
//
//    public String getFileName() {
//        return fileName;
//    }
//
//    public void setFileName(String fileName) {
//        this.fileName = fileName;
//    }
//
//    public String getFilePath() {
//        return filePath;
//    }
//
//    public void setFilePath(String filePath) {
//        this.filePath = filePath;
//    }
//
//    public String getFullFileName() {
//        return fullFileName;
//    }
//
//    public void setFullFileName(String fullFileName) {
//        this.fullFileName = fullFileName;
//    }
//
//    public String getExtension() {
//        return extension;
//    }
//
//    public void setExtension(String extension) {
//        this.extension = extension;
//    }
//
//    public String getFileType() {
//        return fileType;
//    }
//
//    public void setFileType(String fileType) {
//        this.fileType = fileType;
//    }
//
//    public float getScore() {
//        return score;
//    }
//
//    public void setScore(float score) {
//        this.score = score;
//    }
//
//    public float getSize() {
//        return size;
//    }
//
//    public void setSize(float size) {
//        this.size = size;
//    }
//
//    public String getHash_id() {
//        return hash_id;
//    }
//
//    public void setHash_id(String hash_id) {
//        this.hash_id = hash_id;
//    }
//
//    public String getFileNameHash() {
//        return fileNameHash;
//    }
//
//    public void setFileNameHash(String fileNameHash) {
//        this.fileNameHash = fileNameHash;
//    }
//    public String getContent() {
//        return content;
//    }
//
//    public void setContent(String content) {
//        this.content = content;
//    }
//    public Map<String, Integer> getTermCounts() {
//        return termCounts;
//    }
//
//    public Integer getTermCount(String term) {
//        return termCounts.get(term);
//    }
//
//    public void setTermCounts(Map<String, Integer> termCounts) {
//        this.termCounts = termCounts;
//    }
//
//    public void addTermCount(String term, Integer count) {
//        this.termCounts.put(term, count);
//    }
//
//    public String getCategory1_s() {
//		if(null == category1_s) {
//			return "";
//		}
//        return category1_s;
//    }
//
//    public void setCategory1_s(String category1_s) {
//        this.category1_s = category1_s;
//    }
//
//    public String getCategory2_s() {
//        if(null == category2_s) {
//			return "";
//		}
//        return category2_s;
//    }
//
//    public void setCategory2_s(String category2_s) {
//        this.category2_s = category2_s;
//    }
//
//    public String getCategory3_s() {
//        if(null == category3_s) {
//			return "";
//		}
//        return category3_s;
//    }
//
//    public void setCategory3_s(String category3_s) {
//        this.category3_s = category3_s;
//    }
//
//}
