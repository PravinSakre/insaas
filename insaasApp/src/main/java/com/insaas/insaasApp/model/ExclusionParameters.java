//package com.insaas.insaasApp.model;
//
//import org.apache.commons.collections.CollectionUtils;
//
//import java.util.ArrayList;
//import java.util.List;
//
///**
// * Created with IntelliJ IDEA.
// * User: Suryapc
// * Date: 4/9/15
// * Time: 7:34 PM
// * To change this template use File | Settings | File Templates.
// */
//public class ExclusionParameters {
//    private final List<String> filesToExclude;
//    private final List<String> foldersToExclude;
//    private final int maxFileSize;
//    private final int maxFileAge;
//
//    public List<String> getFilesToExclude() {
//        return filesToExclude;
//    }
//
//    public List<String> getFoldersToExclude() {
//        return foldersToExclude;
//    }
//
//    public int getMaxFileSize() {
//        return maxFileSize;
//    }
//
//    public int getMaxFileAge() {
//        return maxFileAge;
//    }
//
//    public ExclusionParameters(int maxFileSize, int maxFileAge) {
//        this.filesToExclude = new ArrayList<String>();
//        this.foldersToExclude = new ArrayList<String>();
//        this.maxFileSize = maxFileSize;
//        this.maxFileAge = maxFileAge;
//    }
//
//    public void addFileExclusionRules(String[] filesToExclude) {
//        CollectionUtils.addAll(this.filesToExclude, filesToExclude);
//    }
//
//    public void addFolderExclusionRules(String[] foldersToExclude) {
//        CollectionUtils.addAll(this.foldersToExclude, foldersToExclude);
//    }
//}
