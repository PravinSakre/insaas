//package com.insaas.insaasApp.model;
//
///**
// * User: Surya
// * Date: 1/20/15
// * Time: 1:16 PM
// * To change this template use File | Settings | File Templates.
// */
//public class SolrMetrics {
//    private final long elapsedTime;
//    private final int queryTime;
//    private final int resultCount;
//    private final long totalCount;
//    private static long totalFileIndexCount;
//
//    public float getElapsedTime() {
//        return elapsedTime;
//    }
//
//    public float getQueryTime() {
//        return queryTime;
//    }
//
//    public int getResultCount() {
//        return resultCount;
//    }
//
//    public long getTotalCount() {
//        return totalCount;
//    }
//
//    public static long getTotalFileIndexCount(){
//        return totalFileIndexCount;
//    }
//
//    public static void incrementTotalFileIndexCount(){
//         totalFileIndexCount++;
//    }
//
//    public SolrMetrics(long elapsedTime, int queryTime, int resultCount, long totalCount) {
//        this.elapsedTime = elapsedTime;
//        this.queryTime = queryTime;
//        this.resultCount = resultCount;
//        this.totalCount = totalCount;
//    }
//}
