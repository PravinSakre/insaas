//package com.insaas.insaasApp.model;
//
//
//import org.apache.commons.io.FileUtils;
//import org.apache.log4j.Logger;
//
//import com.insaas.insaasApp.helper.PropertiesHelper;
//
//import java.io.File;
//import java.io.IOException;
//import java.util.concurrent.*;
//
///**
// * Created with IntelliJ IDEA.
// * User: Nitin
// * Date: 3/3/15
// * Time: 3:43 PM
// * To change this template use File | Settings | File Templates.
// */
//public class SolrBackUp implements Callable<String> {
//    static Logger log = Logger.getLogger(SolrBackUp.class);
//    private final String coreName;
//    public SolrBackUp(String coreName) {
//        this.coreName = coreName;
//    }
//    public static String startBackUp(String coreName){
//        //thread
//        Callable<String> callable = new SolrBackUp(coreName);
//        ExecutorService executor = Executors.newSingleThreadExecutor();
//        Future<String> future = executor.submit(callable);
//        try {
//            return future.get();
//        } catch (InterruptedException e) {
//            log.error("Failed to backup data  "+e.getMessage());
//            throw new RuntimeException("Failed to backup data "+ e.getMessage(), e);
//        } catch (ExecutionException e) {
//            log.error("Failed to backup data  "+e.getMessage());
//            throw new RuntimeException("Failed to backup data "+ e.getMessage(), e);
//        }finally {
//            //shut down the executor service now
//            executor.shutdown();
//        }
//    }
//
//    public String call() throws Exception {
//        File srcPath = new File(PropertiesHelper.getIndexCoreFilePath(coreName));
//        File destPath = new File(PropertiesHelper.getBackupFolder(coreName));
//        FileUtils.copyDirectory(srcPath, destPath);
//        log.info("Backedup folder " + srcPath.toString() + " to " + destPath.toString());
//        return "done back up";  //To change body of implemented methods use File | Settings | File Templates.
//    }
//}
