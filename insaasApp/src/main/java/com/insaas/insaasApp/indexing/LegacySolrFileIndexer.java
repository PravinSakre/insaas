//package com.insaas.insaasApp.indexing;
//
//
//import org.apache.log4j.Logger;
//import org.apache.solr.client.solrj.SolrServerException;
//import org.apache.solr.client.solrj.request.ContentStreamUpdateRequest;
//import org.apache.tika.config.TikaConfig;
//import org.apache.tika.exception.TikaException;
//import org.apache.tika.io.TikaInputStream;
//import org.apache.tika.metadata.Metadata;
//
//import com.insaas.insaasApp.dao.LogChannel;
//import com.insaas.insaasApp.dao.SolrChannel;
//import com.insaas.insaasApp.helper.FriendlyMimeTypeMap;
//import com.insaas.insaasApp.helper.UidHelper;
//import com.insaas.insaasApp.model.ExclusionParameters;
//
//import org.apache.solr.client.solrj.SolrClient;
//
//import java.io.File;
//import java.io.IOException;
//import java.nio.file.Files;
//import java.nio.file.attribute.BasicFileAttributes;
//import java.util.Date;
///**
// * User: Surya
// * Date: 2/18/15
// * Time: 5:54 PM
// * To change this template use File | Settings | File Templates.
// */
//public class LegacySolrFileIndexer extends SolrFileIndexer {
//    private static final Logger LOG = Logger.getLogger(LegacySolrFileIndexer.class);
//
//    public LegacySolrFileIndexer(File fileToIndex, long waitTimeToComplete, String coreName, ExclusionParameters exclusionParameters) {
//        super(fileToIndex, waitTimeToComplete,coreName, exclusionParameters);
//    }
//
//    @Override
//    protected void indexFile(File file,String coreName) {
//        try {
//            indexFileImpl(file,coreName);
//        } catch (TikaException e) {
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
//        } catch (IOException e) {
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
//        } catch (SolrServerException e) {
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
//        }
//
//    }
//
//    private void indexFileImpl(File file,String coreName) throws TikaException, IOException, SolrServerException {
//        //not a zip
//        SolrClient localSolr = SolrChannel.getSolrServer(coreName);
//        TikaConfig tika = new TikaConfig();
//        Metadata metadata = new Metadata();
//        metadata.set(Metadata.RESOURCE_NAME_KEY, file.toString());
//        String mimetype = tika.getDetector().detect(
//                TikaInputStream.get(file), metadata).getSubtype();
//        /*if(GetPropertyValues.isFileInExclusionList(file.getName())){
//            LOG.info("Excluding " + file + " of mime " + mimetype);
//            return;
//        }*/
//        LOG.info("indexing " + file + " of mime " + mimetype);
//        ContentStreamUpdateRequest req = new ContentStreamUpdateRequest("/update/extract");
//        submitFile(file, req);
//        req.setParam("literal.id", file.getAbsolutePath());
//        req.setParam("literal.hash_id", UidHelper.getHashIdFromString(file.getAbsolutePath()));
//        req.setParam("literal.filename", file.getName());
//        req.setParam("literal.hash_filename", UidHelper.getHashIdFromString(file.getName()));
//        req.setParam("literal.filename_length", String.valueOf(file.getName().length()));
//        req.setParam("literal.filetype", mimetype);
//        req.setParam("literal.objectid", file.getAbsolutePath().concat(file.getName()));
////        req.setParam("literal.type","XLS");
//        req.setParam("literal.filepath", file.getParent());
//        //req.setParam("literal.filesize",String.valueOf(file.length()));
//
//        // dates for testing
//        req.setParam("literal.attributes_created_time",FileValidator.getCreationTime(file).toString());
//        req.setParam("literal.attributes_accessed_time",FileValidator.getLastAccessedTime(file).toString());
//        req.setParam("literal.attributes_modified_time",FileValidator.getLastModifiedTime(file).toString());
//
//        Object temp= Files.readAttributes(file.toPath(), BasicFileAttributes.class);
//        req.setParam("literal.filesize",String.valueOf(((BasicFileAttributes) temp).size()));
//        req.setParam("literal.created_date",String.valueOf(((BasicFileAttributes) temp).creationTime()));
//
//        req.setParam("literal.derived_file_type", FriendlyMimeTypeMap.getFriendlyMimeValue(mimetype));
//
//        //todo - how to handle a .c and .pdf file when one has 2 dates and the other has only 1 date
////        req.setParam("literal.last_modified", String.valueOf(new Date(entries.lastModified())));
////        req.setParam("literal.last_modified", String.valueOf(new Date(file.lastModified())));
//        try{
//            localSolr.request(req);
//        }catch(Exception e){
//            LOG.error("error indexing file " + file, e);
//            LogChannel.recordInCsv(new Date(),"INDEXING_ERROR", file.toString(), file.getPath(), e.getMessage(),"");
//        }
//        localSolr.commit();
//        SolrChannel.shutdownSolrServer(localSolr);
//    }
//
//    protected void submitFile(File file, ContentStreamUpdateRequest req) throws IOException {
//        req.addFile(file, null);
//    }
//
//}
