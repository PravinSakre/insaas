//package com.insaas.insaasApp.indexing;
//
//
//import org.apache.commons.lang.StringUtils;
//import org.apache.log4j.Logger;
//import org.apache.solr.client.solrj.SolrClient;
//import org.apache.solr.client.solrj.SolrServerException;
//import org.apache.solr.common.SolrInputDocument;
//import org.apache.tika.exception.TikaException;
//import org.apache.tika.io.TikaInputStream;
//import org.apache.tika.metadata.DublinCore;
//import org.apache.tika.metadata.Metadata;
//import org.apache.tika.parser.AutoDetectParser;
//import org.apache.tika.parser.ParseContext;
//import org.apache.tika.parser.Parser;
//import org.apache.tika.sax.BodyContentHandler;
//import org.xml.sax.ContentHandler;
//import org.xml.sax.SAXException;
//
//import com.insaas.insaasApp.dao.LogChannel;
//import com.insaas.insaasApp.dao.SolrChannel;
//import com.insaas.insaasApp.helper.FriendlyMimeTypeMap;
//import com.insaas.insaasApp.helper.PropertiesHelper;
//import com.insaas.insaasApp.helper.UidHelper;
//import com.insaas.insaasApp.helper.ViewsHelper;
//import com.insaas.insaasApp.model.ExclusionParameters;
//
//import java.io.File;
//import java.io.IOException;
//import java.io.InputStream;
//import java.nio.file.Files;
//import java.nio.file.attribute.BasicFileAttributes;
//import java.util.Date;
//import java.util.zip.ZipEntry;
//
///**
// * Created with IntelliJ IDEA.
// * User: Suryapc
// * Date: 4/16/15
// * Time: 4:20 PM
// * To change this template use File | Settings | File Templates.
// */
//public class ZipFileIndexer  extends SolrFileIndexer {
//    private static final Logger LOG = Logger.getLogger(ZipFileIndexer.class);
//
//    private File originalZipFile;
//    private ZipEntry unzippedFile;
//
//    public ZipFileIndexer(File fileToIndex, long waitTimeToComplete,String coreName, ExclusionParameters exclusionParameters, ZipEntry unzippedFile) {
//        super(fileToIndex, waitTimeToComplete,coreName, exclusionParameters);
//        this.originalZipFile = fileToIndex;
//        this.unzippedFile = unzippedFile;
//    }
//
//    @Override
//    protected void indexFile(File file,String coreName) {
//        try {
//            indexFileImpl(file,coreName);
//        } catch (IOException e) {
//            LOG.error("Exception indexing " +file.getAbsolutePath(), e);
//        } catch (SAXException e) {
//            LOG.error("Exception indexing " +file.getAbsolutePath(), e);
//        } catch (TikaException e) {
//            LOG.error("Exception indexing " +file.getAbsolutePath(), e);
//        } catch (SolrServerException e) {
//            LOG.error("Exception indexing " +file.getAbsolutePath(), e);
//        }
//    }
//
//    private void indexFileImpl(File file,String coreName) throws IOException, SAXException, TikaException, SolrServerException {
//        //To change body of created methods use File | Settings | File Templates.
//        String fileToIndex = unzippedFile.getName().replace("/","\\");
//        String filePath = PropertiesHelper.getFileWithinZipInZipFolder(fileToIndex);
//
//        File indexFile = new File(filePath);
//        InputStream input = TikaInputStream.get(indexFile);
//        //use Apache Tika to convert documents in different formats to plain text
//        ContentHandler textHandler = new BodyContentHandler(100*1024*1024);
//        Metadata meta = new Metadata();
//        Parser parser = new AutoDetectParser(); //handles documents in different formats:
//        ParseContext context = new ParseContext();
//        parser.parse(input, textHandler, meta, context); //convert to plain text
//        indexDocument(textHandler,meta,coreName,indexFile);
//        input.close();
//    }
//
//    private void indexDocument(ContentHandler textHandler,Metadata meta ,String coreName,File file) throws IOException, SolrServerException {
//
//        String doctitle = meta.get(DublinCore.TITLE);
//        String doccreator = meta.get(DublinCore.CREATOR);
//        String lastModifiedBy = meta.get("Last-Author");
//
//        //content
//        String doccontent = textHandler.toString();
//
//        //mimtype
//        String mimtype=meta.get("Content-Type");
//
//        SolrInputDocument doc = new SolrInputDocument();
//        LOG.info("indexing " + file + " of mime " + mimtype);
//
//        doc.addField("id", getRelativeFullFilePath(file, originalZipFile));
//
//        doc.addField("filename",getRelativeFileName(file, originalZipFile));
//        doc.addField("hash_filename", UidHelper.getHashIdFromString(getRelativeFileName(file, originalZipFile)));
//        doc.addField("filename_length", String.valueOf(getRelativeFileName(file, originalZipFile)).length());
//
//        doc.addField("hash_id", UidHelper.getHashIdFromString(originalZipFile.toString()));
//
//        doc.addField("filetype", mimtype);
//        doc.addField("filepath", originalZipFile.getParent());
//        doc.addField("hash_filepath", UidHelper.getHashIdFromString(originalZipFile.getParent()));
//        doc.addField("derived_file_type", FriendlyMimeTypeMap.getFriendlyMimeValue(mimtype));
//
//        // dates for testing
//        doc.addField("modified_by", lastModifiedBy);
//        doc.addField("attributes_created_time",FileValidator.getCreationTime(originalZipFile));
//        doc.addField("attributes_accessed_time",FileValidator.getLastAccessedTime(originalZipFile));
//        doc.addField("attributes_modified_time",FileValidator.getLastModifiedTime(originalZipFile));
//
//        Object temp= Files.readAttributes(file.toPath(), BasicFileAttributes.class);
//        doc.addField("filesize",String.valueOf(((BasicFileAttributes) temp).size()));
//        doc.addField("created_date",String.valueOf(((BasicFileAttributes) temp).creationTime()));
//
//        doc.addField("last_modified",FileValidator.getLastModifiedTime(originalZipFile));
//
//        //map metadata fields to default schema
//        //location: path\solr-4.7.2\example\solr\collection1\conf\schema.xml
//
//        //Dublin Core
//        //thought: schema could be modified to use Dublin Core
//        doc.addField("title", doctitle);
//        doc.addField("author", doccreator);
//        doc.addField("extension", ViewsHelper.getFileExtension(file.getName()));
//
//        //other metadata
//
//        //content (and text)
//        //per schema, the content field is not indexed by default, used for returning and highlighting document content
//        //the schema "copyField" command automatically copies this to the "text" field which is indexed
//        doc.addField("content", doccontent);
//
//        //indexing
//        //when a field is indexed, like "text", Solr will handle tokenization, stemming, removal of stopwords etc, per the schema defn
//
//        //add to index
//        SolrClient solr = SolrChannel.getSolrServer(coreName);
//
//        try{
//            solr.add(doc);
//            solr.commit();
//        }catch(Exception e){
//            LOG.error("error indexing file " + file.getAbsolutePath(), e);
//            LogChannel.recordInCsv(new Date(), "INDEXING_ERROR", file.toString(), file.getPath(), e.getMessage(), "");
//        }
//
//        LOG.info("completed indexing - " + file);
//        SolrChannel.shutdownSolrServer(solr);
//    }
//
//    private static String getRelativeFullFilePath(File fileToIndex, File originalZipFile) {
//        String home = PropertiesHelper.getZipExplodeFolder(PropertiesHelper.getFileSorterHome());
//        String r = StringUtils.remove(fileToIndex.toString(), home);
//        r = StringUtils.replaceOnce(r, File.separator, ":");
//        r = originalZipFile.toString() + r;
//        return r;
//    }
//
//    private static String getRelativeFileName(File fileToIndex, File originalZipFile) {
//        String o = originalZipFile.getName();
//        String f = fileToIndex.getName();
//        String r =  o + ":" + f;
//        return r;
//    }
//}
//
