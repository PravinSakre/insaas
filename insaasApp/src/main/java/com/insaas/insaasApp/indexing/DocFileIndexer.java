//package com.insaas.insaasApp.indexing;
//
//
//import org.apache.commons.lang.StringUtils;
//import org.apache.log4j.Logger;
//import org.apache.poi.xwpf.usermodel.XWPFDocument;
//import org.apache.poi.xwpf.usermodel.XWPFParagraph;
//import org.apache.solr.client.solrj.SolrClient;
//import org.apache.solr.common.SolrInputDocument;
//import org.apache.tika.Tika;
//
//import com.insaas.insaasApp.dao.LogChannel;
//import com.insaas.insaasApp.dao.SolrChannel;
//import com.insaas.insaasApp.helper.FriendlyMimeTypeMap;
//import com.insaas.insaasApp.helper.UidHelper;
//import com.insaas.insaasApp.helper.ViewsHelper;
//import com.insaas.insaasApp.model.ExclusionParameters;
//
//import java.io.File;
//import java.io.FileInputStream;
//import java.io.IOException;
//import java.io.InputStream;
//import java.nio.file.Files;
//import java.nio.file.Path;
//import java.nio.file.Paths;
//import java.nio.file.attribute.FileTime;
//import java.util.Date;
//import java.util.List;
//
//public class DocFileIndexer extends SolrFileIndexer {
//    private static final Logger LOG = Logger.getLogger(DocFileIndexer.class);
//    public DocFileIndexer(File fileToIndex, long waitTimeToComplete, String coreName, ExclusionParameters exclusionParameters) {
//        super(fileToIndex, waitTimeToComplete, coreName, exclusionParameters);
//    }
//
//    @Override
//    protected void indexFile(File file, String coreName) {
//        try {
//            indexFileImpl(file, coreName);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
//
//    private void indexFileImpl(File file, String coreName) throws IOException {
//        InputStream is = new FileInputStream(file);
//        XWPFDocument d = new XWPFDocument(is);
//        List<XWPFParagraph> l = d.getParagraphs();
//        int readFrom = seek(l);
//
//        for (int i = readFrom ; i < l.size() ; i++) {
//            XWPFParagraph o = l.get(i);
//            String text = o.getParagraphText();
//            String t = StringUtils.strip(text);
//            if(StringUtils.isEmpty(t))
//                continue;
//
//            if(isAttachmentText(o))
//                return;
////            System.out.println(o.getParagraphText());
//            indexIt(coreName, t, file, "clause_" + i, String.valueOf(i));
//        }
//    }
//
//    private boolean isAttachmentText(XWPFParagraph o) {
//        String header = o.getStyle();
//        if(StringUtils.startsWith(header, "Heading") && StringUtils.startsWithIgnoreCase(o.getParagraphText(), "Attachment"))
//            return true;
//        return false;
//    }
//
//    private void indexIt(String coreName, String doccontent, File file, String clauseId, String clauseCount) throws IOException {
//        SolrInputDocument doc = addOtherContent(file, clauseId, clauseCount);
//
//        doc.addField("content", doccontent);
//        doc.addField("tokenized_content", doccontent);
//
////        System.out.println(doccontent);
//        SolrClient solr = SolrChannel.getSolrServer(coreName);
//        try{
//            solr.add(doc);
//            solr.commit();
//        }catch(Exception ex){
//            //failed - try again
//
//            try{
//                //doc = addOtherContent(meta, file,textHandler);
//                solr.add(doc);
//                solr.commit();
//            }catch(Exception e){
//                LOG.error("error indexing file " + file.getAbsolutePath(), e);
//                LogChannel.recordInCsv(new Date(), "INDEXING_ERROR", file.toString(), file.getPath(), e.getMessage(), "");
//            }
//        }
//        LOG.info("completed indexing - " + doc.get("id"));
//        SolrChannel.shutdownSolrServer(solr);
//    }
//
//    private int seek(List<XWPFParagraph> l) {
//        int i = 0;
//        int tocEndPos = 0;
//        for (XWPFParagraph o : l) {
//            i++;
//            String s = o.getStyle();
////            System.out.println(s + "; " + o.getStyleID() + "; " + o.getParagraphText());
//            if (StringUtils.startsWith(s, "TOC")) {
//                tocEndPos = i;
////                System.out.println(o.getParagraphText());
//            }
//        }
//        return tocEndPos;
//    }
//
//    private SolrInputDocument addOtherContent(File file, String clauseId, String clauseCount) throws IOException {
//        String mimeType = new Tika().detect(file);//Files.probeContentType(path);
//
//        Path path = Paths.get(file.getAbsolutePath());
//        FileTime lastModifiedTime = Files.getLastModifiedTime(path);
//        FileTime creationTime = (FileTime) Files.getAttribute(path, "creationTime");
//        FileTime lastAccessedTime = (FileTime) Files.getAttribute(path, "lastAccessTime");
//        String doccreator = Files.getOwner(path).getName();
//        Long fileSize = (Long) Files.getAttribute(path, "size");
//
//        SolrInputDocument doc = new SolrInputDocument();
//
//        doc.addField("id", file.getAbsolutePath() + "_" + clauseId);
//
//        doc.addField("filename", file.getName());
//        doc.addField("hash_filename", UidHelper.getHashIdFromString(file.getName()));
//        doc.addField("filename_length", String.valueOf(file.getName().length()));
//        doc.addField("filepath_s", file.getAbsolutePath());
//        //doc.addField("clauseid_s", clauseId);
//        doc.addField("clauseid_s", clauseCount);
//
//        doc.addField("hash_id", UidHelper.getHashIdFromString(file.getAbsolutePath() + "_" + clauseId));
//
//        doc.addField("filetype", mimeType);
//        doc.addField("filepath", file.getParent());
//        doc.addField("hash_filepath", UidHelper.getHashIdFromString(file.getParent()));
//
//        doc.addField("derived_file_type", FriendlyMimeTypeMap.getFriendlyMimeValue(mimeType));
//
//        // dates for testing
////        doc.addField("modified_by", lastModifiedBy);
//        doc.addField("attributes_created_time", new Date(creationTime.toMillis()));
//        doc.addField("attributes_accessed_time", new Date(lastAccessedTime.toMillis()));
//        doc.addField("attributes_modified_time", new Date(lastModifiedTime.toMillis()));
//
//        doc.addField("filesize", fileSize);
//        doc.addField("created_date", new Date(creationTime.toMillis()));
//
//        // doc.addField("last_modified",FileValidator.getLastModifiedTime(file));
//        doc.addField("last_modified", new Date(lastModifiedTime.toMillis()));
//        //map metadata fields to default schema
//        //location: path\solr-4.7.2\example\solr\collection1\conf\schema.xml
//
//        //Dublin Core
//        //thought: schema could be modified to use Dublin Core
////        doc.addField("title", doctitle);
//        doc.addField("author", doccreator);
//
//        doc.addField("extension", ViewsHelper.getFileExtension(file.getName()));
//        doc.addField("category1_s", "Uncategorized");
//        doc.addField("category2_s", "Uncategorized");
//        doc.addField("category3_s", "Uncategorized");
//
//        //other metadata
//
//        //content (and text)
//        //per schema, the content field is not indexed by default, used for returning and highlighting document content
//        //the schema "copyField" command automatically copies this to the "text" field which is indexed
//        return doc;
//    }
//}
