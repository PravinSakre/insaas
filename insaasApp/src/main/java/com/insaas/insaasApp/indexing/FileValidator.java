//package com.insaas.insaasApp.indexing;
//
//
//import com.insaas.insaasApp.model.ExclusionParameters;
//
//import java.io.File;
//import java.io.IOException;
//import java.nio.file.Files;
//import java.nio.file.attribute.BasicFileAttributes;
//import java.nio.file.attribute.FileTime;
//import java.time.LocalDate;
//import java.util.ArrayList;
//import java.util.Collection;
//import java.util.Date;
//import java.util.regex.Matcher;
//import java.util.regex.Pattern;
//
//import org.apache.commons.io.FileUtils;
//
//
///**
// * User: Surya
// * Date: 2/20/15
// * Time: 11:58 AM
// * To change this template use File | Settings | File Templates.
// */
//public class FileValidator {
//    private static Collection<String> getFileExclusionPatterns(ExclusionParameters exclusionParameters) {
//        return exclusionParameters.getFilesToExclude();
//    }
//
//    private static Collection<String> getFolderExclusionPatterns(ExclusionParameters exclusionParameters) {
//        return exclusionParameters.getFoldersToExclude();
//    }
//
//    public static boolean shouldIndexFile(File file, ExclusionParameters exclusionParameters) {
//        //check if the filename is acceptable
//        String fileName=file.getName();
//        Collection<String> list=getFileExclusionPatterns(exclusionParameters);
//        Collection<String> modifiedList= new ArrayList<String>();
//        modifiedList.addAll(list);
//        modifiedList.add("^\\~");
//        modifiedList.add("Thumbs.db");
//        if(matches(fileName, modifiedList))
//            return false;
//
//        //check if the folder name is acceptable
//        String folderName=file.getParent();
//        Collection<String> foldersToExclude=getFolderExclusionPatterns(exclusionParameters);
//        if(matches(folderName, foldersToExclude))
//            return false;
//
//        //size
//        float actualSize = getFileSizeInMB(file);
//        if(exclusionParameters.getMaxFileSize() > 0 && actualSize > exclusionParameters.getMaxFileSize())
//            return false;
//
//        //age
//        if(exclusionParameters.getMaxFileAge() > 0) {
//            int months = monthsSince(file);
//            if(months > exclusionParameters.getMaxFileAge())
//                return false;
//        }
//
//        return true;
//    }
//
//    private static int monthsSince(File file) {
//        try {
//            Date lastAccessedTime = getLastAccessedTime(file);
//          //  int m = Months.monthsBetween(new LocalDate(lastAccessedTime), new LocalDate()).getMonths();
//			/*
//			 * if(m > 0) return m;
//			 */
//        } catch (Exception e) {
//            //ignore
//        }
//        return 0;
//    }
//
//    public static Date getLastAccessedTime(File file) throws IOException {
//        BasicFileAttributes attrs= Files.readAttributes(file.toPath(), BasicFileAttributes.class);
//        FileTime time = attrs.lastAccessTime();
//        return new Date(time.toMillis());
//    }
//
//    public static float getFileSizeInMB(File file) {
//        float fSize = FileUtils.sizeOf(file);
//        return fSize/(1024*1024);
//    }
//
//    public static Date getCreationTime(File file) throws IOException {
//        BasicFileAttributes attrs= Files.readAttributes(file.toPath(), BasicFileAttributes.class);
//        FileTime time = attrs.creationTime();
//        return new Date(time.toMillis());
//    }
//
//    public static Date getLastModifiedTime(File file) throws IOException {
//        BasicFileAttributes attrs= Files.readAttributes(file.toPath(), BasicFileAttributes.class);
//        FileTime time = attrs.lastModifiedTime();
//        return new Date(time.toMillis());
//    }
//
//    private static boolean matches(String fileName, Collection<String> list) {
//        if(list.size() > 0){
//            for(String item:list){
//                Pattern p = Pattern.compile(item.toLowerCase());
//                Matcher m = p.matcher(fileName.toLowerCase()); // get a matcher object
//                if(m.find())
//                    return true;
//            }
//        }
//        return false;
//    }
//}
