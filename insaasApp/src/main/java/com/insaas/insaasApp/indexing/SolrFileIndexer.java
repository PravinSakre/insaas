//package com.insaas.insaasApp.indexing;
//
//
//import org.apache.log4j.Logger;
//
//import com.insaas.insaasApp.dao.LogChannel;
//import com.insaas.insaasApp.model.ExclusionParameters;
//import com.insaas.insaasApp.model.IndexingMetrics;
//import com.insaas.insaasApp.rules.FreezingRules;
//import com.insaas.insaasApp.rules.IndexingRules;
//
//import java.io.File;
//
///**
// * User: Suryapc
// * Date: 2/18/15
// * Time: 5:06 PM
// * To change this template use File | Settings | File Templates.
// */
//public abstract class SolrFileIndexer implements SolrIndexable {
//    private static Logger LOG = Logger.getLogger(SolrFileIndexer.class);
//
//    private final File fileToIndex;
//    private final long waitTimeToComplete;
//    private final String coreName;
//    private ExclusionParameters exclusionParameters;
//    private String file;
//
//    public SolrFileIndexer(File fileToIndex, long waitTimeToComplete,String coreName, ExclusionParameters exclusionParameters) {
//        this.fileToIndex = fileToIndex;
//        this.waitTimeToComplete = waitTimeToComplete;
//        this.coreName=coreName;
//        this.exclusionParameters = exclusionParameters;
//    }
//    public void run() {
//        IndexingMetrics.SINGLETON.decrementFilesInQueue();
//        IndexingMetrics.SINGLETON.incrementFilesProcessing();
//        File actualFile = getFileToIndex();
//        if(!IndexingRules.shouldIndex(actualFile, coreName, exclusionParameters)) {
//            IndexingMetrics.SINGLETON.incrementFilesIgnored();
//            IndexingMetrics.SINGLETON.decrementFilesProcessing();
//            LogChannel.recordIgnoredInCsv(actualFile.getName(),actualFile.getAbsolutePath());
//            LOG.error("file ignored based on exclusion parameters - " + actualFile);
//            return;
//        }
//        FreezingRules.FREEZING_RULES.freezeIndexingIfNeeded(IndexingMetrics.SINGLETON);
//        indexFile(fileToIndex,coreName);
//        IndexingMetrics.SINGLETON.incrementFilesIndexed();
//        IndexingMetrics.SINGLETON.decrementFilesProcessing();
//        try {
//            Thread.sleep(waitTimeToComplete);
//        } catch (InterruptedException e) {
//            LOG.error("error while sleeping");
//        }
//    }
//
//    protected abstract void indexFile(File file,String coreName);
//
//    protected File getFileToIndex() {
//        return fileToIndex;
//    }
//}
