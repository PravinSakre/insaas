//package com.insaas.insaasApp.indexing;
//
//import com.insaas.insaasApp.dao.LogChannel;
//import com.insaas.insaasApp.dao.SolrChannel;
//import com.insaas.insaasApp.helper.FriendlyMimeTypeMap;
//import com.insaas.insaasApp.helper.GetPropertyValues;
//import com.insaas.insaasApp.helper.UidHelper;
//import com.insaas.insaasApp.helper.ViewsHelper;
//import com.insaas.insaasApp.model.ExclusionParameters;
//import com.sun.org.apache.bcel.internal.generic.NEW;
//
//import org.apache.log4j.Logger;
//import org.apache.solr.client.solrj.SolrClient;
//import org.apache.solr.client.solrj.SolrServerException;
//import org.apache.solr.common.SolrInputDocument;
//import org.apache.tika.Tika;
//import org.apache.tika.exception.TikaException;
//import org.apache.tika.io.TikaInputStream;
//import org.apache.tika.metadata.DublinCore;
//import org.apache.tika.metadata.Metadata;
//import org.apache.tika.parser.AutoDetectParser;
//import org.apache.tika.parser.ParseContext;
//import org.apache.tika.parser.Parser;
//import org.apache.tika.parser.microsoft.OfficeParser;
//import org.apache.tika.parser.microsoft.ooxml.OOXMLParser;
//import org.apache.tika.sax.BodyContentHandler;
//import org.xml.sax.ContentHandler;
//import org.xml.sax.SAXException;
//
//import java.io.File;
//import java.io.FileInputStream;
//import java.io.IOException;
//import java.io.InputStream;
//import java.nio.file.Files;
//import java.nio.file.Path;
//import java.nio.file.Paths;
//import java.nio.file.attribute.FileTime;
//import java.util.Date;
//
///**
// * User: Suryapc
// * Date: 2/18/15
// * Time: 6:26 PM
// * To change this template use File | Settings | File Templates.
// */
//public class TikaFileIndexer extends SolrFileIndexer {
//    private static final Logger LOG = Logger.getLogger(TikaFileIndexer.class);
//   private static int nums = 0;
//   protected static String ooSupportedTypes =null;
//   protected static String ofSupportedTypes =null;
//    public TikaFileIndexer(File fileToIndex, long waitTimeToComplete,String coreName, ExclusionParameters exclusionParameters) {
//    	super(fileToIndex, waitTimeToComplete,coreName, exclusionParameters);
//   	 try {
//   		 if(null == TikaFileIndexer.ooSupportedTypes){
//			OOXMLParser ooParser = new OOXMLParser();//(new FileInputStream(jFile), metadata);
//			  OfficeParser ofParser = new OfficeParser();
//			  ParseContext context = new ParseContext();
//			  TikaFileIndexer.ooSupportedTypes = ooParser.getSupportedTypes(context).toString();
//			  TikaFileIndexer.ofSupportedTypes = ofParser.getSupportedTypes(context).toString();
//   		 	}
//   		 } catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//    }
//
//
//    /** (non-Javadoc)
//     * @see com.bwc.indexing.SolrFileIndexer#indexFile(java.io.File, java.lang.String)
//     */
//    @Override
//       protected void indexFile(File file,String coreName) {
//        try {
//            indexFileImpl(file,coreName);
//        } catch (IOException e) {
//            LOG.error("Exception indexing " +file.getAbsolutePath(), e);
//        } catch (SAXException e) {
//            LOG.error("Exception indexing " +file.getAbsolutePath(), e);
//        } catch (TikaException e) {
//            LOG.error("Exception indexing " +file.getAbsolutePath(), e);
//        } catch (SolrServerException e) {
//            LOG.error("Exception indexing " +file.getAbsolutePath(), e);
//        }
//    }
///**
// *
// * @param file
// * @param coreName
// * @throws IOException
// * @throws SAXException
// * @throws TikaException
// * @throws SolrServerException
// */
//    protected void indexFileImpl(File file,String coreName) throws IOException, SAXException, TikaException, SolrServerException {
//        //To change body of created methods use File | Settings | File Templates.
//
//        InputStream input = TikaInputStream.get(file);
//        Metadata meta = new Metadata();
//
//        ContentHandler textHandler = null;
//       if(GetPropertyValues.isContentEnabled()){
//        textHandler = getContentHandler(file, input, meta);
//        //System.out.println("************^^^^^^^^^^^^+++++++"+new Tika().detect(file));
//       }
//        //call to index
//        indexDocument(textHandler,meta,coreName,file);
//        input.close();
//    }
//
//   /**
//    *
//    * @param file
//    * @param input
//    * @param meta
//    * @return
//    * @throws IOException
//    * @throws SAXException
//    * @throws TikaException
//    */
//    private ContentHandler getContentHandler(File file, InputStream input, Metadata meta) throws IOException, SAXException, TikaException {
//        if(FileValidator.getFileSizeInMB(file) > 199)
//            return null;
//        //use Apache Tika to convert documents in different formats to plain text
//        ContentHandler textHandler = new BodyContentHandler(200*1024*1024);
//        Parser parser = new AutoDetectParser(); //handles documents in different formats:
//        ParseContext context = new ParseContext();
//        parser.parse(input, textHandler, meta, context); //convert to plain text
//        return textHandler;
//    }
///**
// *
// * @param textHandler
// * @param meta
// * @param coreName
// * @param file
// * @throws IOException
// * @throws SolrServerException
// * @throws SAXException
// */
//    private void indexDocument(ContentHandler textHandler,Metadata meta ,String coreName,File file) throws IOException, SolrServerException, TikaException, SAXException {
//        SolrInputDocument doc = addOtherContent(meta, file, textHandler);
//
//        if(textHandler != null && GetPropertyValues.isContentEnabled()) {
//            String doccontent = textHandler.toString();
//            doc.addField("content", doccontent);
//            doc.addField("tokenized_content", doccontent);
//        }
//        //indexing
//        //when a field is indexed, like "text", Solr will handle tokenization, stemming, removal of stopwords etc, per the schema defn
//
//        //add to index
//        SolrClient solr = SolrChannel.getSolrServer(coreName);
//        try{
//            solr.add(doc);
//            solr.commit();
//        }catch(Exception ex){
//            //failed - try again
//
//            try{
//            	//doc = addOtherContent(meta, file,textHandler);
//                solr.add(doc);
//                solr.commit();
//            }catch(Exception e){
//                LOG.error("error indexing file " + file.getAbsolutePath(), e);
//                LogChannel.recordInCsv(new Date(), "INDEXING_ERROR", file.toString(), file.getPath(), e.getMessage(), "");
//            }
//        }
//        LOG.info("completed indexing - " + file);
//        SolrChannel.shutdownSolrServer(solr);
//    }
//
//    /**
//     * Add file schema to solr document
//     * @param metadata
//     * @param file
//     * @param textHandler if this parameter is null, use alternate method to get file params
//     * @return
//     * @throws IOException
//     */
//    private SolrInputDocument addOtherContent(Metadata metadata, File file, ContentHandler textHandler) throws IOException, TikaException, SAXException {
//
//    	Path path = Paths.get(file.getAbsolutePath());
//
//    	String doctitle = null;
//        String doccreator = null;
//        String lastModifiedBy = null;
//
//        //String mimeType = Files.probeContentType(path);
//        String mimeType = new Tika().detect(file) ;//Files.probeContentType(path);
//		java.text.SimpleDateFormat df = new java.text.SimpleDateFormat("YYYY-MM-dd'T'hh:mm:ss'Z'");
//
//        String lastModifiedTime = df.format(((FileTime)Files.getLastModifiedTime(path)).toMillis()); 
//        System.out.println("****************************"+lastModifiedTime);
//        String creationTime = df.format(new Date(((FileTime)Files.getAttribute(path, "creationTime")).toMillis()));
//        System.out.println("****************************"+creationTime);
//        String lastAccessedTime = df.format(((FileTime)Files.getAttribute(path, "lastAccessTime")).toMillis());
//        System.out.println("****************************"+lastAccessedTime);
//        doccreator = Files.getOwner(path).getName();
//        Long fileSize = (Long)Files.getAttribute(path, "size");
//
//
//        try {
//			if(null == textHandler && fileSize.longValue() <(300*1024*1024) ){
//				//check if file is a office document, then only parse for additional attributes
//				 if(TikaFileIndexer.ooSupportedTypes.indexOf(mimeType)>=0 || TikaFileIndexer.ofSupportedTypes.indexOf(mimeType)>=0){
//			     	 textHandler = new BodyContentHandler(250*1024*1024);
//			         Parser parser = new AutoDetectParser(); //handles documents in different formats:
//			         ParseContext context = new ParseContext();
//			         parser.parse(new FileInputStream(file), textHandler, metadata, context);
//
//			     }
//
//			}
//			if(metadata.size() > 0){
//				doctitle = metadata.get(DublinCore.TITLE);
//				doccreator = metadata.get(DublinCore.CREATOR);
//			    lastModifiedBy = metadata.get("Last-Author");
//			}
//
//		} catch (Exception e) {
//
//			LOG.error("Error Extracting additional file info - other contents indexed " +e.getMessage());
//		}
//
//
//        LOG.info("indexing " + file + " of mime " + mimeType);
//
//        SolrInputDocument doc = new SolrInputDocument();
//
//        doc.addField("id", file.getAbsolutePath());
//
//        doc.addField("filename",file.getName());
//        doc.addField("hash_filename", UidHelper.getHashIdFromString(file.getName()));
//        doc.addField("filename_length", String.valueOf(file.getName().length()));
//
//        doc.addField("hash_id", UidHelper.getHashIdFromString(file.getAbsolutePath()));
//
//        doc.addField("filetype", mimeType);
//        doc.addField("filepath", file.getParent());
//        doc.addField("hash_filepath", UidHelper.getHashIdFromString(file.getParent()));
//
//        doc.addField("derived_file_type", FriendlyMimeTypeMap.getFriendlyMimeValue(mimeType));
//
//        // dates for testing
//        doc.addField("modified_by", lastModifiedBy);
//        doc.addField("attributes_created_time", creationTime);
//        doc.addField("attributes_accessed_time",lastAccessedTime);
//        doc.addField("attributes_modified_time",lastModifiedTime);
//
//        doc.addField("filesize",fileSize);
//        doc.addField("created_date",creationTime);
//
//       // doc.addField("last_modified",FileValidator.getLastModifiedTime(file));
//        doc.addField("last_modified",lastModifiedTime);
//        //map metadata fields to default schema
//        //location: path\solr-4.7.2\example\solr\collection1\conf\schema.xml
//
//        //Dublin Core
//        //thought: schema could be modified to use Dublin Core
//        doc.addField("title", doctitle);
//        doc.addField("author", doccreator);
//
//        doc.addField("extension", ViewsHelper.getFileExtension(file.getName()));
//
//        //other metadata
//
//        //content (and text)
//        //per schema, the content field is not indexed by default, used for returning and highlighting document content
//        //the schema "copyField" command automatically copies this to the "text" field which is indexed
//        return doc;
//    }
//
//}
