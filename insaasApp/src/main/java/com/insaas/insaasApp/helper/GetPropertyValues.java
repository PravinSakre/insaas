//package com.insaas.insaasApp.helper;
//
////import com.bwc.dao.LogChannel;
//
////import com.bwc.model.User;
//import org.apache.commons.lang.StringUtils;
//import org.apache.log4j.Logger;
//import org.w3c.dom.Document;
//import org.w3c.dom.Element;
//import org.w3c.dom.Node;
//import org.w3c.dom.NodeList;
//
//import com.insaas.insaasApp.dao.SolrChannel;
//import com.insaas.insaasApp.dto.User;
//
//import javax.xml.parsers.DocumentBuilder;
//import javax.xml.parsers.DocumentBuilderFactory;
//import java.io.*;
//import java.util.*;
//
//
//
///**
// * This class converts property file to usable properties
// * Copyright BrainWave Consulting
// * @author Rahul
// *
// */
//public class GetPropertyValues {
//	private static final Logger log = Logger.getLogger(GetPropertyValues.class);
//	private static Properties prop = null;
//	private static Map<String, User> users;
//	private static final String LOCK = "lock";
//	public static String FILE_SORTER_HOME;
//
//	/**
//	 * @param inputStream
//	 * @return
//	 */
//	public static Properties readProprties(InputStream inputStream) {
//		Properties p = new Properties();
//		if (inputStream != null) {
//			try {
//				p.load(inputStream);
//			} catch (IOException e) {
//				log.error("Error Opening config.properties file"
//						+ e.getMessage());
//			}
//		}
//		return p;
//	}
//
//	/**
//	 * @param homePath
//	 * @throws FileNotFoundException
//	 */
//	public static void initializeProperties(String homePath)
//			throws FileNotFoundException {
//		synchronized (LOCK) {
//			InputStream inputStream = new FileInputStream(
//					PropertiesHelper.getConfigFile(homePath));
//			prop = readProprties(inputStream);
//
//			// getting values from build.properties
//			Properties properties = null;
//			try {
//				properties = new Properties();
//				properties.load(Thread.currentThread().getContextClassLoader()
//						.getResourceAsStream("build.properties"));
//			} catch (IOException e) {
//				log.error("Error Opening build.properties file"
//						+ e.getMessage());
//			}
//
//			if (properties != null) {
//				prop.putAll(properties);
//			}
//		}
//	}
//
//	/**
//	 * @param file
//	 */
//	private static void overloadProperties(File file) {
//		// create input stream
//		try {
//			InputStream inputStream = new FileInputStream(file);
//			Properties userProp = readProprties(inputStream);
//			prop.putAll(userProp);
//
//		} catch (FileNotFoundException e) {
//			log.error("Error Opening user properties file" + e.getMessage());
//		} catch (IOException e) {
//			log.error("Error Opening user properties file" + e.getMessage());
//		}
//	}
//
//	/**
//	 *
//	 * @param key
//	 * @return
//	 */
//	public static String getPropValues(String key) {
//		return prop.getProperty(key);
//	}
//
//	/**
//	 *
//	 * @return
//	 */
//	public static Properties getAllProperties() {
//		Properties p = new Properties();
//		p.putAll(prop);
//		p.setProperty(PropertiesHelper.FILE_SORTER_HOME_ENV_VARIABLE,
//				FILE_SORTER_HOME);
//		p.setProperty("folder.home.config", PropertiesHelper
//				.getConfigFolder(PropertiesHelper.getFileSorterHome()));
//		p.setProperty("folder.home.backup",
//				PropertiesHelper.getBackupFolder(""));
//		p.setProperty("folder.home.zipfolder", PropertiesHelper
//				.getZipExplodeFolder(PropertiesHelper.getFileSorterHome()));
//		return p;
//	}
//
//	public static String getPropertyValue(String key) {
//		if(prop!=null){
//			return (String) prop.get(key);
//		}
//		return "";
//	}
//
//	/***
//	 *
//	 * @param key
//	 * @param defaultValue
//	 * @return
//	 */
//	public int getIntProperty(String key, int defaultValue) {
//		try {
//			String result = prop.getProperty(key);
//			return Integer.parseInt(result);
//		} catch (Exception e) {
//			// do nothing
//		}
//		return defaultValue;
//	}
//
//	/***
//	 *
//	 * @param key
//	 * @param defaultValue
//	 * @return
//	 */
//	public boolean getBooleanProperty(String key, boolean defaultValue) {
//		try {
//			String result = prop.getProperty(key);
//			return Boolean.parseBoolean(result);
//		} catch (Exception e) {
//			// do nothing
//		}
//		return defaultValue;
//	}
//
//	/**
//	 * @param fileName
//	 * @return
//	 */
//	public static boolean isFileInExclusionList(String fileName) {
//		String list = (String) GetPropertyValues
//				.getPropValues("exclusionFileList");
//		if (StringUtils.length(list) > 0) {
//			StringTokenizer str = new StringTokenizer(list, ",");
//			while (str.hasMoreTokens()) {
//				StringBuffer token = new StringBuffer(".");
//				token.append(str.nextToken());
//				String tokenInStr = token.toString();
//				if (fileName.matches(".*(" + tokenInStr + ")$")) {
//					return true;
//				}
//			}
//		}
//		return false;
//	}
//
//	/**
//	 * @param homePath
//	 */
//	public static void initializeUsers(String homePath) {
//		Map<String, User> hashMap = new HashMap<String, User>();
//		try {
//			String fXmlFile = PropertiesHelper.getUserXML(homePath);
//
//			DocumentBuilderFactory dbFactory = DocumentBuilderFactory
//					.newInstance();
//			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
//			SolrAdacDocument doc = dBuilder.parse(fXmlFile);
//
//			// optional, but recommended
//			// read this -
//			// http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
//			doc.getDocumentElement().normalize();
//			NodeList nList = doc.getElementsByTagName("user");
//
//			for (int temp = 0; temp < nList.getLength(); temp++) {
//				Node nNode = nList.item(temp);
//				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
//					Element eElement = (Element) nNode;
//					User u = new User();
//					u.setUsername(eElement.getAttribute("username"));
//					u.setPassword(eElement.getAttribute("password"));
//					u.setRoles(eElement.getAttribute("roles"));
//
//					// get core names and saved in user object as list
//					String role = eElement.getAttribute("roles");
//
//					if (StringUtils.equals(role, "admin")) { // if admin get all
//						u.setCoreNames(SolrChannel.getSolrCoreNames());
//					} else {
//						String coreNames[] = StringUtils.split(role, ",");
//						u.setCoreNames(Arrays.asList(coreNames));
//					}
//					hashMap.put(u.getUsername(), u);
//				}
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		users = hashMap;
//	}
//
//	/**
//	 * @param username
//	 * @param password
//	 * @return
//	 */
//	public static boolean isPasswordValid(String username, String password) {
//		User user = getUser(username);
//		if (user == null)
//			return false;
//		if (StringUtils.equals(user.getPassword(), password))
//			return true;
//		return false;
//	}
//
//	/**
//	 * Check property if download file is enabled
//	 * @return
//	 */
//	public static boolean isDownloadFileEnable() {
//		String value = getPropValues("download.files");
//		if (StringUtils.isBlank(value))
//			return false;
//		if (StringUtils.isNotBlank(value)) {
//			if (StringUtils.equals(value, "true"))
//				return true;
//			else
//				return false;
//		}
//		return false;
//	}
//	/**
//	 * Check if content indexing is enabled
//	 * @return
//	 */
//	public static boolean isContentEnabled() {
//		String value = getPropValues("content.index");
//		if (StringUtils.isBlank(value))
//			return false;
//		if (StringUtils.isNotBlank(value)) {
//			if (StringUtils.equals(value, "true"))
//				return true;
//			else
//				return false;
//		}
//		return false;
//	}
//
//	/**
//		 * Check if single field is enabled
//		 * @return
//		 */
//		public static boolean isSingleFieldEnabled() {
//			String value = getPropValues("single.field");
//			if (StringUtils.isBlank(value))
//				return false;
//			if (StringUtils.isNotBlank(value)) {
//				if (StringUtils.equals(value, "true"))
//					return true;
//				else
//					return false;
//			}
//			return false;
//	}
//
//	public static boolean isExportButtonEnabled() {
//		String value = getPropValues("result.exportButton");
//		if (StringUtils.isBlank(value))
//			return false;
//		if (StringUtils.isNotBlank(value)) {
//			if (StringUtils.equals(value, "true"))
//				return true;
//			else
//				return false;
//		}
//		return false;
//	}
//
//	public static boolean isExportDelButtonEnabled() {
//		String value = getPropValues("result.exportDelButton");
//		if (StringUtils.isBlank(value))
//			return false;
//		if (StringUtils.isNotBlank(value)) {
//			if (StringUtils.equals(value, "true"))
//				return true;
//			else
//				return false;
//		}
//		return false;
//	}
//
//	public static boolean isFileSizeEnabled() {
//		String value = getPropValues("result.fileSize");
//		if (StringUtils.isBlank(value))
//			return false;
//		if (StringUtils.isNotBlank(value)) {
//			if (StringUtils.equals(value, "true"))
//				return true;
//			else
//				return false;
//		}
//		return false;
//	}
//
//	/**
// * Check if Show Categories in result enabled
// * @return
// */
//	public static boolean isShowCategoryEnabled() {
//		String value = getPropValues("result.showCategory");
//		if (StringUtils.isBlank(value))
//			return false;
//		if (StringUtils.isNotBlank(value)) {
//			if (StringUtils.equals(value, "true"))
//				return true;
//			else
//				return false;
//		}
//		return false;
//	}
//
//	/**
// * Check number of categories enabled
// * @return
// */
////	public static int getNumberOfCategories() {
////		return getIntProperty("result.noOfCategories",0);
////	}
//
//	/**
//	 * Get user details
//	 * @param username
//	 * @return
//	 */
//	public static User getUser(String username) {
//		return users.get(username);
//	}
//}
