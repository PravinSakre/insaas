//package com.insaas.insaasApp.helper;
//
//import org.apache.commons.lang.StringUtils;
//
//import java.text.DateFormat;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//import java.util.Map;
//
///**
// * User: Suryapc
// * Date: 4/8/15
// * Time: 3:59 PM
// * To change this template use File | Settings | File Templates.
// */
//public class PropertiesHelper {
//    public static final String FILE_SORTER_HOME_ENV_VARIABLE = "FILE_SORTER_HOME";
//    public static final String FILE_SORTER_INDEX_HOME_VARIABLE = "INDEX_HOME";
//
//    public static String getConfigFolder(String homeFilePath) {
//        return appendSlashToFolder(homeFilePath) + "config";
//    }
//
//    public static String getConfigFile(String homeFilePath) {
//        return appendSlashToFolder(getConfigFolder(homeFilePath)) + "config.properties";
//    }
//
//    public static String getUserXML(String homeFilePath) {
//        return appendSlashToFolder(getConfigFolder(homeFilePath)) + "users.xml";
//    }
//
//    public static String getBackupFolder(String coreName) {
//        return appendSlashToFolder(appendSlashToFolder(getFileSorterHome()) + "backup") + coreName;
//    }
//
//    public static String getZipExplodeFolder(String homeFilePath) {
//        return appendSlashToFolder(getFileSorterHome()) + "zipfolder";
//    }
//
//    public static String getFileSorterHome() {
//        Map<String, String> env = System.getenv();
////        return "G:\\FallonProject\\filesorterhome";
//        return env.get(FILE_SORTER_HOME_ENV_VARIABLE);
//    }
//
//    public static String getErrorLogFile() {
//        return appendSlashToFolder(appendSlashToFolder(getFileSorterHome()) + "logs") + "error_log.csv";
//    }
//
//    public static String getIgnoredLogFile() {
//        return appendSlashToFolder(appendSlashToFolder(getFileSorterHome()) + "logs") + "ignored_log.csv";
//    }
//
//    public static String getIndexFilesHome() {
//        Map<String, String> env = System.getenv();
//        return env.get(FILE_SORTER_INDEX_HOME_VARIABLE);
//    }
//
//    public static String getIndexCoreFilePath(String coreName) {
//        return appendSlashToFolder(getIndexFilesHome()) + coreName;
//    }
//
//    private static String appendSlashToFolder(String folderPath) {
//        if(StringUtils.endsWith(folderPath, "/") || StringUtils.endsWith(folderPath, "\\"))
//            return folderPath;
//        else
//            return folderPath + System.getProperty("file.separator");
//    }
//
//    public static String getFileWithinZipInZipFolder(String file) {
//        String zipFolder = getZipExplodeFolder(getFileSorterHome());
//        return appendSlashToFolder(zipFolder) + file;
//    }
//
//    public static String[] splitToArray(String value) {
//        if(StringUtils.isNotBlank(value)) {
//            return StringUtils.split(value, "\n");
//        }
//        return new String[0];
//    }
//
//    public static String getSavedSearchFilePath() {
//        return appendSlashToFolder(getConfigFolder(getFileSorterHome())) + "saved_searches.json";
//    }
//
//    public static String getSavedSearchFilePath(String filename) {
//        return appendSlashToFolder(getConfigFolder(getFileSorterHome())) + filename;
//    }
//
//    public static String addTimeStamp(String filePath) {
//        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
//        return StringUtils.replaceOnce(filePath, ".", "_" + dateFormat.format(new Date()) + ".");
//    }
//}
