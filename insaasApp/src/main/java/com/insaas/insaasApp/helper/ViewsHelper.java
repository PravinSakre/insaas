//package com.insaas.insaasApp.helper;
//
//
//import org.apache.commons.lang.StringUtils;
//import org.apache.log4j.Logger;
//
//import com.insaas.insaasApp.model.SolrQueryBuilder;
//
//import javax.servlet.http.HttpServletRequest;
//import java.text.ParseException;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//
///**
// * User: Surya
// * Date: 1/13/15
// * Time: 11:06 PM
// */
//public class ViewsHelper {
//    private static Logger log = Logger.getLogger(ViewsHelper.class);
//
//    public static String getFriendlyFieldName(String fieldName) {
//        if(StringUtils.equals(fieldName, "content_type"))
//            return "File Type";
//        if(StringUtils.equals(fieldName, "last_modified"))
//            return "Last Modified Date";
//        if(StringUtils.equals(fieldName, "derived_file_type"))
//            return "File Type";
//        return fieldName;
//    }
//
//    public static String createUrl(String previousUrl, String dateRange) {
//        if(StringUtils.isEmpty(dateRange))
//            return previousUrl;
//        int start = StringUtils.indexOf(previousUrl, "&last_modified=");
//        int end = StringUtils.indexOf(previousUrl, "%5D", start);
//        String result = StringUtils.substringBefore(previousUrl, "&last_modified=");
//        if(end > -1)
//            result = result + StringUtils.substring(previousUrl, end + 3);
//        return result + "&last_modified=" + dateRange;
//    }
//
//    public static String getFileExtension(String fileName) {
//        if(StringUtils.isEmpty(fileName))
//            return "";
//        return StringUtils.substringAfterLast(fileName, ".");
//    }
//
//    public static String getValueBeforeExtension(HttpServletRequest request,String key) {
//        String fileSize="";
//        if(request.getAttribute(key)!=null){
//            fileSize=request.getAttribute(key).toString();
//        }
//
//        if(StringUtils.isEmpty(fileSize))
//            return "";
//        float f = Float.parseFloat(fileSize);
//        f=f/1000;
//
//        return StringUtils.substringBefore(String.valueOf(f), ".");
//    }
//
//    public static String convertInKbFromString(String fileSize) {
//        if(StringUtils.isEmpty(fileSize))
//            return "";
//        double f = Float.parseFloat(fileSize);
//        f=f*1000;
//        String ss=String.format("%f",f);
//        return StringUtils.substringBefore(String.valueOf(ss), ".");
//    }
//
//    public static int getFileNameLength(String fileName) {
//        if(StringUtils.isEmpty(fileName))
//            return 0;
//        return StringUtils.length(fileName);
//    }
//
//    public static String getValueOrEmpty(String s) {
//        if(StringUtils.isEmpty(s))
//            return "";
//        return s;
//    }
//
//    public static String getFileNameNoExtension(String fileName) {
//        if(StringUtils.isEmpty(fileName))
//            return "";
//        return StringUtils.substringBeforeLast(fileName, ".");
//    }
//
//    public static String cleanFilePath(String filePath) {
//        return StringUtils.replace(filePath, "\\\\", "\\");
//    }
//
//    public static String applyDateFormat(String date) {
//        if(StringUtils.isEmpty(date))
//            return "";
//        SimpleDateFormat inFormatter = new SimpleDateFormat("EEE MMM dd HH:mm:ss ZZZ yyyy");
//        SimpleDateFormat outFormatter = new SimpleDateFormat("dd/MM/yyyy");
//        Date d = null;
//        try {
//            d = inFormatter.parse(date);
//        } catch (ParseException e) {
//            log.error("error parsing " + date, e);
//        }
//        return outFormatter.format(d);
//    }
//    /**
//    * Date format for excel export without timestamp
//    */
// 	public static String applyDateExcelFormat(String date) {
//        if(StringUtils.isEmpty(date))
//            return "";
//        SimpleDateFormat inFormatter = new SimpleDateFormat("EEE MMM dd HH:mm:ss ZZZ yyyy");
//        SimpleDateFormat outFormatter = new SimpleDateFormat("dd/MM/yyyy");
//        Date d = null;
//        try {
//            d = inFormatter.parse(date);
//        } catch (ParseException e) {
//            log.error("error parsing " + date, e);
//        }
//        return outFormatter.format(d);
//    }
//
//    public static String applyDateWithTimeStamp(String date) {
//        if(StringUtils.isEmpty(date))
//            return "";
//        SimpleDateFormat inFormatter = new SimpleDateFormat("EEE MMM dd HH:mm:ss ZZZ yyyy");
//        SimpleDateFormat outFormatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
//        Date d = null;
//        try {
//            d = inFormatter.parse(date);
//        } catch (ParseException e) {
//            log.error("error parsing " + date, e);
//        }
//        return outFormatter.format(d);
//    }
//
//    public static String getSortUrl(String sortBy,HttpServletRequest request){
//
//        StringBuilder url=new StringBuilder("javascript:submitform(");
//        if(!StringUtils.equals(sortBy,"")){
//            url.append("'"+sortBy+"'");
//        }
//        String sortType=request.getParameter("sortType");
//        if(!StringUtils.equals(sortType,"") && StringUtils.equals(sortType,"ASC") ){
//            url.append(","+"'"+"DESC"+"')");
//            return url.toString();
//        }else{
//            url.append(","+"'"+"ASC"+"')");
//            return url.toString();
//        }
//    }
//
//    public static String getValueFromQueryBuilder(String key,SolrQueryBuilder solrQueryBuilder){
//
//        if(StringUtils.equalsIgnoreCase(key,"fileName")){
//            return (StringUtils.isNotBlank(solrQueryBuilder.getFileName()) ? solrQueryBuilder.getFileName():"");
//        }else if(StringUtils.equalsIgnoreCase(key,"folder")){
//            return (StringUtils.isNotBlank(solrQueryBuilder.getFolder()) ? solrQueryBuilder.getFolder():"");
//        }else if(StringUtils.equalsIgnoreCase(key,"ignoreFileNameField")){
//            return (StringUtils.isNotBlank(solrQueryBuilder.getIgnoreFileNameField()) ? solrQueryBuilder.getIgnoreFileNameField():"");
//        }else if(StringUtils.equalsIgnoreCase(key,"ignoreFilePathField")){
//            return (StringUtils.isNotBlank(solrQueryBuilder.getIgnoreFilePathField()) ? solrQueryBuilder.getIgnoreFilePathField():"");
//        }
//        return "";
//    }
//
//
//
//    public static String getArrowClass(HttpServletRequest request){
//       return StringUtils.equals(request.getParameter("sortType"),"ASC") ? "<i class=\"fa fa-sort-asc\"></i>" : "<i class=\"fa fa-sort-desc\"></i>" ;
//    }
//
//    public static void main(String[] a) {
//            applyDateFormat("Fri Feb 12 12:57:18 IST 2010");
//            applyDateFormat("Tue Jan 06 22:05:55 IST 2015");
//            applyDateFormat("Tue Feb 09 07:45:04 IST 2010");
//    }
//}
