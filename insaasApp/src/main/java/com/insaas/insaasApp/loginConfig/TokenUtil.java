
package com.insaas.insaasApp.loginConfig;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.codec.Hex;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
/**
 * Represents an TokenUtil will create access token
 * @author BrainWave Consulting
 * @since Dec 24, 2018
 */
public class TokenUtil {
	
	/** Logger for this class. */
	private static final Logger logger = LogManager.getLogger(TokenUtil.class.getName());

	/** The MessagesConstant MAGIC_KEY. */
	public static final String MAGIC_KEY = "IntelliTech";

	/**
	 * This method is for generating token
	 * @author BrainWave Consulting
	 * @since Dec 24, 2018
	 * @param userDetails
	 * @return
	 */
	public static String createToken(UserDetails userDetails) {
		long expires = System.currentTimeMillis() + 1000L * 60 * 60;
		String tokens=  expires + ":" + computeSignature(userDetails, expires);
		if (logger.isDebugEnabled()) {
			logger.debug("createToken(UserDetails) - {}", "tokens" + tokens); //$NON-NLS-1$ //$NON-NLS-2$
		}
		return tokens;
	}

	/**
	 * This method is for computing signature 
	 * @author BrainWave Consulting
	 * @since Dec 24, 2018
	 * @param userDetails
	 * @param expires
	 * @return
	 */
	public static String computeSignature(UserDetails userDetails, long expires) {
		StringBuilder signatureBuilder = new StringBuilder();
		signatureBuilder.append(userDetails.getUsername()).append(":");
		signatureBuilder.append(expires).append(":");
		signatureBuilder.append(userDetails.getPassword()).append(":");
		signatureBuilder.append(TokenUtil.MAGIC_KEY);
		MessageDigest digest;
		try {
			digest = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			throw new IllegalStateException("No MD5 algorithm available!");
		}
		return new String(Hex.encode(digest.digest(signatureBuilder.toString().getBytes())));
	}

	/**
	 * This method is for getting user name from token
	 * @author BrainWave Consulting
	 * @since Dec 24, 2018
	 * @param authToken
	 * @return
	 */
	public static String getUserNameFromToken(String authToken) {
		if (logger.isDebugEnabled()) {
			logger.debug("getUserNameFromToken(String) - {}", "authToken=" + authToken.toString()); //$NON-NLS-1$ //$NON-NLS-2$
		}
		if (authToken == null) {
			return null;
		}
		String[] parts = authToken.split(":");
		return parts[0];
	}

	/**
	 * This method is for validating an access token
	 * @author BrainWave Consulting
	 * @since Dec 24, 2018
	 * @param authToken
	 * @param userDetails
	 * @return
	 */
	public static boolean validateToken(String authToken, UserDetails userDetails) {
		String[] parts = authToken.split(":");
		long expires = Long.parseLong(parts[1]);
		String signature = parts[2];
		String signatureToMatch = computeSignature(userDetails, expires);
		if (logger.isDebugEnabled()) {
			logger.debug("validateToken(String, UserDetails) - {}", "signatureToMatch=" + signatureToMatch); //$NON-NLS-1$ //$NON-NLS-2$
		}
		return expires >= System.currentTimeMillis() && signature.equals(signatureToMatch);
	}
}
