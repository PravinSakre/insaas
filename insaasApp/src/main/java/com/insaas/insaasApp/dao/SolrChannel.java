//package com.insaas.insaasApp.dao;
//
//
////import com.bwc.model.*;
//import org.apache.log4j.Logger;
//import org.apache.solr.client.solrj.SolrClient;
//import org.apache.solr.client.solrj.SolrQuery;
//import org.apache.solr.client.solrj.SolrServerException;
//import org.apache.solr.client.solrj.impl.HttpSolrClient;
//import org.apache.solr.client.solrj.request.CoreAdminRequest;
//import org.apache.solr.client.solrj.response.CoreAdminResponse;
//import org.apache.solr.client.solrj.response.QueryResponse;
//import org.apache.solr.common.SolrDocument;
//import org.apache.solr.common.SolrDocumentList;
//import org.apache.solr.common.params.CoreAdminParams.CoreAdminAction;
//import org.apache.solr.common.params.ModifiableSolrParams;
//import org.apache.solr.common.util.NamedList;
//
//import com.insaas.insaasApp.helper.PropertiesHelper;
//import com.insaas.insaasApp.model.*;
//
//import java.io.IOException;
//import java.util.*;
//
///**
// * User: Surya
// * Date: 1/7/15
// * Time: 4:55 PM
// * To change this template use File | Settings | File Templates.
// */
//public class SolrChannel {
//    static Logger log = Logger.getLogger(SolrChannel.class);
//    public static void deleteIndexes(String coreName) {
//        try {
//            SolrClient solrServer = SolrChannel.getSolrServer(coreName);
//            solrServer.deleteByQuery("*:*");
//            solrServer.commit();
//            SolrChannel.shutdownSolrServer(solrServer);
//            log.info("index cleaned");
//        } catch (SolrServerException e) {
//            LogChannel.recordInCsv(new Date(),"Delete failed", "ALL_FILES", "SOLR_SERVER", e.getMessage(),"");
//            log.error("Failed to delete data  "+e.getMessage());
//            throw new RuntimeException("Failed to delete data  "
//                    + e.getMessage(), e);
//        } catch (Exception e) {
//            log.error("Failed to delete data  "+e.getMessage());
//            throw new RuntimeException("Failed to delete data . "
//                    + e.getMessage(), e);
//        }
//    }
//
//    public static long createIndexes(String folder, String workingDir, String coreName, ExclusionParameters exclusionParameters) throws Exception {
//        IndexingMetrics.SINGLETON.setUpIndexingMatrix();
//        String[] folders = PropertiesHelper.splitToArray(folder);
//        SolrIndexer indexer = new SolrIndexer(workingDir, coreName, exclusionParameters);
//        indexer.setUp();
//        for (String f : folders) {
//            indexer.createIndexes(f);
//        }
//        indexer.tearDown();
//        return IndexingMetrics.SINGLETON.getFilesIndexed();
//    }
//
//    public static String createBackUp(String coreName){
//        return SolrBackUp.startBackUp(coreName);
//    }
//
//
//    public static QueryResponse runSolrQuery(SolrQuery solrQuery,String coreName)  {
//        SolrClient solrServer = SolrChannel.getSolrServer(coreName);
//        log.info("running query: " + solrQuery);
//        QueryResponse queryResponse=null;
//        try{
//            queryResponse = solrServer.query(solrQuery);
//        }catch (SolrServerException e){
//            log.error("error running solr query "+ solrQuery, e);
//            throw new RuntimeException("Error running query: "+e.getMessage(), e);
//        }catch (IOException e){
//            log.error("error running solr query "+ solrQuery, e);
//            throw new RuntimeException("Error running query: "+ e.getMessage(),e);
//        }catch (RuntimeException e){
//            log.error("error running solr query "+ solrQuery, e);
//            throw new RuntimeException("Error running query: "+ e.getMessage(),e);
//        }
//        SolrChannel.shutdownSolrServer(solrServer);
//        return queryResponse;
//    }
//
//    public static QueryResponse runNativeQuery(String query, String coreName) {
//        SolrQuery solrQuery=new SolrQuery();
//        solrQuery.setQuery(query);
////        solrQuery.setFields("*,score");
//        return runSolrQuery(solrQuery, coreName);
//    }
//
//    public static List<SolrResult> getSolrResults(SolrDocumentList solrDocumentList, SolrQueryBuilder solrQueryBuilder) {
//        List<SolrResult> solrResults = new ArrayList<SolrResult>();
//        for (SolrDocument lineData : solrDocumentList) {
//            SolrResult solrResult = getSolrResult(solrQueryBuilder, lineData);
//            solrResults.add(solrResult);
//        }
//        return solrResults;
//    }
//
//    private static SolrResult getSolrResult(SolrQueryBuilder solrQueryBuilder, SolrDocument lineData) {
//        SolrResult solrResult = new SolrResult();
//        solrResult.setId((String) lineData.getFieldValue("id"));
//        solrResult.setHash_id((String) lineData.getFieldValue("hash_id"));
//        solrResult.setScore((Float) lineData.getFieldValue("score"));
//        solrResult.setFileName((String) lineData.getFieldValue("filename"));
//        solrResult.setLastModifiedDate(getSingleValue(lineData, "attributes_modified_time"));
//        solrResult.setFullFileName((String) lineData.getFieldValue("id"));
//        solrResult.setFilePath((String) lineData.getFieldValue("filepath"));
//        solrResult.setSize(getSize(lineData, "filesize"));
//        solrResult.setCreatedDate(getSingleValue(lineData, "created_date"));
//        solrResult.setCreatedBy((String) lineData.getFieldValue("author"));
//        solrResult.setFileNameHash((String) lineData.getFieldValue("hash_filename"));
//        solrResult.setExtension((String) lineData.getFieldValue("extension"));
//        solrResult.setLastModifiedBy((String) lineData.getFieldValue("modified_by"));
//        solrResult.setDerivedFileType((String) lineData.getFieldValue("derived_file_type"));
//        solrResult.setAttributesAccessedDate(getSingleValue(lineData, ("attributes_accessed_time")));
//        solrResult.setCategory1_s((String) lineData.getFieldValue("category1_s"));
//        solrResult.setCategory2_s((String) lineData.getFieldValue("category2_s"));
//        solrResult.setCategory3_s((String) lineData.getFieldValue("category3_s"));
//
//        //Rahul - need to be modified and used only in case of split content
//        solrResult.setContent(getSingleValue(lineData,"content"));
//
//        solrResult.addTermCount(solrQueryBuilder.getQuery(), getTermValue(lineData, solrQueryBuilder.getQuery()));
//        for (String keyword : solrQueryBuilder.getKeywords())
//            solrResult.addTermCount(keyword, getTermValue(lineData, keyword));
//        return solrResult;
//    }
//
//    public static SolrResult getSolrResult(SolrDocument lineData) {
//        SolrResult solrResult = new SolrResult();
//        solrResult.setId((String) lineData.getFieldValue("id"));
//        solrResult.setHash_id((String) lineData.getFieldValue("hash_id"));
//        solrResult.setScore((Float) lineData.getFieldValue("score"));
//        solrResult.setFileName((String) lineData.getFieldValue("filename"));
//        solrResult.setLastModifiedDate(getSingleValue(lineData, "attributes_modified_time"));
//        solrResult.setFullFileName((String) lineData.getFieldValue("id"));
//        solrResult.setFilePath((String) lineData.getFieldValue("filepath"));
//        solrResult.setSize(getSize(lineData, "filesize"));
//        solrResult.setCreatedDate(getSingleValue(lineData, "created_date"));
//        solrResult.setCreatedBy((String) lineData.getFieldValue("author"));
//        solrResult.setFileNameHash((String) lineData.getFieldValue("hash_filename"));
//        solrResult.setExtension((String) lineData.getFieldValue("extension"));
//        solrResult.setLastModifiedBy((String) lineData.getFieldValue("modified_by"));
//        solrResult.setDerivedFileType((String) lineData.getFieldValue("derived_file_type"));
//        solrResult.setAttributesAccessedDate(getSingleValue(lineData, ("attributes_accessed_time")));
//        solrResult.setCategory1_s((String) lineData.getFieldValue("category1_s"));
//        solrResult.setCategory2_s((String) lineData.getFieldValue("category2_s"));
//        solrResult.setCategory3_s((String) lineData.getFieldValue("category3_s"));
//
//        //Rahul - need to be modified and used only in case of split content
//        solrResult.setContent(getSingleValue(lineData,"content"));
//
//        return solrResult;
//    }
//
//    public static List<SolrResult> getSolrGroupResults(SolrDocumentList solrDocumentList) {
//        List<SolrResult> solrResults = new ArrayList<SolrResult>();
//        for (SolrDocument lineData : solrDocumentList) {
//            SolrResult solrResult = new SolrResult();
//            solrResult.setId((String) lineData.getFieldValue("id"));
//            solrResult.setHash_id((String) lineData.getFieldValue("hash_id"));
//            solrResult.setScore((Float) lineData.getFieldValue("score"));
//            solrResult.setFileName((String) lineData.getFieldValue("filename"));
//            solrResult.setLastModifiedDate(getSingleValue(lineData, "last_modified"));
//            solrResult.setFullFileName((String) lineData.getFieldValue("id"));
//            solrResult.setFilePath((String) lineData.getFieldValue("filepath"));
//            solrResult.setSize(getSize(lineData, "filesize"));
//            solrResult.setCreatedDate(getSingleValue(lineData, "created_date"));
//            solrResult.setCreatedBy((String) lineData.getFieldValue("author"));
//            solrResult.setExtension((String) lineData.getFieldValue("extension"));
//            solrResults.add(solrResult);
//        }
//        return solrResults;
//    }
//
//
//    public static void deleteFromIndex(List<String> toBeDeleted,String coreName) throws IOException, SolrServerException {
//        SolrClient solrServer = SolrChannel.getSolrServer(coreName);
//        solrServer.deleteById(toBeDeleted);
//        solrServer.commit();
//        SolrChannel.shutdownSolrServer(solrServer);
//    }
//
//    private static String getSingleValue(SolrDocument lineData, String attributeName) {
//        if(lineData.getFieldValue(attributeName) == null)
//            return "";
//        if(!lineData.containsKey(attributeName))
//            return "";
//        if(lineData.getFirstValue(attributeName) == null)
//            return "";
//        return lineData.getFirstValue(attributeName).toString();
//    }
//
//    private static float getSize(SolrDocument lineData, String attributeName) {
//        if(lineData.getFieldValue(attributeName) == null)
//            return 0;
//        float f = Float.parseFloat(lineData.getFieldValue(attributeName).toString());
//        return f/1000;
//    }
//
//    public static int getTermValue(SolrDocument lineData, String term) {
//        Integer i = (Integer) lineData.getFieldValue("termfreq(content,'" + term + "')");
//        if(i == null)
//            i = (Integer) lineData.getFieldValue("termfreq(content,'" + term + "') ");
//        return i == null ? 0 : i;
//    }
//
//    public static SolrClient getSolrServer(String coreName) {
//        String urlString = "http://localhost:8983/solr/"+coreName;
//        return new HttpSolrClient.Builder(urlString).build(); //create solr connection
//    }
//
//    public static List<String> getSolrCoreNames() throws IOException{
//        SolrClient solrServer = new HttpSolrClient.Builder("http://localhost:8983/solr/").build();
//        CoreAdminRequest request = new CoreAdminRequest();
//        request.setAction(CoreAdminAction.STATUS);
//        CoreAdminResponse cores=null;
//
//        try {
//            cores = request.process(solrServer);
//        } catch (SolrServerException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        List<String> coreList=new ArrayList<String>();
//        	  for (int coreCount = 0; coreCount < cores.getCoreStatus().size(); coreCount++) {
//                  coreList.add(cores.getCoreStatus().getName(coreCount));
//              }
//        solrServer.close();
//        return coreList.size()!=0 ? coreList:null;
//    }
//
//    public static void shutdownSolrServer(SolrClient solrServer) {
//        try {
//			solrServer.close();
//		} catch (IOException e) {
//			
//			e.printStackTrace();
//		}
//    }
//
//    public static void main(String []args){
//        try {
//			getSolrCoreNames();
//		} catch (IOException e) {
//			
//			e.printStackTrace();
//		}
//    }
//}
