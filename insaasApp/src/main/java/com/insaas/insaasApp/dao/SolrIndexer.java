//package com.insaas.insaasApp.dao;
//
//
//import org.apache.commons.io.FileUtils;
//import org.apache.commons.lang.StringUtils;
//import org.apache.log4j.Logger;
//import org.apache.solr.client.solrj.SolrQuery;
//import org.apache.solr.client.solrj.response.QueryResponse;
//import org.apache.tika.config.TikaConfig;
//import org.apache.tika.exception.TikaException;
//import org.apache.tika.io.TikaInputStream;
//import org.apache.tika.metadata.Metadata;
//
//import com.insaas.insaasApp.excecutor.SolrExecutable;
//import com.insaas.insaasApp.excecutor.SolrExecutorFactory;
//import com.insaas.insaasApp.helper.GetPropertyValues;
//import com.insaas.insaasApp.helper.PropertiesHelper;
//import com.insaas.insaasApp.helper.UidHelper;
//import com.insaas.insaasApp.model.ExclusionParameters;
//import com.insaas.insaasApp.model.IndexingMetrics;
//import com.insaas.insaasApp.rules.FreezingRules;
//
//import java.io.*;
//import java.util.*;
//import java.util.zip.ZipEntry;
//import java.util.zip.ZipFile;
//import java.util.zip.ZipInputStream;
//
///**
// * User: Surya
// * Date: 1/7/15
// * Time: 5:31 PM
// * To change this template use File | Settings | File Templates.
// */
//public class SolrIndexer {
//    private static final Logger log = Logger.getLogger(SolrIndexer.class);
//    private String workingDir;
//    private String coreName;
//    private ExclusionParameters exclusionParameters;
//    private SolrExecutable solrExecutable;
//    private SolrExecutable solrZipExecutable;
//
//    public SolrIndexer(String workingDir, String coreName, ExclusionParameters exclusionParameters) {
//        this.workingDir = workingDir;
//        this.coreName=coreName;
//        this.exclusionParameters = exclusionParameters;
//        //get no. of thread from config file
//        int noOfThread=Integer.parseInt(GetPropertyValues.getPropValues("threadPoolSize"));
//        //for normal files in folder
//        solrExecutable= SolrExecutorFactory.getSolrExecutable(noOfThread);
//        //for zip files
//        solrZipExecutable=SolrExecutorFactory.getSolrExecutable(0);
//    }
//
//    public void setUp() {
//        setUpLogs();
//        solrExecutable.setUpExecutor();
//        solrZipExecutable.setUpExecutor();
//    }
//
//    private void setUpLogs() {
//        LogChannel.setUpNewErrorFile();
//        LogChannel.setUpNewIgnoreFile();
//    }
//
//    public void tearDown() {
//        solrExecutable.shutdownExecutor();
//        solrZipExecutable.shutdownExecutor();
//        log.info("Total File Added without zip " + IndexingMetrics.SINGLETON.getFilesIndexed());
//    }
//
//    public void createIndexes(String folder) throws Exception {
//        log.info("about to index " + folder + " in working dir " + workingDir + " in core " + coreName);
//        List fileList = new ArrayList<String>();
////        CharSequence cs1 = ":\\";
////        if(folder.length()>0 && folder.contains(cs1)){
//            fileList.add(folder);
//            addFilesToSolr(fileList, "NAME-File");
////        }
//    }
//
//    private void addFilesToSolr(List fileList, String sID) throws Exception {
//        for (Object aFileList : fileList) indexDocs(new File((String) aFileList), sID);
//    }
//
//    private List<String> filesToIndex(File parent, List<String> allFolders) {
//        List<String> foldersToIndex = new ArrayList<String>();
//        Collections.sort(allFolders);
//        String previous = null;
//        for (String candidate : allFolders) {
//            File f = new File(parent,  candidate);
//            if(!f.isDirectory()) {
//                foldersToIndex.add(candidate);
//                continue;
//            }
//            //todo
//            //check if anything in this folder has been indexed
//            boolean somethingIndexed = isAnythingIndexed(parent, candidate);
//            //if so we should skip the next folder
//            if(!somethingIndexed) {
//                if(StringUtils.isNotBlank(previous)) {
//                    foldersToIndex.add(previous);
//                    previous = null;
//                }
//                foldersToIndex.add(candidate);
//                continue;
//            }
//            previous = candidate;
//        }
//        return foldersToIndex;
//    }
//
//    private boolean isAnythingIndexed(File parent, String candidate) {
//        File folder = new File(parent, candidate);
//
//
//        SolrQuery solrQuery=new SolrQuery();
//        solrQuery.setQuery("*");
//        solrQuery.setRows(50);
//        solrQuery.setStart(0);
//
//        String value=parent.getAbsolutePath()+"\\"+candidate;
//        value=UidHelper.getHashIdFromString(value);
//        solrQuery.setFilterQueries("hash_filepath:\""+value+"\"" );
//
//        QueryResponse queryResponse = SolrChannel.runSolrQuery(solrQuery,coreName);
//
//        if(queryResponse.getResults().size()>0){
//            return true;
//        }
//        return false;
//    }
//
//    private void indexDocs(File file, String sID)
//            throws Exception {
//
//        // do not try to index files that cannot be read
//        if (file.canRead()) {
//        	//System.out.println("********** Folder Name -> " + file.getName());
//            if (file.isDirectory()) {
//                //folders should be handled recursively
//                File[] allFiles = file.listFiles();
//                if(null != allFiles){
//                List<File> files = Arrays.asList(allFiles);//filesToIndex(file, Arrays.asList(allFiles));
//                // an IO error could occur
//                if (files != null) {
//                    for (File file1 : files) {
//                        indexDocs( file1, sID);
//                    }
//                }
//                }
//                else{
//                	//handle unreadable directory
//                	LogChannel.recordInCsv(new Date(),"FILE_ACCESS_ERROR", file.getName(), file.getPath(),"no access","");
//                }
//            } else {
//                //handle files
//                try {
//                    FreezingRules.FREEZING_RULES.freezeQueueingIfNeeded(IndexingMetrics.SINGLETON);
////                    if(isZipFile(file)){
////                        handleZipFile(file);
////                    }else{
//                        solrExecutable.index(file, null, coreName, exclusionParameters);
////                    }
////                }
////                catch (FileNotFoundException fnfe) {
////                    log.error("the zipEntry can't be processed "+fnfe.getMessage(),fnfe);
////                    LogChannel.recordInCsv(new Date(),"ZIP_INDEXING_ERROR", file.getName(), file.getPath(),fnfe.getMessage(),"");
//                }
//                catch (Exception e){
//                    log.error("the zipEntry can't be processed "+e.getMessage(),e);
//                    LogChannel.recordInCsv(new Date(),"ZIP_INDEXING_ERROR", file.getName(), file.getPath(),e.getMessage(),"");
//                }
//            }
//        }
//        else{
//        	LogChannel.recordInCsv(new Date(),"FILE_ACCESS_ERROR", file.getName(), file.getPath(),"no access","");
//        }
//    }
//
//    private void handleZipFile(File file) {
//        //its a zip
//        File destDir = new File(PropertiesHelper.getZipExplodeFolder(GetPropertyValues.FILE_SORTER_HOME));
//
//        log.info("unzip " + file);
//        try {
//            ZipFile zipFile = new ZipFile(file);
//            ZipInputStream zipIn = new ZipInputStream(new FileInputStream(file));
//            ZipEntry entry = zipIn.getNextEntry();
//            boolean firstTime=true;
//            while(entry!=null) {
//                String fname="";
//
//                if(firstTime)
//                    firstTime = createDirIfNeeded(destDir, entry, firstTime);
//
//                if(entry.getName().contains("/")){
//                    fname=entry.getName().replace("/","\\");
//                } else
//                    fname=entry.getName().replace("/","\\");
//
//
//                String filePath = destDir.getAbsolutePath() + File.separator + fname;
//
//                if(entry.isDirectory()) {
//                    File dir = new File(filePath);
//                    dir.mkdir();
//                    dir.setWritable(true);
//                }else{
//                    String addToSolr = extractFile(zipIn, filePath, file);
//                    if(entry.getName().contains("/")){
//                        if(addToSolr.equalsIgnoreCase("") && addToSolr.length()==0){
//                            solrZipExecutable.index(file, entry, coreName, exclusionParameters);
//                        }
//                    }
//                }
//                zipIn.closeEntry();
//                entry=zipIn.getNextEntry();
//            }
//            zipFile.close();
//            //System.out.println("deleting directory : ");
//            try {
//                log.error("sleeping before clean up");
//                Thread.sleep(5000);
//                log.error("woke up");
//            } catch (InterruptedException e) {
//                log.error("error cleaning up "+destDir);
//            }
//            FileUtils.cleanDirectory(destDir);
//        } catch (IOException ioe) {
//            log.error("the zipEntry can't be processed ", ioe);
//            LogChannel.recordInCsv(new Date(), "ZIP_INDEXING_ERROR", file.getName(), file.getPath(), ioe.getMessage(), "");
//        }
//    }
//
//
//    private File createDestinationDirForZip() {
//        File destDir = new File(workingDir+"\\temp");
//
//        if (!destDir.exists()) {
//            if (destDir.mkdir()) {
//                boolean b = destDir.setWritable(true);
//                log.info("Directory is created! "+ destDir + "; writable=" + b);
//            } else {
//                log.info("Failed to create " + destDir);
//            }
//        }
//        return destDir;
//    }
//
//    private static String extractFile(ZipInputStream zipIn, String filePath, File zipFile)  {
//        String msg="";
//        try{
//            BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(filePath));
//            byte[] bytesIn = new byte[4096];
//            int read;
//            while ((read = zipIn.read(bytesIn)) != -1) {
//                bos.write(bytesIn, 0, read);
//            }
//            bos.close();
//            return msg;
//        }catch ( IOException ee){
//            msg=ee.getMessage();
//            log.error("error during writing file "+ee.getMessage(),ee);
//            LogChannel.recordInCsv(new Date(),"TEMP_FILE_ERROR", zipFile.toString(), filePath, ee.getMessage(),"");
//            return msg;
//        }
//    }
//
//
//    private static boolean createDirIfNeeded(File destDir, ZipEntry entry, boolean firstTime) {
//        if(entry.getName().contains("/")){
//            String tt[]=entry.getName().split("/");
//            String filePath = destDir.getAbsolutePath() + File.separator + tt[0];
//            File dir = new File(filePath);
//            dir.mkdir();
//            dir.setWritable(true);
//            firstTime=false;
//        }
//        return firstTime;
//    }
//
//    private static boolean isZipFile(File file) throws IOException, TikaException {
//        TikaConfig tika = new TikaConfig();
//        Metadata metadata = new Metadata();
//        metadata.set(Metadata.RESOURCE_NAME_KEY, file.toString());
//        String mimetype = tika.getDetector().detect(
//                TikaInputStream.get(file), metadata).getSubtype();
//        return "zip".equalsIgnoreCase(mimetype);
//    }
//
//    public static void main(String[] args) throws IOException, TikaException {
//        String folder = "E:\\fs1\\index";
//        File startFolder = new File(folder);
//        File[] files = startFolder.listFiles();
//        for (File file : files) {
//        }
//    }
//
//
//
//}
