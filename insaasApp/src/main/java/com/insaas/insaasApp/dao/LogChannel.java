//package com.insaas.insaasApp.dao;
//
//
//import org.apache.commons.lang.StringEscapeUtils;
//import org.apache.commons.lang.StringUtils;
//import org.apache.log4j.Logger;
//
//import com.insaas.insaasApp.helper.PropertiesHelper;
//import com.opencsv.CSVWriter;
//
//import java.io.BufferedWriter;
//import java.io.FileWriter;
//import java.io.IOException;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//
///**
// * User: Surya
// * Date: 1/7/15
// * Time: 4:57 PM
// */
//public class LogChannel {
//
//    private static Logger log = Logger.getLogger(LogChannel.class);
//    private static String errorFile = null;
//    private static String ignoreFile = null;
//
//    public static void setUpNewIgnoreFile() {
//        String filePath=PropertiesHelper.getIgnoredLogFile();
//        ignoreFile = PropertiesHelper.addTimeStamp(filePath);
//        recordHeaderInCsvForIgnored();
//    }
//
//    public static void setUpNewErrorFile() {
//        String filePath=PropertiesHelper.getErrorLogFile();
//        errorFile = PropertiesHelper.addTimeStamp(filePath);
//        recordHeaderInCsvForError();
//    }
//
//    public static void recordInCsv(Date date,String title, String filename, String folder, String message,String zipFilePath){
//
//        String[] parts = new String[6];
//
//        SimpleDateFormat outFormatter = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a");
//        parts[0]=StringEscapeUtils.escapeJava(outFormatter.format(new Date()));
//        if(!StringUtils.equals(title,""))
//            parts[1]=StringEscapeUtils.escapeJava(title);
//        if(!StringUtils.equals(filename,""))
//            parts[2]=StringEscapeUtils.escapeJava(filename);
//        if(!StringUtils.equals(folder,""))
//            parts[3]=StringEscapeUtils.escapeJava(folder);
//        if(!StringUtils.equals(message,""))
//            parts[4]=StringEscapeUtils.escapeJava(message);
//        if(!StringUtils.equals(zipFilePath,""))
//            parts[5]=StringEscapeUtils.escapeJava(zipFilePath);
//
//        writeToFile(parts, errorFile);
//    }
//
//    private static void writeToFile(String[] parts, String filePath) {
//        try
//        {
//            FileWriter fw = new FileWriter(filePath,true); //the true will append the new data
//            BufferedWriter out = new BufferedWriter(fw);
//            CSVWriter writer = new CSVWriter(out);
//            writer.writeNext(parts);
//
//            writer.close();
//            out.close();
//            fw.close();
//        }
//        catch(IOException ioe)
//        {
//            log.error("Exception occurred during writing csv to " + filePath, ioe);
//            throw new RuntimeException("Exception occurred during writing csv to "+filePath,ioe);
//        }
//    }
//
//    public static void recordIgnoredInCsv(String filename,String realFilePath){
//
//        String[] parts = new String[3];
//        SimpleDateFormat outFormatter = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a");
//
//        parts[0]=StringEscapeUtils.escapeJava(outFormatter.format(new Date()));
//
//        if(!StringUtils.equals(filename,""))
//            parts[1]=StringEscapeUtils.escapeJava(filename);
//        if(!StringUtils.equals(realFilePath,""))
//            parts[2]=StringEscapeUtils.escapeJava(realFilePath);
//
//        writeToFile(parts, ignoreFile);
//    }
//
//    public static void main(String[] a) {
//
//        LogChannel.recordInCsv(new Date(),"A,?'bc","1234","sdmnsldknl","sldkcvnsldvn","AntExample\\src\\com\\vaannila\\helloworld\\HelloWorld.java");
//        LogChannel.recordInCsv(new Date(),"A,?'bc","1234","sdmnsldknl","sldkcvnsldvn","AntExample\\src\\com\\vaannila\\helloworld\\HelloWorld.java");
//        LogChannel.recordInCsv(new Date(),"A,?'bc","1234","sdmnsldknl","sldkcvnsldvn","AntExample\\src\\com\\vaannila\\helloworld\\HelloWorld.java");
//        LogChannel.recordInCsv(new Date(),"A,?'bc","1234","sdmnsldknl","sldkcvnsldvn","AntExample\\src\\com\\vaannila\\helloworld\\HelloWorld.java");
//
//    }
//
//    private static void recordHeaderInCsvForIgnored() {
//        String[] parts = new String[6];
//
//        parts[0]="Date";
//        parts[1] = "Title";
//        parts[2] = "Filename";
//        parts[3] = "Folder";
//        parts[4] = "Message";
//        parts[5] = "Zip File Path";
//        writeToFile(parts, ignoreFile);
//    }
//
//    private static void recordHeaderInCsvForError() {
//        String[] parts = new String[6];
//        parts[0]="Date";
//        parts[1]="File Path";
//        parts[0]="Actual File Path";
//        writeToFile(parts, errorFile);
//    }
//}
