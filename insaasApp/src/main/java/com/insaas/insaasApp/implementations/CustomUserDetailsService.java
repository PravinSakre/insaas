
package com.insaas.insaasApp.implementations;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.insaas.insaasApp.dto.UserDto;
import com.insaas.insaasApp.entity.User;
import com.insaas.insaasApp.repository.UserRepository;



/**
 * Represents an CustomUserDetailsService will validate the userName and
 * password
 * 
 * @author BrainWave Consulting
 * @since Dec 24, 2018
 */
@Service("customUserDetailsService")
public class CustomUserDetailsService implements UserDetailsService {
	@Autowired
	private UserRepository userRepository;

	private static final Logger logger = LogManager.getLogger(CustomUserDetailsService.class.getName());

	String role;

	UserDetails userDetails;

	public final String CURRENT_USER_KEY = "CURRENT_USER";

	/**
	 * This method is for getting user object
	 * 
	 * @author BrainWave Consulting
	 * @since Dec 24, 2018
	 * @param usernameOrEmail
	 * @return
	 */
	private User getMapObject(String usernameOrEmail) {
		User user = null;
		try {
			if (logger.isDebugEnabled()) {
				logger.debug("getMapObject(String) - {}", "in try"); //$NON-NLS-1$ //$NON-NLS-2$
			}

			user = userRepository.findByUserId(usernameOrEmail);

			if (logger.isDebugEnabled()) {
				logger.debug("getMapObject(String) - {}", "user==" + user); //$NON-NLS-1$ //$NON-NLS-2$
			}
		} catch (Exception e) {
			if (logger.isDebugEnabled()) {
				logger.debug("getMapObject(String) - {}", "JsonParseException :: " + e); //$NON-NLS-1$ //$NON-NLS-2$
			}
		}
		return user;

	}

	/**
	 * This method is for getting user details by email
	 * 
	 * @author BrainWave Consulting
	 * @since Dec 24, 2018
	 * @param usernameOrEmail
	 * @return
	 */

	@Override
	public UserDetails loadUserByUsername(String usernameOrEmail) throws UsernameNotFoundException {
		User account = userRepository.findByUserId(usernameOrEmail);

		if (null == account) {
			throw new UsernameNotFoundException("username not found..");

		}
		/**
		 * UserDto object
		 */
		UserDto userDto = new UserDto();

		if (usernameOrEmail.trim().isEmpty()) {
			throw new UsernameNotFoundException("username is empty");
		}

		// userPojo.setIdUserAutoGen(account.getIdUserAutoGen());
		userDto.setPassword(account.getPassword());
		userDto.setUserId(account.getUserId());
		userDto.setUserRoleId(account.getUserRoleId());

		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		authorities.add(new SimpleGrantedAuthority(String.format("ROLE_%s", userDto.getUserRoleId())));
		return new org.springframework.security.core.userdetails.User(userDto.getUserId(), userDto.getPassword(),
				authorities);
	}

	/**z
	 * This method is for validating aceess for perticular menu
	 * 
	 * @author Brainwave Consulting
	 * @since Dec 25, 2018
	 * @param menuName
	 * @return
	 * @throws UsernameNotFoundException
	 */
	public Map<String, String> validateMenuAccess(String menuName) throws UsernameNotFoundException {

		return null;

	}

}
