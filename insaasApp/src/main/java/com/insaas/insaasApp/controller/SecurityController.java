
package com.insaas.insaasApp.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.insaas.insaasApp.common.Constant;
import com.insaas.insaasApp.dto.CustomerDto;
import com.insaas.insaasApp.dto.Result;
import com.insaas.insaasApp.dto.UserDto;
import com.insaas.insaasApp.entity.CustomerDetails;
import com.insaas.insaasApp.entity.Role;
import com.insaas.insaasApp.entity.User;
import com.insaas.insaasApp.repository.CustomerDetailsRepository;
import com.insaas.insaasApp.service.SecurityService;

/**
 * This Controller will handle Security services like login, logout,
 *  forgotPassword, changePassword
 * @author BrainWave Consulting
 * @since Dec 18, 2018
 */
@CrossOrigin
@RestController
@RequestMapping("/login")
public class SecurityController {
	/**
	 * Logger object
	 */
	private static final Logger logger = LogManager.getLogger(SecurityController.class.getName());

	/**
	 * security service object will handles security services
	 */
	@Autowired
	private SecurityService securityService; 

	/**
	 * This method will validate the credentials using security service.
	 * 
	 * @author Brainwave Consulting
	 * @since Dec 18, 2018
	 * @param userName,
	 *            Entered user Name
	 * @param password,
	 *            Entered password
	 * @return failure or success with login tokens
	 * @throws Exception 
	 */
	@RequestMapping(value = "/authenticate")
	public ResponseEntity<?> doLogin(@RequestParam String userName, String password) {
		logger.debug("doLogin:username=" + userName + "\t password=" + password);
		logger.info("in doLogin controller..........");
		Result result = new Result();
		if ((userName == null || userName.isEmpty()) && (password == null || password.isEmpty())) {
			result.setMessage("Username, Password should not be Empty or null");
			return new ResponseEntity<Result>(result, HttpStatus.UNAUTHORIZED);
		}

		Map responseMap = new HashMap<>();

		try {
			String response = securityService.authenticationUsingUserData(userName, password);
			if (response == null || response.isEmpty()) {
				logger.error("response is empty");
				return new ResponseEntity<>("response is empty", HttpStatus.NO_CONTENT);
			}
			responseMap = new ObjectMapper().readValue(response, new TypeReference<Map>() {
			});
			logger.info("responseMap="+responseMap);
			if (responseMap == null) {
				logger.error("responseMap is empty");
				return new ResponseEntity<>("responseMap is empty", HttpStatus.NO_CONTENT);
			}
			logger.info("responseMap==" + responseMap);
			String loginStatus = responseMap.get("status").toString();

			if ("200 OK".equals(loginStatus)) {
				logger.info("Login successfull.....");
				
				securityService.updateSearchJsonWithCustomerCoreName(responseMap.get("core_name").toString(), Constant.SEARCH_DASHBOARD_FILE_NAME);
				return new ResponseEntity<Map<String, String>>(responseMap, HttpStatus.OK);
			} else {
				return new ResponseEntity<Map<String, String>>(responseMap, HttpStatus.NOT_ACCEPTABLE);
			}
		} catch (Exception e) {
			logger.error("Exception in dologin method.." + e);
			e.printStackTrace();
			result.setMessage(e.getMessage());
			return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
		}
	}
	
	@RequestMapping(value="/addUser")
	public ResponseEntity<String> addUserData(@RequestBody UserDto userDto){
		logger.info("in add user controller method...data="+userDto);
		String result="";
		try {
		 result=securityService.addUser(userDto);
		 logger.info("result="+result);
		 return new ResponseEntity<String>(result,HttpStatus.OK);
		}catch(Exception e) {
			logger.error("exception is",e);
			result=e.getMessage();
			return new ResponseEntity<String>(result,HttpStatus.BAD_REQUEST);
		}
		
	}
			
	
	
	
	
	

	/**
	 * This method is used for logOut from system.
	 * 
	 * @author Brainwave Consulting
	 * @since Dec 18, 2018
	 * @param accessToken
	 * @return
	 */
	@RequestMapping(value = "/logout")
	public ResponseEntity<?> logoutUser(@RequestParam String accessToken) {
		logger.debug("logoutUser:accessToken" + accessToken);
		if ((accessToken == null || accessToken.isEmpty())) {
			return new ResponseEntity<>(true, HttpStatus.OK);
		}
		Boolean responseOfLogout = false;
		try {
			return new ResponseEntity<>(responseOfLogout, HttpStatus.OK);
		} catch (Exception e) {
			logger.error("Exception in logoutUser method..." + e);
			e.printStackTrace();
			return new ResponseEntity<>(responseOfLogout, HttpStatus.BAD_REQUEST);
		}
	}

	/**
	 * This method will convert object into json String
	 * 
	 * @author Brainwave Consulting
	 * @since Dec 18, 2018
	 * @param userName
	 * @param password
	 * @return
	 */
	private String getJsonOfObject(Object Object) {
		logger.debug("getJsonOfObject:Object=" + Object);
		ObjectMapper objectMapper = new ObjectMapper();
		String jsonString = "";
		try {
			jsonString = objectMapper.writeValueAsString(Object);
		} catch (Exception e) {
			logger.error("Exception in getJsonOfObject method " + e);
			e.printStackTrace();
		}
		return jsonString;
	}



	/**
	 * This method will used for forgot passwrod using service it will remove
	 * accssToken
	 * 
	 * @author BrainWave Consulting
	 * @since Dec 18, 2018
	 * @param email
	 * @return ResponseEntity<?>
	 *
	 */
	@RequestMapping(value = "/forgotPassword")
	public ResponseEntity<?> forgotPasword(@RequestParam String email) {
		logger.info("forgotPasword:email=" + email);
		Result result = new Result();
		if ((email == null || email.isEmpty())) {
			result.setMessage("Invalid email Id");
			return new ResponseEntity<Result>(result, HttpStatus.BAD_REQUEST);
		}
		Map<String, String> forgotPasswordResponseMap = new HashMap<>();
		try {
			forgotPasswordResponseMap = securityService.forgotPassword(email);
		} catch (Exception e) {
			logger.error("Exception in forgotPasword method.." + e);
		}
		return new ResponseEntity<Map<String, String>>(forgotPasswordResponseMap, HttpStatus.OK);
	}
	@RequestMapping(value = "/addForgotPassword")
	public ResponseEntity<?> addForgotPasword(@RequestParam String accessToken,String password) {
		logger.info("forgotPasword:email=" + password);
		Result result = new Result();
		if ((password == null || password.isEmpty())) {
			result.setMessage("Invalid email Id");
			return new ResponseEntity<Result>(result, HttpStatus.BAD_REQUEST);
		}
		String forgotPasswordResult = "";
		try {
			forgotPasswordResult = securityService.addForgotPassword(accessToken,password);
			result.setMessage(forgotPasswordResult);
			logger.info("forgotPasswordResult="+forgotPasswordResult);
		} catch (Exception e) {
			
			logger.error("Exception in forgotPasword method.." + e);
			e.printStackTrace();
			result.setMessage(e.getMessage());
			return new ResponseEntity<Result>(result, HttpStatus.UNPROCESSABLE_ENTITY);
	}
		return new ResponseEntity<Result>(result, HttpStatus.OK);
	}

	/**
	 * this will used for change password by requested user.
	 * 
	 * @author BrainWave Consulting
	 * @since Dec 18, 2018
	 * @param accessToken
	 * @param password
	 * @return ResponseEntity<?>
	 *
	 */
	@RequestMapping(value = "/changePassword")
	public ResponseEntity<?> changePassword(@RequestParam String accessToken, String password) {
		logger.debug("changePassword:accessToken=" + accessToken + "\t password=" + password);
		Result result = new Result();
		if ((accessToken == null || accessToken.isEmpty()) && (password == null || password.isEmpty())) {
			result.setMessage("Invalid accessToken or password");
			return new ResponseEntity<Result>(result, HttpStatus.BAD_REQUEST);
		}
		Map<String, String> responseMap = new HashMap<>();
		try {
			responseMap = securityService.changePassword(accessToken, password);
			logger.info("Password changed successfully......");
		} catch (Exception e) {
			logger.error("Exception in change password method=" + e);
			e.printStackTrace();
			result.setMessage(e.getMessage());
			return new ResponseEntity<Result>(result, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<Map<String, String>>(responseMap, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/allUser")
	public List<UserDto> getAllUsers() 
	{
		logger.info("in all user controller");
		List<UserDto> userList=new ArrayList();
		try {
			userList=securityService.getUserData();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return userList;
	}
	@RequestMapping(value = "/allRole")
	public List<Role> getAllRoles() 
	{
		logger.info("in all Role controller");
		List<Role> roleList=new ArrayList();
		try {
			roleList=securityService.getRoleData();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return roleList;
	}
	
	
	
	
	@RequestMapping(value="/deleteUser")
	public Integer deleteUser(Integer idUserAutoGen)
	{
		logger.info("in deleteUSER Controller");
		Integer result = null;
		logger.info("in deleteUSER controller data is="+idUserAutoGen);
		 try {
			result=securityService.deleteUserData(idUserAutoGen);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return result;
		
	}
	
	@RequestMapping(value="/getSingleUser")
	public UserDto getSingleUser(Integer idUserAutoGen) throws Exception
	{
		//String result = " ";
		//User user=new User();
		logger.info("in getSingleUser controller data is="+idUserAutoGen);
		
		UserDto user=securityService.getSingleUser(idUserAutoGen);
		
			// TODO Auto-generated catch block
			
		
		
		return user;
		
	}
	
	@RequestMapping(value="/updateUser")
	public ResponseEntity<String> updateUserData(UserDto userDto){
		logger.info("in updateUserData  controller method...data="+userDto);
		String result="";
		try {
		 result=securityService.updateUser(userDto);
		 logger.info("result="+result);
		 return new ResponseEntity<String>(result,HttpStatus.OK);
		}catch(Exception e) {
			logger.error("exception is",e);
			result=e.getMessage();
			return new ResponseEntity<String>(result,HttpStatus.BAD_REQUEST);
		}
		
	}

	
	
	
}