//package com.insaas.insaasApp.controller;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.CrossOrigin;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//import com.insaas.insaasApp.dto.QueryParamsDto;
//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;
//import org.apache.solr.common.SolrDocumentList;
//
//import com.insaas.insaasApp.service.QueryBuilderService;
//
//@CrossOrigin
//@RestController
//@RequestMapping("/queryBuilder")
//public class QueryController {
//	@Autowired
//	private QueryBuilderService QueryBuilderService;
//	private static Logger logger = LogManager.getLogger(QueryController.class.getName());
//
//	@RequestMapping(value = "/queryData")
//	public ResponseEntity<?> queryData(@RequestParam String queryParams) {
//		SolrDocumentList solrQueryFromData = null;
//		try {
//			logger.info("queryParams=" + queryParams);
//			QueryParamsDto queryParamsDto = new QueryParamsDto();
//			// queryParamsDto.setFacetQuery(queryParams);
//			// String facetFields[]= {"User_Name"};
//			// String facetPivotFields[]={"Sentiment","User_Name"};
//			// String filterQueryParams[]={"Sentiment:Positive"};
//			// String
//			// fieldList[]={"id","User_Name","Feedback_Message","Individual_Rating","Useful_for","Aspect1_Sentiment","Total_Rating","Gender","Feedback_Date"};
//			queryParamsDto.setqParam("q");
//			queryParamsDto.setqValue(queryParams);
//			queryParamsDto.setFlagFacet(true);
//			queryParamsDto.setFacetQuery(queryParams);
//
//			// queryParamsDto.setFacetFields(facetFields);
//			// queryParamsDto.setFacetPivotFields(facetPivotFields);
//			// queryParamsDto.setFilterQueryParams(filterQueryParams);
//			// queryParamsDto.setFlafDistribute(true);
//			// queryParamsDto.setFlagFacet(true);
//			// queryParamsDto.setFlagHightlight(true);
//			// queryParamsDto.setSimpleFields(fieldList);
//			// queryParamsDto.setTermsField("");
//			// queryParamsDto.setTimeAllowed(898);
//			solrQueryFromData = QueryBuilderService.solrQuery(queryParamsDto);
//			logger.info("solrQueryFromData==" + solrQueryFromData);
//		} catch (Exception e) {
//			logger.error("Exception is=", e);
//		}
//		return new ResponseEntity<SolrDocumentList>(solrQueryFromData, HttpStatus.OK);
//	}
//}