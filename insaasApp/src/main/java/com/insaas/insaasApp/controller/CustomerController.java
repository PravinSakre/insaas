package com.insaas.insaasApp.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.insaas.insaasApp.dto.CustomerDto;
import com.insaas.insaasApp.entity.Customer;
import com.insaas.insaasApp.entity.CustomerDetails;
import com.insaas.insaasApp.service.CustomerService;
import com.insaas.insaasApp.serviceImpl.CustomerServiceImpl;


@CrossOrigin
@RestController
@RequestMapping("/customerController")
public class CustomerController {
	private static final Logger logger = LogManager.getLogger(CustomerController.class.getName());
	@Autowired
	private CustomerService customerService;
	
	
	@RequestMapping(value = "/allCustomer")
	public List<CustomerDto> getAllCutomers() 
	{
		List<CustomerDto>  customerDetailsDtoList=new ArrayList();
		customerDetailsDtoList=customerService.getAllCutomers();

   return customerDetailsDtoList;
	}
	
	@RequestMapping(value="/addCustomer")
	public ResponseEntity<String> addCustomerData(@RequestBody CustomerDto customerDto){
		logger.info("in add customer controller method...data="+customerDto);
		String result="";
		try {
		 result=customerService.addCustomerData(customerDto);
		 logger.info("result="+result);
		 return new ResponseEntity<String>(result,HttpStatus.OK);
		}catch(Exception e) {
			logger.error("exception is",e);
			result=e.getMessage();
			return new ResponseEntity<String>(result,HttpStatus.BAD_REQUEST);
		}
		
	}
	
/*	@RequestMapping(value="/deleteCustomer")
	ResponseEntity<String> deleteCustomer(@RequestBody String customerId) {
		
		logger.info("in deleteCustomer controller data is="+customerId);
		
		String result="";
		return new ResponseEntity<String>(result,HttpStatus.OK);
		
	}*/
	@RequestMapping(value="/deleteCustomer")
	public Integer deleteCustomer(Integer custDetailsId)
	{
		Integer result = null;
		logger.info("in deleteCustomer controller data is="+custDetailsId);
		 try {
			result=customerService.deleteCustomerData(custDetailsId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return result;
		
	}
	
	@RequestMapping(value="updateCustomer")
	public ResponseEntity<String> updateCustomer(CustomerDto customerDto) {
		logger.info("in update customer");
		String result="";
		logger.info("in update customer controller data is="+customerDto.toString());
		try {
		 result=customerService.updateCustomer(customerDto);
		logger.info("result in update customer="+result);
		return new ResponseEntity<String>(result,HttpStatus.OK);
		}
		catch(Exception e) {
			logger.error("Exeption while updating customer is",e);
			return new ResponseEntity<String>(result,HttpStatus.BAD_REQUEST);
		}
	
	}
//	@RequestMapping(value = "/getSingleCustomerData")
//	public List<CustomerDto> getSingleCustomerData(Integer customerId) throws Exception 
//	{
//		//CustomerDto customerDto = new CustomerDto();
//		logger.info(" im in getSingleCustomerData controller : "+customerId);
////		CustomerDetails customerDetails=new CustomerDetails();
//		List<CustomerDto> customerDetailslist=customerService.getSingleCustomerData(customerId);
//		logger.info("get Controller data : "+customerDetailslist.toString());
//         return customerDetailslist;
//	}
	@RequestMapping(value = "/getSingleCustomerData")
	public CustomerDto getSingleCustomerData(Integer custDetailsId) throws Exception 
	{
		//CustomerDto customerDto = new CustomerDto();
		logger.info(" im in getSingleCustomerData controller : "+custDetailsId);
		
//		CustomerDetails customerDetails=new CustomerDetails();
		CustomerDto customerDetailslist=customerService.getSingleCustomerData(custDetailsId);
		logger.info("get Controller data : "+customerDetailslist.toString());
         return customerDetailslist;
	}
	
	
	

}
