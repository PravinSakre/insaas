package com.insaas.insaasApp.controller;

// import java.util.ArrayList;
// import java.util.List;
// import java.util.Map;
// import org.apache.solr.client.solrj.SolrClient;
// import org.apache.solr.client.solrj.SolrQuery;
// import org.apache.solr.client.solrj.impl.HttpSolrClient;
// import org.springframework.data.domain.PageRequest;
// import org.springframework.data.domain.Pageable;
// import org.springframework.data.solr.core.query.Criteria;
// import org.springframework.data.solr.core.query.Join;
// import org.springframework.data.solr.core.query.SimpleFilterQuery;
// import org.springframework.data.solr.core.query.SimpleQuery;
// import org.springframework.data.solr.core.query.SolrDataQuery;
// import org.springframework.data.solr.repository.Query;
// import com.insaas.insaasApp.pojo.SolrDocument;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.insaas.insaasApp.pojo.SolrSearchFields;
import com.insaas.insaasApp.repository.SolrAdacDocumentRepository;
import com.insaas.insaasApp.service.SecurityService;
import com.insaas.insaasApp.service.SolrService;

@CrossOrigin
@RestController
@RequestMapping("/solrController")
public class SolrController {

	private static final Logger logger = LogManager.getLogger(SolrController.class.getName());

	@Autowired
	SolrService solrService;

	@Autowired
	SolrAdacDocumentRepository solrAdacDocumentRepository;
	
	@Autowired
	private SecurityService securityService;


	@RequestMapping(value = "/test")
	public ResponseEntity<?> getResult() {
		return new ResponseEntity<String>("success", HttpStatus.OK);
	}

	

	
	@RequestMapping(value = "/updateDocument")
	public ResponseEntity<?> updateDocument(@RequestParam String documents, String accessToken) {
		logger.info("updated Dcoument controller..");
		String responseMsg = "";
		try {
			if (accessToken == null || accessToken.isEmpty())
				return new ResponseEntity<String>("Session Expired..", HttpStatus.UNAUTHORIZED);

			if (!securityService.validateAccessToken(accessToken))
				return new ResponseEntity<String>("Session Expired..", HttpStatus.UNAUTHORIZED);
			
			responseMsg = solrService.updateSolrDocuments(documents.toString(), accessToken);
			if(responseMsg == null) {
				return new ResponseEntity<String>("", HttpStatus.NOT_FOUND);
			}
			
			if(responseMsg.isEmpty()) {
				throw new Exception("Response msg not found");
			}
			return new ResponseEntity<String>(responseMsg, HttpStatus.OK);
			// return null;
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.METHOD_FAILURE);
		}
	}

	@RequestMapping(value = "/solrSearch")
	public ResponseEntity<?> solrSearch(@RequestParam String solrSearchFields) {
		logger.info("searchRecordByAspect accessToken :: "+ solrSearchFields);
		Gson g = new Gson();
		SolrSearchFields fields = g.fromJson(solrSearchFields, SolrSearchFields.class);
		logger.info("searchRecordByAspect accessToken :: "+ fields.toString());
		logger.info("in connect controller.........");
		try {
			if (fields.getAccessToken() == null || fields.getAccessToken().isEmpty())
				return new ResponseEntity<String>("Session Expired..", HttpStatus.UNAUTHORIZED);

			if (!securityService.validateAccessToken(fields.getAccessToken()))
				return new ResponseEntity<String>("Session Expired..", HttpStatus.UNAUTHORIZED);
			
			return new ResponseEntity<Object>(solrService.solrSearch(fields), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.METHOD_FAILURE);
		}
	}

	@RequestMapping(value = "/massDocumentUpdate")
	public ResponseEntity<?> massDocumentUpdate(@RequestParam String solrSearchFields, String accessToken) {
		logger.info("updated Dcoument controller..");
		String responseMsg = "";
		try {
			if (accessToken == null || accessToken.isEmpty())
				return new ResponseEntity<String>("Session Expired..", HttpStatus.UNAUTHORIZED);

			if (!securityService.validateAccessToken(accessToken))
				return new ResponseEntity<String>("Session Expired..", HttpStatus.UNAUTHORIZED);
			
			responseMsg = solrService.updateMassSolrDocuments(solrSearchFields.toString(), accessToken);
			if (responseMsg == null) {
				return new ResponseEntity<String>("No data found", HttpStatus.NOT_FOUND);
			}

			if (responseMsg.isEmpty()) {
				return new ResponseEntity<String>("No data found", HttpStatus.NOT_FOUND);
			}
			return new ResponseEntity<String>(responseMsg, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.METHOD_FAILURE);
		}
	}

}
