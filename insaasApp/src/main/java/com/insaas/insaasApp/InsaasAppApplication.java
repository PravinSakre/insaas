package com.insaas.insaasApp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.insaas.insaasApp.bean.SecurityInformationBean;

@SpringBootApplication
public class InsaasAppApplication {
	public static void main(String[] args) throws Exception {
		
		SpringApplication.run(InsaasAppApplication.class, args); 
		
	}
	
	  @Bean(name = "securityBean") public SecurityInformationBean createBean() {
	  return new SecurityInformationBean(); 
	  
	  }
}
 