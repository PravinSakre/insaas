package com.insaas.insaasApp.service;

import com.insaas.insaasApp.dto.CustomerDto;
import com.insaas.insaasApp.entity.Customer;
import com.insaas.insaasApp.entity.CustomerDetails;

import java.util.List;


public interface CustomerService {
	
	public List<CustomerDto> getAllCutomers(); 
	public String addCustomerData(CustomerDto customerDto) throws Exception;

	public String updateCustomer(CustomerDto customerDto) throws Exception ;
	//public CustomerDetails getSingleCustomerData(Integer custDetailsId) throws Exception;
	public CustomerDetails getCustomer();
	public CustomerDto getSingleCustomerData(Integer customerId) throws Exception;
	public Integer deleteCustomerData(Integer custDetailsId)throws Exception;

	
	
 }
