package com.insaas.insaasApp.service;

import java.util.List;

import com.insaas.insaasApp.dto.UserDto;

public interface UserService {
	public String addUser(UserDto userDto) throws Exception;

}
