package com.insaas.insaasApp.service;

/**
 * 
 * This Class will handle Configuration services like prepare & getServer
 * Configurations
 * 
 * @author BrainWave Consulting
 * @since Dec 18, 2018
 */
public interface ConfigurationService {
	/**
	 * This method will call getServerConfiguration service Implement method.
	 * 
	 * @author BrainWave Consulting
	 * @since Dec 19, 2018
	 * @param configurationName
	 * @return String
	 *
	 */
	public String getServerConfiguration(String configurationName);

	/**
	 * This method will call prepareConfiguration service Implement method.
	 * 
	 * @author BrainWave Consulting
	 * @since Dec 19, 2018
	 * @param xmlFilePath
	 *            void
	 *
	 */
	public void prepareConfiguration(String xmlFilePath);

}
