package com.insaas.insaasApp.service;

import java.util.List;
import java.util.Map;

import com.insaas.insaasApp.dto.UserDto;
import com.insaas.insaasApp.entity.Role;
import com.insaas.insaasApp.entity.User;


/**
 * 
 * This Class will handle Login services like authentication, validation,
 * logOut, forgot, change password
 * 
 * @author BrainWave Consulting
 * @since Dec 18, 2018
 */

public interface SecurityService {

	/**
	 * this method will call authenticationUsingUserData service Implement method.
	 * 
	 * @author BrainWave Consulting
	 * @since Dec 18, 2018
	 * @param username
	 * @param password
	 * @return String
	 *
	 */
	public String authenticationUsingUserData(String username, String password) throws Exception;
	public Map<String, String> changePassword(String accessToken, String password) throws Exception;
	public Map<String, String> forgotPassword(String email);
	public String addForgotPassword(String accessToken,String password) throws Exception;
	public String addUser(UserDto userDto) throws Exception;
	public List<UserDto> getUserData()throws Exception;
	public Integer deleteUserData(int idUserAutoGen)throws Exception;
	public UserDto getSingleUser(int idUserAutoGen)throws Exception;
	public String updateUser(UserDto userDto) throws Exception;
	public List<Role> getRoleData() throws Exception;
	
	public void updateSearchJsonWithCustomerCoreName(String coreName, String dashboardType) throws Exception;
	public boolean validateAccessToken(String accessToken) throws Exception;
	public Object getUserId(String accessToken)throws Exception;
}