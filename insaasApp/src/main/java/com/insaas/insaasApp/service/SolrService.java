package com.insaas.insaasApp.service;

import org.apache.solr.client.solrj.SolrClient;
import org.springframework.data.domain.Page;
import com.insaas.insaasApp.pojo.SolrSearchFields;
import com.insaas.insaasApp.solr.core.document.SolrAdacDocument;

/**
 * This will help for connect & and execution from solr
 * 
 * @author Brainwave Consulting
 *
 */
public interface SolrService {

	public SolrClient connectToSolrServer(String urlString, String accessToken) throws Exception;

	public Object solrSearch(SolrSearchFields solrSearchFields) throws Exception;

	public String updateSolrDocuments(String documents, String accessToken) throws Exception;

	public String updateMassSolrDocuments(String documents, String accessToken) throws Exception;
}
