package com.insaas.insaasApp.service;

import java.util.List;

import com.insaas.insaasApp.entity.CustomerDetails;

public interface CustomerDetailsService {
	
	public List<CustomerDetails> getCustomerDetailsList() throws Exception;

}
