package com.insaas.insaasApp.service;
/**
 * 
 * Represents an MessageService will handles email and SMS sending
 * @author BrainWave Consulting
 * @since Dec 25, 2018
 */
public interface MessageService {
	/**
	 * This method is for sending the mail
	 * @author Brainwave Consulting
	 * @since Dec 25, 2018
	 * @param configuration
	 * @param mailFrom
	 * @param mailTo
	 * @param subject
	 * @param message
	 */
	public void sendMail(String configuration, String mailFrom, String mailTo, String subject, String message);
	
}
