package com.insaas.insaasApp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.insaas.insaasApp.entity.Role;



/**
 * 
 * Represents an RoleRepository will handles User requests
 * @author BrainWave Consulting
 * @since Dec 24, 2018
 */
@Repository
public interface RoleRepository extends CrudRepository<Role, Long> {
	/**
	 * This method will find role data by role and status
	 * @author Brainwave Consulting
	 * @since Dec 24, 2018
	 * @param userRole
	 * @param active
	 * @return
	 * @throws Exception
	 */
	public Role findRoleByUserRoleAndActive(String userRole, boolean active) throws Exception;

	/**
	* This method will find all roles
	 * @author Brainwave Consulting
	 * @since Dec 24, 2018 
	 */
	public List<Role> findAll();
	
//	@Query(value = "SELECT user_Role FROM role1 r", nativeQuery = true)
//	public List<String> findAllByuserRole();

	/**
	 * This method will find role by role Id
	 * @author Brainwave Consulting
	 * @since Dec 24, 2018
	 * @param roleId
	 * @return
	 * @throws Exception
	 */
	public Role findByRoleId(Integer roleId) throws Exception;
	public Role findRoleByuserRole(String userRole) throws Exception;
	
}
