
package com.insaas.insaasApp.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.insaas.insaasApp.entity.User;

/**
 * 
 * Represents an UserRepository will handle Crud operations on User table
 * 
 * @author BrainWave Consulting
 * @since Dec 24, 2018
 */
@Repository
public interface UserRepository extends CrudRepository<User, Long> {
	/**
	 * This method will find all users by active status
	 * 
	 * @author Brainwave Consulting
	 * @since Dec 24, 2018
	 * @param active
	 * @return
	 */

	public List<User> findAllByActive(Boolean active);
	public List<User> findAll();

	/**
	 * This method will find user object by userId
	 * 
	 * @author Brainwave Consulting
	 * @since Dec 24, 2018
	 * @param userId
	 * @return
	 */
	public User findByUserId(String userId);

	/**
	 * This method will find user object by email
	 * 
	 * @author Brainwave Consulting
	 * @since Dec 24, 2018
	 * @param email
	 * @return
	 */
	
	public User findByUserFnameAndActive(String userFname, boolean active);
	public User findByEmailId(String email);
	public User findByUserFname(String userFname);

	public User findByIdUserAutoGen(Integer idUserAutoGen);
}
