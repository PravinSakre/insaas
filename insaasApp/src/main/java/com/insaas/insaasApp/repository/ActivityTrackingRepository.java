package com.insaas.insaasApp.repository;



import org.springframework.data.repository.CrudRepository;

import com.insaas.insaasApp.entity.ActivityTracking;


/**
 * Represents an ActivityTrackingRepository will handle Crud operations on ActivityTracking table
 * @author BrainWave Consulting
 * @since Dec 24, 2018
 */
public interface ActivityTrackingRepository extends CrudRepository<ActivityTracking, Long>{
	/**
	 * This method is for getting ActivityTracking data from database by access token
	 * @author Brainwave Consulting
	 * @since Dec 24, 2018
	 * @param accessToken
	 * @return
	 */
	public ActivityTracking findByAccessToken(String accessToken);

}
