package com.insaas.insaasApp.repository;

import java.util.Optional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.solr.core.query.result.FacetPage;
import org.springframework.data.solr.repository.Facet;
import org.springframework.data.solr.repository.Highlight;
import org.springframework.data.solr.repository.Query;
import org.springframework.data.solr.repository.SolrCrudRepository;
import org.springframework.stereotype.Repository;

import com.insaas.insaasApp.solr.core.document.SolrAdacDocument;

@Repository
public interface SolrAdacDocumentRepository extends SolrCrudRepository<SolrAdacDocument, String> {
/**
 * @author Brain labs pvt ltd
 *  
 * @param sentiment
 * @param pageable
 * @return
 */
	@Query("Sentiment:?0")
	@Facet(fields = { "Aspect"}, limit = 10)
	public FacetPage<SolrAdacDocument> findBySentimentAndFacetOnAspect(String sentiment, Pageable pageable);
	
	@Query("*:*")
	@Facet(fields = { "Aspect"}, limit = 10)
	public FacetPage<SolrAdacDocument> findByAspectContainsAndFeedbackContainsFacetOnAspect(String aspect, String feedback, Pageable pageable);

	@Query("*:*")
	@Facet(fields = { "Aspect"}, limit = 10)
	@Highlight(fields = {"Feedback"}, prefix = "<b>", postfix = "</b>")
	public FacetPage<SolrAdacDocument> findByAspectAndFacetOnAspect(String aspect, Pageable pageable);
	
	@Query("Aspect:?0")
	@Highlight(fields = {"Feedback"}, prefix = "<u>", postfix = "<u>")
	public FacetPage<SolrAdacDocument> findByAspect(String aspect, Pageable pageable);

	
	@Query("Feedback:?0")
	@Facet(fields = { "Aspect" }, limit = 10)
	@Highlight(fields = {"Feedback"}, prefix = "<b>", postfix = "</b>")
	public FacetPage<SolrAdacDocument> findByFeedbackFacetOnAspect(String feedback, Pageable pageable);
	
	public Optional<SolrAdacDocument> findById(String id);
	

}
