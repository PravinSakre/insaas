package com.insaas.insaasApp.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.insaas.insaasApp.entity.Customer;
import com.insaas.insaasApp.entity.CustomerDetails;

@Repository
public interface CustomerDetailsRepository extends CrudRepository<CustomerDetails, Long>	 {
	
	//public List<CustomerDetails> findAll();
	//public CustomerDetails findByCustDetailsId(Integer CustDetailsId);
	
	
	
//	public List<CustomerDetails> findCustomerDetailsByCustomerId(Integer customerId);
	public CustomerDetails findCustomerIdByCustDetailsId(Integer custDetailsId);
	public List<CustomerDetails> findCustomerDetailsByCustomerIdAndActive(Integer customerId,boolean active);
	public CustomerDetails findByCustDetailsId(Integer custDetailsId);  
	
	public CustomerDetails findByCustomerId(Integer customerId); 
	public List<CustomerDetails> findAllByCustomerId(Integer customerId); 
	public List<CustomerDetails> findAllByActive(boolean active);
	public List<CustomerDetails> findAllByActiveAndCustomerId(boolean active,Integer customerId);
	//public CustomerDetails findBycustDetailsId(Integer custDetailsId);
	
	
}
