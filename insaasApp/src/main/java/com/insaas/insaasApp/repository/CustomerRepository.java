package com.insaas.insaasApp.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.insaas.insaasApp.entity.Customer;
import com.insaas.insaasApp.entity.CustomerDetails;


@Repository
public interface CustomerRepository extends CrudRepository<Customer, Long> {
	
	public List<Customer> findAll();
	public Customer findByCustomerName(String customerName);
	public Customer findByCustomerId(Integer customerId);
	public List<Customer> findAllByActive(boolean active); 
	public Customer findByCustomerIdAndActive(Integer customerId,boolean active);
	
	
	
	

}
