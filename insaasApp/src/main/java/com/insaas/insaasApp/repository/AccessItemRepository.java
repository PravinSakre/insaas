
package com.insaas.insaasApp.repository;

import org.springframework.data.repository.CrudRepository;

import com.insaas.insaasApp.entity.AccessItemMaster;
/**
 * Represents an AccessItemRepository will handle Crud operations on AccessItemMaster table
 * @author BrainWave Consulting
 * @since Dec 24, 2018
 */
public interface AccessItemRepository extends CrudRepository<AccessItemMaster, Long> {
	/**
	 * This method is for accessing AccessItemMaster data by item name
	 * @author Brainwave Consulting
	 * @since Dec 24, 2018
	 * @param accessItemName
	 * @return
	 */
	public AccessItemMaster findAccessItemMasterByAccessItemName(String accessItemName); 
	/**
	 * This method is for accessing AccessItemMaster data by item id
	 * @author Brainwave Consulting
	 * @since Jan 10,19
	 * @param accessItemName
	 * @return
	 */
	public AccessItemMaster findByAccessItemId(Integer accessItemId); 
	
	/**
	 * This method is for AccessItemrecord AccessItemMaster and accessItem type
	 * @author Brainwave Consulting
	 * @since Jan 10,19
	 * @param accessItemName
	 * @return
	 */
	public AccessItemMaster findByAccessItemIdAndAccessItemType(Integer accessItemId, String accessItemType);
	
	/**
	 * This method is for get accessItem master by type
	 * @author Brainwave Consulting
	 * @since Jan 10,19
	 * @param accessItemName
	 * @return
	 */
	public AccessItemMaster findByAccessItemType(String accessItemType);
}
