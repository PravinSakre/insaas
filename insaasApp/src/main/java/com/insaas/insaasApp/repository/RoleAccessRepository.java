
package com.insaas.insaasApp.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.insaas.insaasApp.entity.RoleAccess;

/**
 * Represents an RoleAccessRepository will handle Crud operations on RoleAccess table
 * @author BrainWave Consulting
 * @since Dec 24, 2018
 */
@Repository
public interface RoleAccessRepository extends CrudRepository<RoleAccess, Integer> {
	/**
	 * This method will find role by roleId
	 * @author Brainwave Consulting
	 * @since Dec 24, 2018
	 * @param roleId
	 * @param accessItemId
	 * @return RoleAccess object
	 * @throws Exception
	 */
	public RoleAccess findRoleAccessByRoleIdAndAccessItemId(Integer roleId, Integer accessItemId) throws Exception;
	/**
	 * This method will return list of role accesses by role id
	 * @author Brainwave Consulting
	 * @since Jan 10, 2019
	 * @param roleId
	 * @return
	 * @throws Exception
	 */
	public List<RoleAccess> findRoleAccessByRoleId(Integer roleId) throws Exception;
}
