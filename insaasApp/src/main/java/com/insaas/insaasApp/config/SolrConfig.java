package com.insaas.insaasApp.config;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.solr.core.SolrTemplate;
import org.springframework.data.solr.repository.config.EnableSolrRepositories;

@Configuration
@EnableSolrRepositories(basePackages = "com.insaas.insaasApp.repository")
@ComponentScan
public class SolrConfig {
	@Autowired
	PropertyFileConfig propertyFileConfig;
	@Bean
    public SolrClient solrClient() {
        return new HttpSolrClient.Builder(propertyFileConfig.getSolrServerUrl()).build();
        		//new HttpSolrClient("http://localhost:8983/solr", null, null, false);
    }
 
    @Bean
    public SolrTemplate solrTemplate(SolrClient client) throws Exception {
        return new SolrTemplate(client);
    }

}
