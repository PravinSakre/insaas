
package com.insaas.insaasApp.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * Represents an PropertyFileConfig will define configurations
 * 
 * @author BrainWave Consulting
 * @since Dec 24, 2018
 */
@Configuration
public class PropertyFileConfig {

	@Value("${mail.config.xml.file.path}")
	private String xmlFilePath;

	@Value("${forgot.password.mail.from}")
	private String mailFrom;

	@Value("${forgot.password.mail.to.user}")
	private String mailToUser;

	@Value("${forgot.password.mail.to.user.subject}")
	private String mailToUserSubject;

	@Value("${forgot.password.mail.to.user.message}")
	private String mailToUserMessage;

	@Value("${forgot.password.notification.to.admin}")
	private String mailToAdmin;

	@Value("${forgot.password.notification.to.admin.subject}")
	private String mailToAdminSubject;

	@Value("${forgot.password.notification.to.admin.message}")
	private String mailToAdminMessage;

	@Value("${access.token.validity}")
	private Integer accessTokenValidity;

	@Value("${solr.server.url}")
	private String solrServerUrl;
	
//	insaas.dashboard.file.path
	@Value("${insaas.dashboard.file.path}")
	private String insaasDashboardFilePath;
	
	public String getInsaasDashboardFilePath() {
		return insaasDashboardFilePath;
	}

	public void setInsaasDashboardFilePath(String insaasDashboardFilePath) {
		this.insaasDashboardFilePath = insaasDashboardFilePath;
	}

	@Value("${solr.server.create.core.bat.file.path}")
	private String solrServerCreateCoreBatFilePath;
	

	public String getSolrServerCreateCoreBatFilePath() {
		return solrServerCreateCoreBatFilePath;
	}

	public void setSolrServerCreateCoreBatFilePath(String solrServerCreateCoreBatFilePath) {
		this.solrServerCreateCoreBatFilePath = solrServerCreateCoreBatFilePath;
	}
	
	@Value("${solr.server.inputDocument.fields}")
	private String solrServerInputDocumentFields;
	

	public String getSolrServerInputDocumentFields() {
		return solrServerInputDocumentFields;
	}

	public void setSolrServerInputDocumentFields(String solrServerInputDocumentFields) {
		this.solrServerInputDocumentFields = solrServerInputDocumentFields;
	}

	public String getSolrServerUrl() {
		return solrServerUrl;
	}

	public void setSolrServerUrl(String solrServerUrl) {
		this.solrServerUrl = solrServerUrl;
	}

	@Value("${solr.server.csv.file.path}")
	private String solrServerCsvFilePath;
	
	public String getSolrServerCsvFilePath() {
		return solrServerCsvFilePath;
	}

	public void setSolrServerCsvFilePath(String solrServerCsvFilePath) {
		this.solrServerCsvFilePath = solrServerCsvFilePath;
	}

	/**
	 * @return the xmlFilePath
	 */
	public String getXmlFilePath() {
		return xmlFilePath;
	}

	/**
	 * @param xmlFilePath
	 *            the xmlFilePath to set
	 */
	public void setXmlFilePath(String xmlFilePath) {
		this.xmlFilePath = xmlFilePath;
	}

	/**
	 * @return the mailFrom
	 */
	public String getMailFrom() {
		return mailFrom;
	}

	/**
	 * @param mailFrom
	 *            the mailFrom to set
	 */
	public void setMailFrom(String mailFrom) {
		this.mailFrom = mailFrom;
	}

	/**
	 * @return the mailToUser
	 */
	public String getMailToUser() {
		return mailToUser;
	}

	/**
	 * @param mailToUser
	 *            the mailToUser to set
	 */
	public void setMailToUser(String mailToUser) {
		this.mailToUser = mailToUser;
	}

	/**
	 * @return the mailToUserSubject
	 */
	public String getMailToUserSubject() {
		return mailToUserSubject;
	}

	/**
	 * @param mailToUserSubject
	 *            the mailToUserSubject to set
	 */
	public void setMailToUserSubject(String mailToUserSubject) {
		this.mailToUserSubject = mailToUserSubject;
	}

	/**
	 * @return the mailToUserMessage
	 */
	public String getMailToUserMessage() {
		return mailToUserMessage;
	}

	/**
	 * @param mailToUserMessage
	 *            the mailToUserMessage to set
	 */
	public void setMailToUserMessage(String mailToUserMessage) {
		this.mailToUserMessage = mailToUserMessage;
	}

	/**
	 * @return the mailToAdmin
	 */
	public String getMailToAdmin() {
		return mailToAdmin;
	}

	/**
	 * @param mailToAdmin
	 *            the mailToAdmin to set
	 */
	public void setMailToAdmin(String mailToAdmin) {
		this.mailToAdmin = mailToAdmin;
	}

	/**
	 * @return the mailToAdminSubject
	 */
	public String getMailToAdminSubject() {
		return mailToAdminSubject;
	}

	/**
	 * @param mailToAdminSubject
	 *            the mailToAdminSubject to set
	 */
	public void setMailToAdminSubject(String mailToAdminSubject) {
		this.mailToAdminSubject = mailToAdminSubject;
	}

	/**
	 * @return the mailToAdminMessage
	 */
	public String getMailToAdminMessage() {
		return mailToAdminMessage;
	}

	/**
	 * @param mailToAdminMessage
	 *            the mailToAdminMessage to set
	 */
	public void setMailToAdminMessage(String mailToAdminMessage) {
		this.mailToAdminMessage = mailToAdminMessage;
	}

	/**
	 * @return the accessTokenValidity
	 */
	public Integer getAccessTokenValidity() {
		return accessTokenValidity;
	}

	/**
	 * @param accessTokenValidity
	 *            the accessTokenValidity to set
	 */
	public void setAccessTokenValidity(Integer accessTokenValidity) {
		this.accessTokenValidity = accessTokenValidity;
	}

}
