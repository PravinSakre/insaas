
package com.insaas.insaasApp.common;

/**
 * 
 * Represents an Constant will store the constant fields
 * 
 * @author BrainWave Consulting
 * @since April 17, 2019
 */
public class Constant {

	/** The Constant WORKING_DIR. */
	public static final String WORKING_DIR = System.getProperty("user.dir");

	/** The Constant CONFIGURATIONS. */
	public static final String CONFIGURATIONS = "configurations";
	//public static final String PROPERTY_FILE_PATH ="/home/properties/application.properties";
	public static final String PROPERTY_FILE_PATH ="D:\\SVN_INSAAS\\tags\\dev\\source\\insaasApplication\\insaasApp\\src\\main\\resources\\application.properties";
	//public static final String CSV_FILE_PATH="D:/SVN_REPO_INSAAS/tags/dev/source/insaasApplication/insaasApp/xmlFiles/";
	public static final String CSV_FILE_PATH=WORKING_DIR +"/xmlFiles/BWC_GRS_German_Text_Analysis/getADataForRunCSV";
	public static final String CSV_FILE_OF_PY_OUTPUT=WORKING_DIR +"/xmlFiles/BWC_GRS_German_Text_Analysis/run_results200_test_prediction.csv";
	public static final String PYTHON_FILE_PATH="/xmlFiles/BWC_GRS_German_Text_Analysis/data_prediction.py";
	public static final String PYTHON_COMMAND="python3 ";
	public static final String PARSE_HUB_URL="https://www.parsehub.com/api/v2";
	public static final String SOLR_URL="http://192.168.1.129/solr/core_name/";
	public static final String URL_OF_SOLR="http://192.168.1.129/solr/#/";
	public static final String CREATE_CORE_BAT_PATH="solr.server.create.core.bat.file.path";
	public static final String SOLR_INPUT_DOC_FIELDS="solr.server.inputDocument.fields";
	/** The Constant NAME. */
	public static final String NAME = "name";
	public static final String RESULT_STRING="result";
	public static final String ACCESS_TOKEN="accessToken";
	public static final String ACCESS_ITEM_TYPE_MENU = "menu";
	public static final String ACCESS_ITEM_TYPE_BUTTON = "button";
	public static final String MSG_EXC_PASS_CHANGED="PASSWORD CHANGED SUCCESSFULLY...";
	public static final String INSUFFICIENT_AUTHENTICATION_EXCEPTION="This user don't have sufficient authentication";
	public static final String USER_NOT_FOUND = "User not found";
	public static final String MSG_PASS_ADD="URL to add new password, is sent to user please see email";
	public static final String MSG_PASS_CHANGE="PASSWORD CHANGED SUCCESSFULLY...";
	public static final String MSG_EMAIL_NOT_EXIST="Email id  Not exist";
	public static final String MSG_PASS_VALID="PLEASE ENTER VALID PREVIOUS PASSWORD..";
	public static final String ROLE_NOT_FOUND = "Role not found..";
	public static final String ROLE_ACCESS_LIST_NOT_FOUND = "Role access list not found";
	public static final String ACCESS_ITEM_ID_NOT_FOUND = "Access item id not found";

	public static final String ACCESS_ITEM_MASTER_NOT_FOUND = "Access item master not found";
	public static final String API_KEY="tFWMO9FohPWb";
	public static final String PROJECT_TOKEN="tiTvad_AYvs2";
	public static final String RUN_TOKEN="thVuzfLMGG4P";
	public static final String SPARK_SUBMIT_PATH="/opt/spark";
	
	public static final String DEFAULT_DASHBOARD_FILE_NAME = "default-dashboard.json";
	public static final String SEARCH_DASHBOARD_FILE_NAME = "search.json";

	public static final String DEFAULT_DASHBOARD = "DefaultDashBoard";
	public static final String SEARCH_DASHBOARD = "SearchDashBoard";
  	
	public static final String APPLY_TO_ALL = "applyToAll";
	public static final String DELETE_FROM_ALL = "deleteFromAll";
	public static final String APPLY_TO_SELECTED = "applyToSelected";
	public static final String DELETE_FROM_SELECTED = "deleteFromSelected";
  	
}