
package com.insaas.insaasApp.common;

/**
 * 
 * Represents an MessagesConstant will store the constant fields
 * 
 * @author BrainWave Consulting
 * @since April 17, 2019
 */
public class MessagesConstant {

	/** The MessagesConstant WORKING_DIR. */
	public static final String WORKING_DIR = System.getProperty("user.dir");

	/** The MessagesConstant CONFIGURATIONS. */
	public static final String CONFIGURATIONS = "configurations";
	//public static final String CSV_FILE_PATH="D:/SVN_REPO_INSAAS/tags/dev/source/insaasApplication/insaasApp/xmlFiles/";
	public static final String CSV_FILE_PATH="D:/SVN_INSAAS/tags/dev/source/insaasApplication/insaasApp/xmlFiles/";
	public static final String PARSE_HUB_URL="https://www.parsehub.com/api/v2";
	/** The MessagesConstant NAME. */
	public static final String NAME = "name";
	public static final String RESULT_STRING="result";
	public static final String ACCESS_TOKEN="accessToken";
	public static final String ACCESS_ITEM_TYPE_MENU = "menu";
	public static final String ACCESS_ITEM_TYPE_BUTTON = "button";
	public static final String MSG_EXC_PASS_CHANGED="PASSWORD CHANGED SUCCESSFULLY...";
	public static final String INSUFFICIENT_AUTHENTICATION_EXCEPTION="This user don't have sufficient authentication";
	public static final String USER_NOT_FOUND = "User not found";
	public static final String MSG_PASS_ADD="URL to add new password, is sent to user please see email";
	public static final String MSG_PASS_CHANGE="PASSWORD CHANGED SUCCESSFULLY...";
	public static final String MSG_EMAIL_NOT_EXIST="Email id  Not exist";
	public static final String MSG_PASS_VALID="PLEASE ENTER VALID PREVIOUS PASSWORD..";
	public static final String ROLE_NOT_FOUND = "Role not found..";
	public static final String ROLE_ACCESS_LIST_NOT_FOUND = "Role access list not found";
	public static final String ACCESS_ITEM_ID_NOT_FOUND = "Access item id not found";

	public static final String ACCESS_ITEM_MASTER_NOT_FOUND = "Access item master not found";
	public static final String API_KEY="tFWMO9FohPWb";
	public static final String PROJECT_TOKEN="tiTvad_AYvs2";
	public static final String RUN_TOKEN="tS0vEn_SreiF";
	
	public static final String FIELD_EMPTY_OR_NULL_MESSAGE = "Fields should not be empty or null";
}