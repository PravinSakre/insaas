package com.insaas.insaasApp.common;

public class ApplicationConstant {

	public static final String EMPTY = "";
	public static final String COMMA = ",";
	public static final String SEMICOLON = ";";
	public static final String USER_NAME = "Username"; 
	public static final String FEEDBACK = "Feedback"; 
	public static final String ASPECT = "Aspect"; 
	public static final String SENTIMENT = "Sentiment"; 
	public static final String SOURCE_OF_INFO = "Source"; 
	public static final String DATE_TIME="date";
	public static final String FEEDBACK_HASH_VALUE = "FeedbackHashValue";
	public static final String GENDER="Gender";
	public static final String RATING="Rating";
}
