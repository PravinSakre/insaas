/**
 * 
 */
package com.insaas.insaasApp.pojo;

import java.util.List;
import java.util.Set;

/**
 * @author User
 *
 */
public class SolrSearchFields {

	private String accessToken;
	private List<String> aspect;
	private String search;
	private Integer rows;
	private Integer start;
	private String updateOrDeleteflag;
	private String updatedValue;
	private List<String> updatedDocId;

	public List<String> getUpdatedDocId() {
		return updatedDocId;
	}

	public void setUpdatedDocId(List<String> updatedDocId) {
		this.updatedDocId = updatedDocId;
	}

	public String getUpdatedValue() {
		return updatedValue;
	}

	public void setUpdatedValue(String updatedValue) {
		this.updatedValue = updatedValue;
	}

	public String getUpdateOrDeleteflag() {
		return updateOrDeleteflag;
	}

	public void setUpdateOrDeleteflag(String updateOrDeleteflag) {
		this.updateOrDeleteflag = updateOrDeleteflag;
	}

	/**
	 * @return the accessToken
	 */
	public String getAccessToken() {
		return accessToken;
	}

	/**
	 * @param accessToken the accessToken to set
	 */
	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	/**
	 * @return the aspect
	 */
	public List<String> getAspect() {
		return aspect;
	}

	/**
	 * @param aspect the aspect to set
	 */
	public void setAspect(List<String> aspect) {
		this.aspect = aspect;
	}

	/**
	 * @return the search
	 */
	public String getSearch() {
		return search;
	}

	/**
	 * @param search the search to set
	 */
	public void setSearch(String search) {
		this.search = search;
	}

	/**
	 * @return the rows
	 */
	public Integer getRows() {
		return rows;
	}

	/**
	 * @param rows the rows to set
	 */
	public void setRows(Integer rows) {
		this.rows = rows;
	}

	/**
	 * @return the start
	 */
	public Integer getStart() {
		return start;
	}

	/**
	 * @param start the start to set
	 */
	public void setStart(Integer start) {
		this.start = start;
	}

	@Override
	public String toString() {
		return "SolrSearchFields [accessToken=" + accessToken + ", aspect=" + aspect + ", search=" + search + ", rows="
				+ rows + ", start=" + start + ", updateOrDeleteflag=" + updateOrDeleteflag + ", updatedValue="
				+ updatedValue + ", updatedDocId=" + updatedDocId + "]";
	}

}
