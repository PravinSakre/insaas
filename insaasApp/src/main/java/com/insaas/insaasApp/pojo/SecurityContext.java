/**
 * @author BrainWave Consulting
 * @since Feb 2, 2019
 * 
 */
package com.insaas.insaasApp.pojo;
import java.util.Date;
import java.util.List;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.web.context.WebApplicationContext;




/**
 * Represents an SecurityContext will handles User requests
 * @author BrainWave Consulting
 * @since Feb 2, 2019
 */
public class SecurityContext {
	private SecurityContext() {
		
	}
	
	public SecurityContext(String accessToken) {
		if(null == accessToken || "".equals(accessToken)) {
			new SecurityContext();
		}
		
	}
	
	private String userId = null;
	private Date loggedInTime = null;
	private Date lastAccessedTime = null;
	
	private Date loggedOutTime = null;
	private List<Integer> writeOrgAccessList = null;
	private List<Integer> editOrgAccessList = null;
	private List<Integer> readOrgAccessList = null;
	private Integer roleID = null;
	private Integer userAutoGenId = null;
	
	private Integer orgID = null;
	/**
	 * @return the orgID
	 */
	public Integer getOrgID() {
		return orgID;
	}
	/**
	 * @param orgID the orgID to set
	 */
	public void setOrgID(Integer orgID) {
		this.orgID = orgID;
	}
	/**
	 * @return the parentOrgID
	 */
	public Integer getParentOrgID() {
		return parentOrgID;
	}
	/**
	 * @param parentOrgID the parentOrgID to set
	 */
	public void setParentOrgID(Integer parentOrgID) {
		this.parentOrgID = parentOrgID;
	}
	private Integer parentOrgID = null;
	
	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}
	/**
	 * @return the loggedInTime
	 */
	public Date getLoggedInTime() {
		return loggedInTime;
	}
	/**
	 * @param loggedInTime the loggedInTime to set
	 */
	public void setLoggedInTime(Date loggedInTime) {
		this.loggedInTime = loggedInTime;
	}
	/**
	 * @return the loggedOutTime
	 */
	public Date getLoggedOutTime() {
		return loggedOutTime;
	}
	/**
	 * @param loggedOutTime the loggedOutTime to set
	 */
	public void setLoggedOutTime(Date loggedOutTime) {
		this.loggedOutTime = loggedOutTime;
	}
	/**
	 * @return the writeOrgAccessList
	 */
	public List<Integer> getWriteOrgAccessList() {
		return writeOrgAccessList;
	}
	/**
	 * @param writeOrgAccessList the writeOrgAccessList to set
	 */
	public void setWriteOrgAccessList(List<Integer> writeOrgAccessList) {
		this.writeOrgAccessList = writeOrgAccessList;
	}
	/**
	 * @return the editOrgAccessList
	 */
	public List<Integer> getEditOrgAccessList() {
		return editOrgAccessList;
	}
	/**
	 * @return the lastAccessedTime
	 */
	public Date getLastAccessedTime() {
		return lastAccessedTime;
	}
	/**
	 * @param lastAccessedTime the lastAccessedTime to set
	 */
	public void setLastAccessedTime(Date lastAccessedTime) {
		this.lastAccessedTime = lastAccessedTime;
	}
	/**
	 * @param editOrgAccessList the editOrgAccessList to set
	 */
	public void setEditOrgAccessList(List<Integer> editOrgAccessList) {
		this.editOrgAccessList = editOrgAccessList;
	}
	/**
	 * @return the readOrgAccessList
	 */
	public List<Integer> getReadOrgAccessList() {
		return readOrgAccessList;
	}
	/**
	 * @param readOrgAccessList the readOrgAccessList to set
	 */
	public void setReadOrgAccessList(List<Integer> readOrgAccessList) {
		this.readOrgAccessList = readOrgAccessList;
	}
	
	public Integer getRoleID() {
		return roleID;
	}
	public void setRoleID(Integer roleID) {
		this.roleID = roleID;
	}
	public Integer getUserAutoGenId() {
		return userAutoGenId;
	}
	public void setUserAutoGenId(Integer userAutoGenId) {
		this.userAutoGenId = userAutoGenId;
	}

}
