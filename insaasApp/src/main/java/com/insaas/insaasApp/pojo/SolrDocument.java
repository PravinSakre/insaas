package com.insaas.insaasApp.pojo;

import java.util.List;
import java.util.Map;

import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.core.SolrCore;


public class SolrDocument {
  private String  data;
  private List<Map<String,String>>   aspect;
public String getData() {
	return data;
}
public void setData(String data) {
	this.data = data;
}
public List<Map<String, String>> getAspect() {
	return aspect;
}
public void setAspect(List<Map<String, String>> aspect) {
	this.aspect = aspect;
}
  
  

}
