package com.insaas.insaasApp.dto;

import java.util.List;

import com.insaas.insaasApp.entity.CustomerDetails;

public class CustomerDto {
	
	private Integer customerId;
	private String customerName;
	private Integer custDetailsId;
	private String uRL;
	private String aPI_Key;
	private String project_Token;
	private String coreName;
	private String run_Token;
	private List<CustomerDetails> customerDetailsList;
	private String analysisStatement;
	private String sourceOfInfo;
	private Boolean active;
	
	
	public String getSourceOfInfo() {
		return sourceOfInfo;
	}
	public void setSourceOfInfo(String sourceOfInfo) {
		this.sourceOfInfo = sourceOfInfo;
	}
	public List<CustomerDetails> getCustomerDetailsList() {
		return customerDetailsList;
	}
	public void setCustomerDetailsList(List<CustomerDetails> customerDetailsList) {
		this.customerDetailsList = customerDetailsList;
	}
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public Integer getCustDetailsId() {
		return custDetailsId;
	}
	public void setCustDetailsId(Integer custDetailsId) {
		this.custDetailsId = custDetailsId;
	}
	public String getuRL() {
		return uRL;
	}
	public void setuRL(String uRL) {
		this.uRL = uRL;
	}
	public String getaPI_Key() {
		return aPI_Key;
	}
	public void setaPI_Key(String aPI_Key) {
		this.aPI_Key = aPI_Key;
	}
	public String getProject_Token() {
		return project_Token;
	}
	public void setProject_Token(String project_Token) {
		this.project_Token = project_Token;
	}
	public String getCoreName() {
		return coreName;
	}
	public void setCoreName(String coreName) {
		this.coreName = coreName;
	}
	public String getRun_Token() {
		return run_Token;
	}
	public void setRun_Token(String run_Token) {
		this.run_Token = run_Token;
	}
	public String getAnalysisStatement() {
		return analysisStatement;
	}
	public void setAnalysisStatement(String analysisStatement) {
		this.analysisStatement = analysisStatement;
	}
	@Override
	public String toString() {
		return "CustomerDto [customerId=" + customerId + ", customerName=" + customerName + ", custDetailsId="
				+ custDetailsId + ", uRL=" + uRL + ", aPI_Key=" + aPI_Key + ", project_Token=" + project_Token
				+ ", coreName=" + coreName + ", run_Token=" + run_Token + ", customerDetailsList=" + customerDetailsList
				+ ", analysisStatement=" + analysisStatement + ", sourceOfInfo=" + sourceOfInfo + "]";
	}
	
	
}