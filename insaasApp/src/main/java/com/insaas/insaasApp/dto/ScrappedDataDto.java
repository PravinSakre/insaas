//package com.insaas.insaasApp.dto;
//
//public class ScrappedDataDto {
//
//	private String userName;
//	private String feedback;
//	private String date;
//	private String aspect;
//	private String sentiment;
//	private String sourceOfInfo;
//	private String gender;
//	
//	public String getGender() {
//		return gender;
//	}
//	public void setGender(String gender) {
//		this.gender = gender;
//	}
//	public String getUserName() {
//		return userName;
//	}
//	public void setUserName(String userName) {
//		this.userName = userName;
//	}
//	public String getFeedback() {
//		return feedback;
//	}
//	public void setFeedback(String feedback) {
//		this.feedback = feedback;
//	}
//	public String getDate() {
//		return date;
//	}
//	public void setDate(String date) {
//		this.date = date;
//	}
//	public String getAspect() {
//		return aspect;
//	}
//	public void setAspect(String aspect) {
//		this.aspect = aspect;
//	}
//	public String getSentiment() {
//		return sentiment;
//	}
//	public void setSentiment(String sentiment) {
//		this.sentiment = sentiment;
//	}
//	public String getSourceOfInfo() {
//		return sourceOfInfo;
//	}
//	public void setSourceOfInfo(String sourceOfInfo) {
//		this.sourceOfInfo = sourceOfInfo;
//	}
//	@Override
//	public String toString() {
//		return "ScrappedDataDto [userName=" + userName + ", feedback=" + feedback + ", date=" + date + ", aspect="
//				+ aspect + ", sentiment=" + sentiment + ", sourceOfInfo=" + sourceOfInfo + "]";
//	}
//	
//}
