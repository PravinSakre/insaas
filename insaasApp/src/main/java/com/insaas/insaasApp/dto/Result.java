package com.insaas.insaasApp.dto;
/**
 * 
 * Represents an Result will handles the result messages
 * @author BrainWave Consulting
 * @since Dec 25, 2018
 */
public class Result {
	private Integer id;
	private String message;
	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}
	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	
}
