package com.insaas.insaasApp.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Vinay
 * Date: 3/27/15
 * Time: 1:26 PM
 * To change this template use File | Settings | File Templates.
 */

public class User  {

    private String username;

    private String password;

    private String roles;

    public List<String> getCoreNames() {
        return coreNames;
    }

    public void setCoreNames(List<String> coreNames) {
        this.coreNames = coreNames;
    }

    private List<String> coreNames = new ArrayList<String>();

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRoles() {
        return roles;
    }

    public void setRoles(String roles) {
        this.roles = roles;
    }

}
