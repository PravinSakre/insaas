//package com.insaas.insaasApp.dto;
//import com.google.gson.Gson;
//import com.google.gson.JsonObject;
//import com.google.gson.JsonParser;
//import com.insaas.insaasApp.helper.GetPropertyValues;
//import org.apache.commons.lang.StringUtils;
//import org.apache.solr.client.solrj.SolrQuery;
//
//import java.util.ArrayList;
//import java.util.List;
//
///**
// * User: Surya
// * Date: 1/17/15
// * Time: 11:01 PM
// * To change this template use File | Settings | File Templates.
// */
//public class SolrQueryBuilderDto {
//	
//
//	private List<String> keywords = new ArrayList<String>();
//	private String fileName;
//	private String folder;
//	private int records;
//	private String query;
//	private List<String> fileTypes = new ArrayList<String>();
//	private String ignoreFileNameField;
//	private String category1_s;
//	private String category2_s;
//	private String category3_s;
//	private Boolean duplicateFileContents;
//	
//	private String solrQueryBuilderParam;
//	private String countParam;
//	private String filePath;
//	private String derived_file_type;
//	private String defaultSearchOp;
//	
//	public String getSolrQueryBuilderParam() {
//		return solrQueryBuilderParam;
//	}
//
//	public void setSolrQueryBuilderParam(String solrQueryBuilderParam) {
//		this.solrQueryBuilderParam = solrQueryBuilderParam;
//	}
//
//	public String getCountParam() {
//		return countParam;
//	}
//
//	public void setCountParam(String countParam) {
//		this.countParam = countParam;
//	}
//
//	public String getFilePath() {
//		return filePath;
//	}
//
//	public void setFilePath(String filePath) {
//		this.filePath = filePath;
//	}
//
//	public String getDerived_file_type() {
//		return derived_file_type;
//	}
//
//	public void setDerived_file_type(String derived_file_type) {
//		this.derived_file_type = derived_file_type;
//	}
//
//	public String getDefaultSearchOp() {
//		return defaultSearchOp;
//	}
//
//	public void setDefaultSearchOp(String defaultSearchOp) {
//		this.defaultSearchOp = defaultSearchOp;
//	}
//
//	public String getSortBy() {
//		return sortBy;
//	}
//
//	public void setSortBy(String sortBy) {
//		this.sortBy = sortBy;
//	}
//
//	public String getCategory1_s() {
//		return category1_s;
//	}
//
//	public void setCategory1_s(String category1_s) {
//		this.category1_s = category1_s;
//	}
//
//	public String getCategory2_s() {
//		return category2_s;
//	}
//
//	public void setCategory2_s(String category2_s) {
//		this.category2_s = category2_s;
//	}
//
//	public String getCategory3_s() {
//		return category3_s;
//	}
//
//	public void setCategory3_s(String category3_s) {
//		this.category3_s = category3_s;
//	}
//
//	public List<String> getUserSize() {
//		return userSize;
//	}
//
//	public void setUserSize(List<String> userSize) {
//		this.userSize = userSize;
//	}
//
//	private List<String> userSize = new ArrayList<String>();
//
//	public List<String> getExtentions() {
//		return extentions;
//	}
//
//	public void setExtentions(List<String> extentions) {
//		this.extentions = extentions;
//	}
//
//	private List<String> extentions = new ArrayList<String>();
//
//	public List<String> getExcludeIdList() {
//		return excludeIdList;
//	}
//
//	public void setExcludeIdList(List<String> excludeIdList) {
//		this.excludeIdList = excludeIdList;
//	}
//
//	public List<String> excludeIdList=new ArrayList<String>();
//
//	public Boolean getDuplicateFileNames() {
//		return duplicateFileNames;
//	}
//
//	public void setDuplicateFileNames(Boolean duplicateFileNames) {
//		this.duplicateFileNames = duplicateFileNames;
//	}
//
//	public void setDuplicateFileNames(String duplicateFileNames) {
//
//		if(StringUtils.isBlank(duplicateFileNames)){
//			setDuplicateFileNames(false);
//			return;
//		}
//
//		if(StringUtils.equals("true",duplicateFileNames)){
//			setDuplicateFileNames(true);
//		}else if(StringUtils.equals("false",duplicateFileNames)){
//			setDuplicateFileNames(false);
//		}
//	}
//
//	private Boolean duplicateFileNames;
//
//	public String getLast_modified() {
//		return last_modified;
//	}
//
//	public void setLast_modified(String last_modified) {
//		this.last_modified = last_modified;
//	}
//
//	private String last_modified;
//
//	public String getOperator() {
//		return operator;
//	}
//
//	public void setOperator(String operator) {
//		this.operator = operator;
//	}
//
//	public String getValidOperator() {
//		if(StringUtils.isBlank(operator))
//			return "AND";
//		return operator;
//	}
//
//	private String operator;
//
//	public String getIgnoreFileNameField() {
//		return ignoreFileNameField;
//	}
//
//	public void setIgnoreFileNameField(String ignoreFileNameField) {
//		this.ignoreFileNameField = ignoreFileNameField;
//	}
//
//	public String getIgnoreFilePathField() {
//		return ignoreFilePathField;
//	}
//
//	public void setIgnoreFilePathField(String ignoreFilePathField) {
//		this.ignoreFilePathField = ignoreFilePathField;
//	}
//
//	public List<String> getIgnoreFileContentField() {
//		return ignoreFileContentField;
//	}
//
//	public void setIgnoreFileContentField(List<String> ignoreFileContentField) {
//		this.ignoreFileContentField = ignoreFileContentField;
//	}
//
//	private String ignoreFilePathField;
//	private List<String> ignoreFileContentField=new ArrayList<String>();
//
//	
//
//	public String getSortType() {
//		return sortType;
//	}
//
//	public void setSortType(String sortType) {
//		this.sortType = sortType;
//	}
//
//	private String sortBy;
//	private String sortType;
//
//
//	public List<String> getFileTypes() {
//		return fileTypes;
//	}
//
//	public void setFileTypes(List<String> fileTypes) {
//		this.fileTypes = fileTypes;
//	}
//
//	public void addFileType(String fileType) {
//		if(StringUtils.isNotEmpty(fileType))
//			fileTypes.add(fileType);
//	}
//
//	public String getQuery() {
//		return query;
//	}
//
//	public void setQuery(String query) {
//		this.query = query;
//	}
//
//	public List<String> getKeywords() {
//		return keywords;
//	}
//
//	public void setKeywords(List<String> keywords) {
//		this.keywords = keywords;
//	}
//
//	public String getFileName() {
//		return fileName;
//	}
//
//	public void setFileName(String fileName) {
//		this.fileName = fileName;
//	}
//
//	public String getFolder() {
//		return folder;
//	}
//
//	public void setFolder(String folder) {
//		this.folder = folder;
//	}
//
//	public int getRecords() {
//		return records;
//	}
//
//	public void setRecords(int records) {
//		this.records = records;
//	}
//
//	public void addQuery(String query) {
//		if(StringUtils.isEmpty(query))
//			return;
//		if(StringUtils.isEmpty(getQuery())) {
//			setQuery(query);
//			return;
//		}
//		addKeyword(query);
//	}
//
//	public void addKeyword(String keyword) {
//		if(null!=keyword && !StringUtils.equals(keyword,""))
//			keywords.add(keyword);
//	}
//	/**
//	 * Query Builder
//	 */
//	public SolrQuery toSolrQuery() {
//		/*if(StringUtils.isEmpty(getQuery()))
//            return null;*/
//
//		SolrQuery solrQuery=new SolrQuery();
//		setDefaults(solrQuery);
//
//		handleTermFrequencies(solrQuery);
//
//		String op=" "+this.getValidOperator()+" ";
//
//		if(StringUtils.isEmpty(this.getQuery())){
//			solrQuery.setQuery("*");
//		}
//
//
//
//		StringBuilder queryBuilder=new StringBuilder();
//		//Code to check if single field is enabled, put only one field in query
//		if(GetPropertyValues.isSingleFieldEnabled()){
//			if(StringUtils.isNotEmpty(getQuery()))
//				queryBuilder.append(this.getQuery());
//
//			if (this.getKeywords().size() > 0){
//				for(String item:this.getKeywords()){
//					if(!StringUtils.equals(item, ""))
//						queryBuilder.append(op + item);
//				}
//			}
//		}else{
//
//			if(StringUtils.isNotEmpty(getQuery()))
//				queryBuilder.append("content:"+this.getQuery());
//
//			if (this.getKeywords().size() > 0){
//				for(String item:this.getKeywords()){
//					if(!StringUtils.equals(item, ""))
//						queryBuilder.append(op + " content:" + item);
//				}
//			}
//
//			if(StringUtils.isEmpty(queryBuilder.toString()) && StringUtils.isNotEmpty(getFileName())){
//				queryBuilder.append("filename:"+this.getFileName());
//			}else if(StringUtils.isNotEmpty(getFileName())){
//				queryBuilder.append(op+" filename:"+this.getFileName());
//			}
//			if(StringUtils.isEmpty(queryBuilder.toString()) && StringUtils.isNotEmpty(getFolder())){
//				queryBuilder.append("id:"+getFolder());
//			}else if(StringUtils.isNotEmpty(getFolder())){
//				queryBuilder.append(op+" id:"+getFolder());
//			}
//
//			if (this.getFileTypes().size() > 0){
//				StringBuilder result=new StringBuilder("( in ");
//				for(String item:this.getFileTypes()){
//					if(!StringUtils.equals(item, "")){
//						result.append("\""+item+"\"").append(" ");
//					}
//				}
//				result.append(")");
//				solrQuery.addFilterQuery("derived_file_type:"+result.toString()+"");
//			}
//
//			if(StringUtils.isNotEmpty(getLast_modified())){
//				solrQuery.addFilterQuery("last_modified:"+getLast_modified());
//			}
//
//			if(this.getRecords() >=50 )
//				solrQuery.setRows(this.getRecords());
//
//
//
//			StringBuilder negativeQueryBuilder=new StringBuilder();
//
//			if (this.getIgnoreFileContentField().size() > 0){
//
//				if(this.getIgnoreFileContentField().size()==1){
//					negativeQueryBuilder.append("-content:" + this.getIgnoreFileContentField().get(0) + "");
//				}else{
//					for(int i=0;i < this.getIgnoreFileContentField().size();i++){
//						String item=this.getIgnoreFileContentField().get(i);
//						if(i==0)
//							negativeQueryBuilder.append("-content:" + item + "");
//						else
//							negativeQueryBuilder.append(" "+op+" -content:" + item + "");
//					}
//				}
//			}
//
//			if(StringUtils.isEmpty(negativeQueryBuilder.toString()) && StringUtils.isNotEmpty(getIgnoreFileNameField())){
//				negativeQueryBuilder.append("-filename:" + getIgnoreFileNameField() );
//			}else if(StringUtils.isNotEmpty(negativeQueryBuilder.toString()) && StringUtils.isNotEmpty(getIgnoreFileNameField())){
//				negativeQueryBuilder.append(" "+op+"-filename:" + getIgnoreFileNameField() );
//			}
//
//
//			if(StringUtils.isEmpty(negativeQueryBuilder.toString()) && StringUtils.isNotEmpty(getIgnoreFilePathField())){
//				negativeQueryBuilder.append("-id:" + getIgnoreFilePathField() );
//			}else if(StringUtils.isNotEmpty(negativeQueryBuilder.toString()) && StringUtils.isNotEmpty(getIgnoreFilePathField())){
//				negativeQueryBuilder.append(" "+op+"-id:" + getIgnoreFilePathField() );
//			}
//
//			if(StringUtils.isNotEmpty(negativeQueryBuilder.toString())){
//				solrQuery.addFilterQuery(negativeQueryBuilder.toString());
//			}
//		}
//
//		handleSort(solrQuery);
//
//		if(StringUtils.isNotBlank(getCategory1_s())){
//			solrQuery.addFilterQuery("category1_s:\""+getCategory1_s()+"\"");
//		}
//
//		if(StringUtils.isNotBlank(getCategory2_s())){
//			solrQuery.addFilterQuery("category2_s:\""+getCategory2_s()+"\"");
//		}
//
//		if(StringUtils.isNotBlank(getCategory3_s())) {
//			solrQuery.addFilterQuery("category3_s:\""+getCategory3_s()+"\"");
//		}
//
//		if(StringUtils.isNotBlank(queryBuilder.toString())){
//			solrQuery.setQuery(queryBuilder.toString());
//		}
//
//		if(this.getExcludeIdList().size()>0){
//			for(String item:this.getExcludeIdList()){
//				JsonObject jsonObject = (new JsonParser()).parse(item).getAsJsonObject();
//				if(null!=jsonObject){
//					String hash_id=jsonObject.get("hash_id").toString();
//					solrQuery.addFilterQuery("-hash_id:"+hash_id+"");
//				}
//			}
//
//
//		}
//
//		return solrQuery;
//	}
//
//	private void handleSort(SolrQuery solrQuery) {
//		//To change body of created methods use File | Settings | File Templates.
//		if(StringUtils.isNotEmpty(getSortBy()) && StringUtils.isNotEmpty(getSortType())){
//			if(StringUtils.equals(getSortType(),"ASC"))
//				solrQuery.addSort(getSortBy(), SolrQuery.ORDER.asc);
//			else if(StringUtils.equals(getSortType(),"DESC")){
//				solrQuery.addSort(getSortBy(), SolrQuery.ORDER.desc);
//			}
//		}
//	}
//
//	private void handleTermFrequencies(SolrQuery solrQuery) {
//		StringBuilder sb = new StringBuilder("*,score");
//		//        if(StringUtils.isNotEmpty(getQuery()))
//		//            sb.append(",termfreq(content,'").append(this.getQuery()).append("')");
//		//        for (String keyword : getKeywords())
//		//            sb.append(",termfreq(content,'").append(keyword).append("')");
//		solrQuery.setFields(sb.toString());
//	}
//
//
//	private void handleFileNameIgnores(SolrQuery solrQuery) {
//		if(StringUtils.isNotEmpty(getIgnoreFileNameField()))
//			solrQuery.addFilterQuery("-filename:*"+getIgnoreFileNameField()+"*");
//	}
//	private void handleFilePathIgnores(SolrQuery solrQuery) {
//		if(StringUtils.isNotEmpty(getIgnoreFilePathField()))
//			solrQuery.addFilterQuery("-id:*"+getIgnoreFilePathField()+"*");
//	}
//	private void handleFileContentIgnores(SolrQuery solrQuery) {
//		if (this.getIgnoreFileContentField().size() > 0){
//			for(String item:this.getIgnoreFileContentField()){
//				if(!StringUtils.equals(item, ""))
//					solrQuery.addFilterQuery("-"+item+"");
//			}
//		}
//	}
//
//	private void handleFileTypes(SolrQuery solrQuery) {
//		if (this.getFileTypes().size() > 0){
//			StringBuilder result=new StringBuilder("( in ");
//			for(String item:this.getFileTypes()){
//				if(!StringUtils.equals(item, ""))
//					result.append(item).append(" ");
//			}
//			result.append(")");
//			solrQuery.addFilterQuery("derived_file_type:"+result.toString()+"");
//
//		}
//	}
//
//	private void handleKeywords(SolrQuery solrQuery) {
//		if (this.getKeywords().size() > 0){
//			for(String item:this.getKeywords()){
//				if(!StringUtils.equals(item, ""))
//					solrQuery.addFilterQuery(item);
//			}
//		}
//	}
//
//	private void setDefaults(SolrQuery solrQuery) {
//		solrQuery.setFacet(true);
//		solrQuery.setFacetMinCount(1);
//		solrQuery.setRows(50);
//		solrQuery.setStart(0);
//
//		/*solrQuery.addFacetField("category1_s");
//        solrQuery.addFacetField("category2_s");
//        solrQuery.addFacetField("category3_s");*/
//
//		solrQuery.addFacetField("derived_file_type");
//		solrQuery.addFacetField("last_modified");
//		solrQuery.addFacetField("extension");
//
//		/*if(getDuplicateFileNames()){
//            solrQuery.setParam("group", true);
//            solrQuery.setParam("group.field","hash_filename");
//            solrQuery.setParam("group.limit","100000");
//        }*/
//
//		//for getting minimum and maximum
//		solrQuery.setParam("stats", true);
//		solrQuery.setParam("stats.field","filesize");
//
//	}
//
//	public String toJson() {
//		Gson gson = new Gson();
//		return gson.toJson(this);
//	}
//
//	public static SolrQueryBuilderDto fromJson(String json) {
//		Gson gson = new Gson();
//		if(StringUtils.isEmpty(json))
//			return new SolrQueryBuilderDto();
//		return gson.fromJson(json, SolrQueryBuilderDto.class);
//	}
//
//	public static SolrQuery queryToGroupByField(String fieldName){
//		SolrQuery solrQuery = new SolrQuery();
//		solrQuery.setQuery("*");
//		solrQuery.set("group",true);
//		solrQuery.add("group.field",fieldName);
//		return solrQuery;
//	}
//
//	public static SolrQuery queryToGetByField(String fieldName,String fieldValue){
//		SolrQuery solrQuery = new SolrQuery();
//		solrQuery.setQuery("*");
//		solrQuery.setFields("*,score");
//		solrQuery.setFilterQueries(fieldName + ":\"" + fieldValue + "\"");
//		return solrQuery;
//	}
//
//	public static SolrQuery queryToGetBySubString(String subString){
//		SolrQuery solrQuery = new SolrQuery();
//		solrQuery.setQuery(subString);
//		solrQuery.setFields("*,score");
//		return solrQuery;
//	}
//
//	public void addFileContentField(String ignoreFileContentField) {
//		if(StringUtils.isEmpty(ignoreFileContentField))
//			return;
//		this.ignoreFileContentField.add(ignoreFileContentField);
//	}
//
//	public Boolean getDuplicateFileContents() {
//		return duplicateFileNames;
//	}
//
//	public void setDuplicateFileContents(Boolean duplicateFileContents) {
//		this.duplicateFileContents = duplicateFileContents;
//	}
//
//	public void setDuplicateFileContents(String duplicateFileContents) {
//
//		if(StringUtils.isBlank(duplicateFileContents)){
//			setDuplicateFileContents(false);
//			return;
//		}
//
//		if(StringUtils.equals("true",duplicateFileContents)){
//			setDuplicateFileContents(true);
//		}else if(StringUtils.equals("false",duplicateFileContents)){
//			setDuplicateFileContents(false);
//		}
//	}
//	@Override
//	public String toString() {
//		return "SolrQueryBuilderDto [keywords=" + keywords + ", fileName=" + fileName + ", folder=" + folder
//				+ ", records=" + records + ", query=" + query + ", fileTypes=" + fileTypes + ", ignoreFileNameField="
//				+ ignoreFileNameField + ", category1_s=" + category1_s + ", category2_s=" + category2_s
//				+ ", category3_s=" + category3_s + ", duplicateFileContents=" + duplicateFileContents
//				+ ", solrQueryBuilderParam=" + solrQueryBuilderParam + ", countParam=" + countParam + ", filePath="
//				+ filePath + ", derived_file_type=" + derived_file_type + ", defaultSearchOp=" + defaultSearchOp
//				+ ", userSize=" + userSize + ", extentions=" + extentions + ", excludeIdList=" + excludeIdList
//				+ ", duplicateFileNames=" + duplicateFileNames + ", last_modified=" + last_modified + ", operator="
//				+ operator + ", ignoreFilePathField=" + ignoreFilePathField + ", ignoreFileContentField="
//				+ ignoreFileContentField + ", sortBy=" + sortBy + ", sortType=" + sortType + "]";
//	}
//}
