/**
 * 
 */
package com.insaas.insaasApp.dto;

import java.util.Date;

import com.insaas.insaasApp.entity.User;

/**
 * 
 * Represents an UserDto will handles transferring of user data
 * 
 * @author BrainWave Consulting
 * @since Dec 24, 2018
 */
public class UserDto {
	private Integer idUserAutoGen;
	private String accessToken;
	private String userId;
	private String password;
	private String userFname;
	private String userLname;
	private String emailId;
	private Integer userRoleId;
	private String customerName;
	private String userRole;
	private String mobNo;
	private Date passwordValidity;
	private Boolean active;
	private Date createdOn;
	private Date updatedOn;
	private Integer createdBy;
	private Integer updatedBy;
	private	Integer	roleId;
	private	Integer	orgId;
	
	public Integer getOrgId() {
		return orgId;
	}


	public void setOrgId(Integer orgId) {
		this.orgId = orgId;
	}


	public Integer getRoleId() {
		return roleId;
	}


	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}


	/**
	 * @return the accessToken
	 */
	
	public String getAccessToken() {
		return accessToken;
	}

	
	public String getCustomerName() {
		return customerName;
	}


	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}


	/**
	 * @param accessToken
	 *            the accessToken to set
	 */
	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param userId
	 *            the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the userFname
	 */
	public String getUserFname() {
		return userFname;
	}

	/**
	 * @param userFname
	 *            the userFname to set
	 */
	public void setUserFname(String userFname) {
		this.userFname = userFname;
	}

	/**
	 * @return the userLname
	 */
	public String getUserLname() {
		return userLname;
	}

	/**
	 * @param userLname
	 *            the userLname to set
	 */
	public void setUserLname(String userLname) {
		this.userLname = userLname;
	}

	/**
	 * @return the emailId
	 */
	public String getEmailId() {
		return emailId;
	}

	/**
	 * @param emailId
	 *            the emailId to set
	 */
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	/**
	 * @return the userRoleId
	 */
	public Integer getUserRoleId() {
		return userRoleId;
	}

	/**
	 * @param userRoleId
	 *            the userRoleId to set
	 */
	public void setUserRoleId(Integer userRoleId) {
		this.userRoleId = userRoleId;
	}

	/**
	 * @return the role
	 */
	public String getUserRole() {
		return userRole;
	}

	/**
	 * @param role
	 *            the role to set
	 */
	public void setUserRole(String userRole) {
		this.userRole = userRole;
	}

	
	/**
	 * @return the mobNo
	 */
	public String getMobNo() {
		return mobNo;
	}

	/**
	 * @param mobNo
	 *            the mobNo to set
	 */
	public void setMobNo(String mobNo) {
		this.mobNo = mobNo;
	}

	/**
	 * @return the passwordValidity
	 */
	public Date getPasswordValidity() {
		return passwordValidity;
	}

	/**
	 * @param passwordValidity
	 *            the passwordValidity to set
	 */
	public void setPasswordValidity(Date passwordValidity) {
		this.passwordValidity = passwordValidity;
	}

	/**
	 * @return the active
	 */
	public Boolean getActive() {
		return active;
	}

	/**
	 * @param active
	 *            the active to set
	 */
	public void setActive(Boolean active) {
		this.active = active;
	}

	/**
	 * @return the createdOn
	 */
	public Date getCreatedOn() {
		return createdOn;
	}

	/**
	 * @param createdOn
	 *            the createdOn to set
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * @return the updatedOn
	 */
	public Date getUpdatedOn() {
		return updatedOn;
	}

	/**
	 * @param updatedOn
	 *            the updatedOn to set
	 */
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	/**
	 * @return the createdBy
	 */
	public Integer getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy
	 *            the createdBy to set
	 */
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the updatedBy
	 */
	public Integer getUpdatedBy() {
		return updatedBy;
	}

	/**
	 * @param updatedBy
	 *            the updatedBy to set
	 */
	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Integer getIdUserAutoGen() {
		return idUserAutoGen;
	}

	public void setIdUserAutoGen(Integer idUserAutoGen) {
		this.idUserAutoGen = idUserAutoGen;
	}

	public User getUserDto() {
		User user = new User();
		user.setUserId(userId);
		user.setPassword(password);
		user.setUserFname(userFname);
		user.setUserLname(userLname);
		user.setEmailId(emailId);
		user.setUserRoleId(userRoleId);
		user.setMobNo(mobNo);
		user.setPasswordValidity(passwordValidity);
		user.setActive(active);
		user.setCreatedOn(createdOn);
		user.setUpdatedOn(updatedOn);
		user.setCreatedBy(createdBy);
		user.setUpdatedBy(updatedBy);
		return user;
	}

	public User getUserDto(User user) {
		user.setUserId(user.getUserId());
		user.setUserFname(userFname);
		user.setUserLname(userLname);
		user.setEmailId(emailId);
		user.setUserRoleId(userRoleId);
		
		user.setMobNo(mobNo);
		user.setPasswordValidity(passwordValidity);
		user.setActive(true);
		user.setCreatedOn(new Date());
		user.setUpdatedOn(new Date());
		user.setCreatedBy(createdBy);
		user.setUpdatedBy(updatedBy);
		return user;
	}


	@Override
	public String toString() {
		return "UserDto [idUserAutoGen=" + idUserAutoGen + ", accessToken=" + accessToken + ", userId=" + userId
				+ ", password=" + password + ", userFname=" + userFname + ", userLname=" + userLname + ", emailId="
				+ emailId + ", userRoleId=" + userRoleId + ", customerName=" + customerName + ", userRole=" + userRole
				+ ", mobNo=" + mobNo + ", passwordValidity=" + passwordValidity + ", active=" + active + ", createdOn="
				+ createdOn + ", updatedOn=" + updatedOn + ", createdBy=" + createdBy + ", updatedBy=" + updatedBy
				+ ", roleId=" + roleId + ", orgId=" + orgId + "]";
	}


//	@Override
//	public String toString() {
//		return "UserDto [idUserAutoGen=" + idUserAutoGen + ", accessToken=" + accessToken + ", userId=" + userId
//				+ ", password=" + password + ", userFname=" + userFname + ", userLname=" + userLname + ", emailId="
//				+ emailId + ", userRoleId=" + userRoleId + ", customerName=" + customerName + ", userRole=" + userRole
//				+ ", mobNo=" + mobNo + ", passwordValidity=" + passwordValidity + ", active=" + active + ", createdOn="
//				+ createdOn + ", updatedOn=" + updatedOn + ", createdBy=" + createdBy + ", updatedBy=" + updatedBy
//				+ ", roleId=" + roleId + "]";
//	}
//


}
