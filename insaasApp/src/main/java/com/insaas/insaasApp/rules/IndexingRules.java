//package com.insaas.insaasApp.rules;
//
//import com.insaas.insaasApp.helper.GetPropertyValues;
//import com.insaas.insaasApp.indexing.FileValidator;
//import com.insaas.insaasApp.model.ExclusionParameters;
//import com.insaas.insaasApp.tracking.Trackable;
//import com.insaas.insaasApp.tracking.TrackingFactory;
//
//import org.apache.commons.lang.StringUtils;
//
//import java.io.File;
//
///**
// * Created with IntelliJ IDEA.
// * User: Suryapc
// * Date: 3/11/15
// * Time: 6:37 PM
// * To change this template use File | Settings | File Templates.
// */
//public class IndexingRules {
//    private static Trackable trackable= TrackingFactory.getTrackable();
//    private static GetPropertyValues getPropertyValues=new GetPropertyValues();
//    public static boolean shouldIndex(File file, String coreName, ExclusionParameters exclusionParameters) {
//        //check file pattern
//        if(!FileValidator.shouldIndexFile(file, exclusionParameters))
//            return false;
//        //check if reindexing is enabled
//        if(getPropertyValues.getBooleanProperty("reindex.files",false))
//            return true;
//
//        //check if file has been indexed
//        return !trackable.hasBeenIndexed(file,coreName);
//    }
//}
