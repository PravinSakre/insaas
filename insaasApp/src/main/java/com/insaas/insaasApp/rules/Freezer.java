//package com.insaas.insaasApp.rules;
//
//
//import org.apache.log4j.Logger;
//
///**
// * User: Suryapc
// * Date: 4/21/15
// * Time: 4:20 PM
// * To change this template use File | Settings | File Templates.
// */
//public class Freezer {
//    private static final Logger log = Logger.getLogger(Freezer.class);
//    public static final Freezer freezer = new Freezer();
//    private boolean indexingFrozen = false;
//    private boolean readingFrozen = false;
//    private final long FREEZE_SECONDS = 30;
//
//    public boolean isIndexingFrozen() {
//        return indexingFrozen;
//    }
//
//    public boolean isReadingFrozen() {
//        return readingFrozen;
//    }
//
//    public void setReadingFrozen(boolean readingFrozen) {
//        this.readingFrozen = readingFrozen;
//        if(readingFrozen) {
//            new java.util.Timer().schedule(
//                    new java.util.TimerTask() {
//                        @Override
//                        public void run() {
//                            freezer.setReadingFrozen(false);
//                            log.info("queue freezer thawed");
//                        }
//                    },
//                    FREEZE_SECONDS*1000
//            );
//        }
//    }
//
//    public void setIndexingFrozen(boolean indexingFrozen) {
//        this.indexingFrozen = indexingFrozen;
//        if(indexingFrozen) {
//            new java.util.Timer().schedule(
//                    new java.util.TimerTask() {
//                        @Override
//                        public void run() {
//                            setIndexingFrozen(false);
//                            log.info("index freezer thawed");
//                        }
//                    },
//                    FREEZE_SECONDS*1000
//            );
//        }
//    }
//}
