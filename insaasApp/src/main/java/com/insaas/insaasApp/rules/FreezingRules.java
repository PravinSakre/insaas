//package com.insaas.insaasApp.rules;
//
//
//import org.apache.log4j.Logger;
//
//import com.insaas.insaasApp.model.IndexingMetrics;
//
///**
// * User: Suryapc
// * Date: 4/21/15
// * Time: 4:23 PM
// * To change this template use File | Settings | File Templates.
// */
//public class FreezingRules {
//    private static final Logger log = Logger.getLogger(FreezingRules.class);
//    public static final FreezingRules FREEZING_RULES = new FreezingRules();
//
//    private long previousIndexingCount = 0;
//    private static final long indexingBatch = 1000;
//    private static final long queueBatch = 1000;
//    private static final long sleepTime = 30;
//
//    public boolean shouldFreezeIndexing(IndexingMetrics indexingMetrics) {
//        if(indexingMetrics.getFilesIndexed() - previousIndexingCount > indexingBatch) {
//            log.info("got to freeze indexing");
//            previousIndexingCount = indexingMetrics.getFilesIndexed();
//            Freezer.freezer.setIndexingFrozen(true);
//        }
//        return Freezer.freezer.isIndexingFrozen();
//    }
//
//    public boolean shouldFreezeQueue(IndexingMetrics indexingMetrics) {
//        if(indexingMetrics.getFilesInQueue() > queueBatch) {
//            log.info("got to freeze queue");
//            Freezer.freezer.setReadingFrozen(true);
//        }
//        return Freezer.freezer.isReadingFrozen();
//    }
//
//    public void freezeIndexingIfNeeded(IndexingMetrics indexingMetrics) {
//        if(shouldFreezeIndexing(indexingMetrics))
//            sleep(sleepTime * 1000);
//    }
//
//    public void freezeQueueingIfNeeded(IndexingMetrics indexingMetrics) {
//        if(shouldFreezeQueue(indexingMetrics))
//            sleep(sleepTime * 1000);
//    }
//
//    private void sleep(long l) {
//        try {
//            Thread.sleep(l);
//        } catch (InterruptedException e) {
//            log.error("error sleeping ", e);
//        }
//    }
//}
