package com.insaas.insaasApp.util;

import org.hashids.Hashids;
import org.springframework.security.crypto.password.StandardPasswordEncoder;
public class EncoderUtil {

	public static final StandardPasswordEncoder pwdEncoder = new StandardPasswordEncoder("tadalin-rest-application");

	public static final Hashids HASHID = new Hashids("TadalinHashId", 6, "0123456789abcdefghijklmnopqrstuvwxyz");

	public static String hashidEncode(Long id) {
		return HASHID.encode(id);
	}

	public static long[] hashidDecode(String str) {
		return HASHID.decode(str);
	}
}
