package com.insaas.insaasApp.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

@Entity
@Table(name = "Customer")
@Audited
public class Customer {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer customerId;
	private String customerName;
	private	Boolean	active;
	public Boolean getActive() {
		return active;
	}
	public void setActive(Boolean active) {
		this.active = active;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	private String coreName;
	private String analysisStatement;
	
	public String getCoreName() {
		return coreName;
	}
	public void setCoreName(String coreName) {
		this.coreName = coreName;
	}
	public Integer getCustomerId() {
		return customerId;
	}
	
	
	public String getAnalysisStatement() {
		return analysisStatement;
	}
	public void setAnalysisStatement(String analysisStatement) {
		this.analysisStatement = analysisStatement;
	}
	@Override
	public String toString() {
		return "Customer [customerId=" + customerId + ", customerName=" + customerName + ", coreName=" + coreName
				+ ", analysisStatement=" + analysisStatement + "]";
	}
	

}
