
package com.insaas.insaasApp.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.envers.Audited;
/**
 * 
 * Represents an User entity class will have attributes
 * @author BrainWave Consulting
 * @since Dec 24, 2018
 */
@Entity
@Table(name = "user")
@Audited
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer idUserAutoGen;
	private String userId;
	private String password;
	private String userFname;
	private String userLname;
	private String emailId;
	private Integer userRoleId;
	private String mobNo;
	private Date passwordValidity;
	private Boolean active;
	private Date createdOn;
	private Date updatedOn;
	private Integer createdBy;
	private Integer updatedBy;
	private Integer orgId;
	 
	public Integer getOrgId() {
		return orgId;
	}


	public void setOrgId(Integer orgId) {
		this.orgId = orgId;
	}


	/**
	 * @return the idUserAutoGen
	 */
	public Integer getIdUserAutoGen() {
		return idUserAutoGen;
	}


	/**
	 * @param idUserAutoGen the idUserAutoGen to set
	 */
	public void setIdUserAutoGen(Integer idUserAutoGen) {
		this.idUserAutoGen = idUserAutoGen;
	}


	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}


	/**
	 * @param userId the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}


	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}


	/**
	 * @param password the password to set
	 * @return 
	 */
	public String setPassword(String password) {
		return this.password = password;
	}


	/**
	 * @return the userFname
	 */
	public String getUserFname() {
		return userFname;
	}


	/**
	 * @param userFname the userFname to set
	 */
	public void setUserFname(String userFname) {
		this.userFname = userFname;
	}


	/**
	 * @return the userLname
	 */
	public String getUserLname() {
		return userLname;
	}


	/**
	 * @param userLname the userLname to set
	 */
	public void setUserLname(String userLname) {
		this.userLname = userLname;
	}


	/**
	 * @return the emailId
	 */
	public String getEmailId() {
		return emailId;
	}


	/**
	 * @param emailId the emailId to set
	 */
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}


	/**
	 * @return the userRoleId
	 */
	public Integer getUserRoleId() {
		return userRoleId;
	}


	/**
	 * @param userRoleId the userRoleId to set
	 */
	public void setUserRoleId(Integer userRoleId) {
		this.userRoleId = userRoleId;
	}

	/**
	 * @return the mobNo
	 */
	public String getMobNo() {
		return mobNo;
	}


	/**
	 * @param mobNo the mobNo to set
	 */
	public void setMobNo(String mobNo) {
		this.mobNo = mobNo;
	}


	

	/**
	 * @return the passwordValidity
	 */
	public Date getPasswordValidity() {
		return passwordValidity;
	}


	/**
	 * @param passwordValidity the passwordValidity to set
	 */
	public void setPasswordValidity(Date passwordValidity) {
		this.passwordValidity = passwordValidity;
	}


	/**
	 * @return the active
	 */
	public Boolean getActive() {
		return active;
	}


	/**
	 * @param active the active to set
	 */
	public void setActive(Boolean active) {
		this.active = active;
	}


	/**
	 * @return the createdOn
	 */
	public Date getCreatedOn() {
		return createdOn;
	}


	/**
	 * @param createdOn the createdOn to set
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}


	/**
	 * @return the updatedOn
	 */
	public Date getUpdatedOn() {
		return updatedOn;
	}


	/**
	 * @param updatedOn the updatedOn to set
	 */
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}


	/**
	 * @return the createdBy
	 */
	public Integer getCreatedBy() {
		return createdBy;
	}


	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}


	/**
	 * @return the updatedBy
	 */
	public Integer getUpdatedBy() {
		return updatedBy;
	}


	/**
	 * @param updatedBy the updatedBy to set
	 */
	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "User [idUserAutoGen=" + idUserAutoGen + ", userId=" + userId + ", password=" + password + ", userFname="
				+ userFname + ", userLname=" + userLname + ", emailId=" + emailId + ", userRoleId=" + userRoleId
				+  ", mobNo=" + mobNo + ", passwordValidity="
				+ passwordValidity + ", active=" + active + ", createdOn=" + createdOn + ", updatedOn=" + updatedOn
				+ ", createdBy=" + createdBy + ", updatedBy=" + updatedBy + "]";
	}
	
}
