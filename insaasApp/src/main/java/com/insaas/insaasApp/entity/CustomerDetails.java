package com.insaas.insaasApp.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

@Entity
@Table(name = "CustomerDetails")
@Audited
public class CustomerDetails {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer custDetailsId;
	private Integer customerId;
	private String sourceOfInfo;
	private String uRL;
	private	Boolean	active;
	public Boolean getActive() {
		return active;
	}
	public void setActive(Boolean active) {
		this.active = active;
	}
	private String aPI_Key;
	private String project_Token;
	
	private String run_Token;
	public Integer getCustDetailsId() {
		return custDetailsId;
	}
	public void setCustDetailsId(Integer custDetailsId) {
		this.custDetailsId = custDetailsId;
	}
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	public String getuRL() {
		return uRL;
	}
	public void setuRL(String uRL) {
		this.uRL = uRL;
	}
	public String getaPI_Key() {
		return aPI_Key;
	}
	public void setaPI_Key(String aPI_Key) {
		this.aPI_Key = aPI_Key;
	}
	public String getProject_Token() {
		return project_Token;
	}
	public void setProject_Token(String project_Token) {
		this.project_Token = project_Token;
	}
	
	public String getRun_Token() {
		return run_Token;
	}
	public void setRun_Token(String run_Token) {
		this.run_Token = run_Token;
	}
	public String getSourceOfInfo() {
		return sourceOfInfo;
	}
	public void setSourceOfInfo(String sourceOfInfo) {
		this.sourceOfInfo = sourceOfInfo;
	}
	@Override
	public String toString() {
		return "CustomerDetails [custDetailsId=" + custDetailsId + ", customerId=" + customerId + ", sourceOfInfo="
				+ sourceOfInfo + ", uRL=" + uRL + ", active=" + active + ", aPI_Key=" + aPI_Key + ", project_Token="
				+ project_Token + ", run_Token=" + run_Token + "]";
	}



	
	
	
}
