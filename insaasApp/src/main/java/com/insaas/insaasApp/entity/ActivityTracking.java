
package com.insaas.insaasApp.entity;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.envers.Audited;
/**
 * Represents an ActivityTracking entity class will have attributes
 * @author BrainWave Consulting
 * @since Dec 24, 2018
 */
@Entity
@Table(name = "activity_tracking")
@Audited
public class ActivityTracking {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer activityTrackingId;
	private String userId;
	private String accessToken;
	private Date loggedInTime;
	private Date loggedOutTime;
	public Integer getActivityTrackingId() {
		return activityTrackingId;
	}
	public void setActivityTrackingId(Integer activityTrackingId) {
		this.activityTrackingId = activityTrackingId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getAccessToken() {
		return accessToken;
	}
	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}
	
	public Date getLoggedInTime() {
		return loggedInTime;
	}
	public void setLoggedInTime(Date loggedInTime) {
		this.loggedInTime = loggedInTime;
	}
	public Date getLoggedOutTime() {
		return loggedOutTime;
	}
	public void setLoggedOutTime(Date loggedOutTime) {
		this.loggedOutTime = loggedOutTime;
	}
	@Override
	public String toString() {
		return "ActivityTracking [activityTrackingId=" + activityTrackingId + ", userId=" + userId + ", accessToken="
				+ accessToken + ", loggedInTime=" + loggedInTime + ", loggedOutTime=" + loggedOutTime + "]";
	}
}