package com.insaas.insaasApp.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

@Entity
@Table(name = "core_access")
@Audited
public class CoreAccess {
@Id
@GeneratedValue(strategy = GenerationType.AUTO)
private Integer coreId;
private Integer orgId;
private String coreName;
private String coreType;
public Integer getCoreId() {
	return coreId;
}
public void setCoreId(Integer coreId) {
	this.coreId = coreId;
}
public String getCoreName() {
	return coreName;
}
public void setCoreName(String coreName) {
	this.coreName = coreName;
}
public String getCoreType() {
	return coreType;
}
public void setCoreType(String coreType) {
	this.coreType = coreType;
}
public Integer getOrgId() {
	return orgId;
}
public void setOrgId(Integer orgId) {
	this.orgId = orgId;
}
}
