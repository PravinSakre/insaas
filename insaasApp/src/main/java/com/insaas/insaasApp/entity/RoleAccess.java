package com.insaas.insaasApp.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.envers.Audited;
/**
 * Represents an RoleAccess entity class will have attributes
 * @author BrainWave Consulting
 * @since Dec 24, 2018
 */
@Entity
@Table(name = "role_access")
@Audited
public class RoleAccess {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer roleAccessId;
	private Integer roleId;
	private Integer accessItemId;
	private Boolean active;
	private Date createdOn;
	private Date updatedOn;
	private Integer createdBy;
	private Integer updatedBy;

	/**
	 * @return the roleAccessId
	 */
	public Integer getRoleAccessId() {
		return roleAccessId;
	}

	/**
	 * @param roleAccessId
	 *            the roleAccessId to set
	 */
	public void setRoleAccessId(Integer roleAccessId) {
		this.roleAccessId = roleAccessId;
	}

	/**
	 * @return the roleId
	 */
	public Integer getRoleId() {
		return roleId;
	}

	/**
	 * @param roleId
	 *            the roleId to set
	 */
	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	/**
	 * @return the accessItemId
	 */
	public Integer getAccessItemId() {
		return accessItemId;
	}

	/**
	 * @param accessItemId
	 *            the accessItemId to set
	 */
	public void setAccessItemId(Integer accessItemId) {
		this.accessItemId = accessItemId;
	}

	
	/**
	 * @return the active
	 */
	public Boolean getActive() {
		return active;
	}

	/**
	 * @param active
	 *            the active to set
	 */
	public void setActive(Boolean active) {
		this.active = active;
	}

	/**
	 * @return the createdOn
	 */
	public Date getCreatedOn() {
		return createdOn;
	}

	/**
	 * @param createdOn
	 *            the createdOn to set
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * @return the updatedOn
	 */
	public Date getUpdatedOn() {
		return updatedOn;
	}

	/**
	 * @param updatedOn
	 *            the updatedOn to set
	 */
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	/**
	 * @return the createdBy
	 */
	public Integer getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy
	 *            the createdBy to set
	 */
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the updatedBy
	 */
	public Integer getUpdatedBy() {
		return updatedBy;
	}

	/**
	 * @param updatedBy
	 *            the updatedBy to set
	 */
	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "RoleAccess [roleAccessId=" + roleAccessId + ", roleId=" + roleId + ", accessItemId=" + accessItemId
				+  ", active=" + active + ", createdOn=" + createdOn + ", updatedOn="
				+ updatedOn + ", createdBy=" + createdBy + ", updatedBy=" + updatedBy + ", getRoleAccessId()="
				+ getRoleAccessId() + ", getRoleId()=" + getRoleId() + ", getAccessItemId()=" + getAccessItemId()
				+ ", getActive()=" + getActive() + ", getCreatedOn()="
				+ getCreatedOn() + ", getUpdatedOn()=" + getUpdatedOn() + ", getCreatedBy()=" + getCreatedBy()
				+ ", getUpdatedBy()=" + getUpdatedBy() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode()
				+ ", toString()=" + super.toString() + "]";
	}

}
