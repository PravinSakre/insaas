
package com.insaas.insaasApp.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.envers.Audited;
/**
 * 
 * Represents an AccessItemMaster entity class will have attributes
 * @author BrainWave Consulting
 * @since Dec 24, 2018
 */
@Entity
@Table(name = "access_item_master")
@Audited
public class AccessItemMaster {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private	Integer	accessItemId;
	private	String	accessItemName;
	private	String	accessItemType;
	private String screenDisplayMenus;
	private String screenUrl;
	private	Boolean	active;
	private	Date	createdOn;
	private	Date	updatedOn;
	private	Integer	createdBy;
	private	Integer	updatedBy;
	/**
	 * @return the accessItemId
	 */
	public Integer getAccessItemId() {
		return accessItemId;
	}
	/**
	 * @return the screenUrl
	 */
	public String getScreenUrl() {
		return screenUrl;
	}
	/**
	 * @param screenUrl the screenUrl to set
	 */
	public void setScreenUrl(String screenUrl) {
		this.screenUrl = screenUrl;
	}
	/**
	 * @param accessItemId the accessItemId to set
	 */
	public void setAccessItemId(Integer accessItemId) {
		this.accessItemId = accessItemId;
	}
	/**
	 * @return the accessItemName
	 */
	public String getAccessItemName() {
		return accessItemName;
	}
	/**
	 * @param accessItemName the accessItemName to set
	 */
	public void setAccessItemName(String accessItemName) {
		this.accessItemName = accessItemName;
	}
	/**
	 * @return the accessItemType
	 */
	public String getAccessItemType() {
		return accessItemType;
	}
	/**
	 * @param accessItemType the accessItemType to set
	 */
	public void setAccessItemType(String accessItemType) {
		this.accessItemType = accessItemType;
	}
	/**
	 * @return the active
	 */
	public Boolean getActive() {
		return active;
	}
	/**
	 * @param active the active to set
	 */
	public void setActive(Boolean active) {
		this.active = active;
	}
	/**
	 * @return the createdOn
	 */
	public Date getCreatedOn() {
		return createdOn;
	}
	/**
	 * @param createdOn the createdOn to set
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	/**
	 * @return the updatedOn
	 */
	public Date getUpdatedOn() {
		return updatedOn;
	}
	/**
	 * @param updatedOn the updatedOn to set
	 */
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	/**
	 * @return the createdBy
	 */
	public Integer getCreatedBy() {
		return createdBy;
	}
	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	/**
	 * @return the updatedBy
	 */
	public Integer getUpdatedBy() {
		return updatedBy;
	}
	/**
	 * @param updatedBy the updatedBy to set
	 */
	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}
	/**
	 * @return the screenDisplayMenus
	 */
	public String getScreenDisplayMenus() {
		return screenDisplayMenus;
	}
	/**
	 * @param screenDisplayMenus the screenDisplayMenus to set
	 */
	public void setScreenDisplayMenus(String screenDisplayMenus) {
		this.screenDisplayMenus = screenDisplayMenus;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "AccessItemMaster [accessItemId=" + accessItemId + ", accessItemName=" + accessItemName
				+ ", accessItemType=" + accessItemType + ", active=" + active + ", createdOn=" + createdOn
				+ ", updatedOn=" + updatedOn + ", createdBy=" + createdBy + ", updatedBy=" + updatedBy + "]";
	}
	
				
}
