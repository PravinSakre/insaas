package com.insaas.insaasApp.serviceImpl;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.zip.GZIPInputStream;
import java.net.HttpURLConnection;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.common.SolrInputDocument;
import org.apache.solr.internal.csv.CSVParser;
import org.apache.solr.internal.csv.CSVStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.insaas.insaasApp.common.ApplicationConstant;
import com.insaas.insaasApp.common.Constant;
import com.insaas.insaasApp.common.MessagesConstant;
//import com.insaas.insaasApp.dto.ScrappedDataDto;
import com.insaas.insaasApp.entity.Customer;
import com.insaas.insaasApp.entity.CustomerDetails;
import com.insaas.insaasApp.repository.CustomerDetailsRepository;
import com.insaas.insaasApp.repository.CustomerRepository;
//import com.insaas.insaasApp.service.RunPythonScriptService;
import com.insaas.insaasApp.service.ScrappingService;
import com.insaas.insaasApp.service.SolrService;
import com.opencsv.CSVWriter;

@Service
public class ScrappingServiceImpl implements ScrappingService {
	private static final Logger logger = LogManager.getLogger(ScrappingServiceImpl.class.getName());

//	@Autowired
//	private RunPythonScriptService runPythonScriptService;
	@Autowired
	private SolrService solrService;

	@Autowired
	private CustomerDetailsRepository customerDetailsRepository;
	@Autowired
	private CustomerRepository customerRepository;

	private static final String CSV_SEPARATOR = ",";

	/*
	 * @Override public String getProject() throws Exception { StringBuilder
	 * responseText = new StringBuilder(); try {
	 * 
	 * String url = MessagesConstant.PARSE_HUB_URL + "/projects/" +
	 * MessagesConstant.PROJECT_TOKEN + "/last_ready_run/data?api_key=" +
	 * MessagesConstant.API_KEY + "&format=csv"; BufferedReader bufferReader = null;
	 * int responseCode = 0; System.out.println("Connecting to " + url);
	 * URLConnection connection = null; connection = new URL(url).openConnection();
	 * connection.setDoOutput(true); responseCode = ((HttpURLConnection)
	 * connection).getResponseCode(); logger.info("responseCode=" + responseCode);
	 * if (responseCode > 0 && responseCode == 200) { InputStream inpuntStream =
	 * connection.getInputStream(); GZIPInputStream gZipInputStream = new
	 * GZIPInputStream(inpuntStream); bufferReader = new BufferedReader(new
	 * InputStreamReader((gZipInputStream), StandardCharsets.UTF_8)); String text =
	 * new String(""); File file = new File(MessagesConstant.CSV_FILE_PATH +
	 * "getProjectCSV.csv"); FileWriter outputfile = new FileWriter(file); // create
	 * CSVWriter object filewriter object as parameter CSVWriter writer = new
	 * CSVWriter(outputfile); while ((text = bufferReader.readLine()) != null) { if
	 * (null != text && !text.isEmpty()) { // writeDataToCsvFile();
	 * responseText.append(text); writeDataToCsvFile(text.split(","), writer); }
	 * 
	 * } writer.close(); logger.info("responseText=" + responseText); } } catch
	 * (Exception e) { logger.error("Exception is=", e); e.printStackTrace(); }
	 * return responseText.toString(); }
	 */

	/*
	 * @Override public String runProject() throws Exception { String responseText =
	 * ""; int responseCode = 0; try { String url = MessagesConstant.PARSE_HUB_URL +
	 * "/runs/" + MessagesConstant.RUN_TOKEN + "/data?api_key=" +
	 * MessagesConstant.API_KEY + "&format=csv"; BufferedReader bufferReader = null;
	 * 
	 * System.out.println("Connecting to " + url); URLConnection connection = null;
	 * connection = new URL(url).openConnection(); connection.setDoOutput(true);
	 * responseCode = ((HttpURLConnection) connection).getResponseCode();
	 * logger.info("responseCode=" + responseCode); if (responseCode > 0 &&
	 * responseCode == 200) { responseText = "Successfully Runned";
	 * logger.info("Project Runned successfully"); } } catch (Exception e) {
	 * logger.error("Exception is=", e); e.printStackTrace(); } return responseText;
	 * }
	 */

	/*
	 * @Override public String listAllProjects() { StringBuilder responseText = new
	 * StringBuilder(); try { String url = MessagesConstant.PARSE_HUB_URL +
	 * "/projects?api_key=" + MessagesConstant.API_KEY + "&format=csv";
	 * BufferedReader bufferReader = null; int responseCode = 0;
	 * System.out.println("Connecting to " + url); URLConnection connection = null;
	 * connection = new URL(url).openConnection(); connection.setDoOutput(true);
	 * responseCode = ((HttpURLConnection) connection).getResponseCode();
	 * logger.info("responseCode=" + responseCode); if (responseCode > 0 &&
	 * responseCode == 200) { InputStream inpuntStream =
	 * connection.getInputStream(); // GZIPInputStream gZipInputStream=new
	 * GZIPInputStream(inpuntStream); bufferReader = new BufferedReader(new
	 * InputStreamReader((inpuntStream), StandardCharsets.UTF_8)); String text = new
	 * String(""); File file = new File(MessagesConstant.CSV_FILE_PATH +
	 * "listProject.csv"); FileWriter outputfile = new FileWriter(file);
	 * 
	 * // create CSVWriter object filewriter object as parameter CSVWriter writer =
	 * new CSVWriter(outputfile); while ((text = bufferReader.readLine()) != null) {
	 * if (null != text && !text.isEmpty()) { responseText.append(text);
	 * 
	 * writeDataToCsvFile(text.split(","), writer); }
	 * 
	 * } writer.close(); logger.info("responseText=" + responseText); } } catch
	 * (Exception e) { logger.error("Exception is=", e); e.printStackTrace(); }
	 * return String.valueOf(responseText); }
	 */
	/*
	 * @Override public String getARun() throws Exception { StringBuilder
	 * responseText = new StringBuilder(); try { String url =
	 * MessagesConstant.PARSE_HUB_URL + "/runs/" + MessagesConstant.RUN_TOKEN +
	 * "?api_key=" + MessagesConstant.API_KEY + "&format=csv"; BufferedReader
	 * bufferReader = null; int responseCode = 0;
	 * System.out.println("Connecting to " + url); URLConnection connection = null;
	 * connection = new URL(url).openConnection(); connection.setDoOutput(true);
	 * responseCode = ((HttpURLConnection) connection).getResponseCode();
	 * logger.info("responseCode=" + responseCode); if (responseCode > 0 &&
	 * responseCode == 200) { InputStream inpuntStream =
	 * connection.getInputStream(); // GZIPInputStream gZipInputStream=new
	 * GZIPInputStream(inpuntStream); bufferReader = new BufferedReader(new
	 * InputStreamReader((inpuntStream), StandardCharsets.UTF_8)); String text = new
	 * String(""); File file = new File(MessagesConstant.CSV_FILE_PATH +
	 * "getRunCSv.csv"); FileWriter outputfile = new FileWriter(file);
	 * 
	 * // create CSVWriter object filewriter object as parameter CSVWriter writer =
	 * new CSVWriter(outputfile); while ((text = bufferReader.readLine()) != null) {
	 * if (null != text && !text.isEmpty()) { // writeDataToCsvFile();
	 * responseText.append(text); writeDataToCsvFile(text.split(","), writer); }
	 * 
	 * } writer.close(); logger.info("responseText=" + responseText); } } catch
	 * (Exception e) { logger.error("Exception is=", e); e.printStackTrace(); }
	 * 
	 * return String.valueOf(responseText); }
	 */

//Calling method for scrapping....
	@Override
	public String getADataForRun(Integer CustDetailsId) throws Exception {
		return null;
	}

	/*
	 * @Override public String getALastReadyData() throws Exception { StringBuilder
	 * responseText = new StringBuilder(); try {
	 * 
	 * String url = MessagesConstant.PARSE_HUB_URL + "/projects/" +
	 * MessagesConstant.PROJECT_TOKEN + "/last_ready_run/data?api_key=" +
	 * MessagesConstant.API_KEY + "&format=csv"; BufferedReader bufferReader = null;
	 * int responseCode = 0; System.out.println("Connecting to " + url);
	 * URLConnection connection = null; connection = new URL(url).openConnection();
	 * connection.setDoOutput(true); responseCode = ((HttpURLConnection)
	 * connection).getResponseCode(); logger.info("responseCode=" + responseCode);
	 * logger.info("type=" + connection.getContentEncoding()); if (responseCode > 0
	 * && responseCode == 200) { InputStream inpuntStream =
	 * connection.getInputStream(); GZIPInputStream gZipInputStream = new
	 * GZIPInputStream(inpuntStream); bufferReader = new BufferedReader(new
	 * InputStreamReader((gZipInputStream), StandardCharsets.UTF_8)); String text =
	 * new String(""); File file = new File(MessagesConstant.CSV_FILE_PATH +
	 * "getLastReadyData.csv"); FileWriter outputfile = new FileWriter(file);
	 * 
	 * // create CSVWriter object filewriter object as parameter CSVWriter writer =
	 * new CSVWriter(outputfile); while ((text = bufferReader.readLine()) != null) {
	 * if (null != text && !text.isEmpty()) { // writeDataToCsvFile();
	 * responseText.append(text); writeDataToCsvFile(text.split(","), writer); }
	 * 
	 * } writer.close(); logger.info("Start ....."); logger.info("responseText=" +
	 * responseText); logger.info("End..."); } } catch (Exception e) {
	 * logger.error("Exception is=", e); e.printStackTrace(); }
	 * 
	 * return String.valueOf(responseText); }
	 */
	/*
	 * @Override public String cancelARun() { try { String url =
	 * "https://www.parsehub.com/api/v2/runs/" + MessagesConstant.RUN_TOKEN +
	 * "/cancel?api_key=" + MessagesConstant.API_KEY + "&format=csv"; BufferedReader
	 * bufferReader = null; int responseCode = 0;
	 * System.out.println("Connecting to " + url); URLConnection connection = null;
	 * connection = new URL(url).openConnection(); connection.setDoOutput(true);
	 * 
	 * StringBuilder responseText = new StringBuilder();
	 * 
	 * responseCode = ((HttpURLConnection) connection).getResponseCode();
	 * logger.info("responseCode=" + responseCode); if (responseCode > 0 &&
	 * responseCode == 200) { logger.info("Run stop"); } else {
	 * logger.info("Project aleready stopped"); } } catch (Exception e) {
	 * logger.error("Exception is=", e); e.printStackTrace(); } return null; }
	 */
	/*
	 * @Override public String deleteARun() { try { String url =
	 * "https://www.parsehub.com/api/v2/runs/" + MessagesConstant.RUN_TOKEN +
	 * "?api_key=" + MessagesConstant.API_KEY + "&format=csv"; BufferedReader
	 * bufferReader = null; int responseCode = 0;
	 * System.out.println("Connecting to " + url); URLConnection connection = null;
	 * connection = new URL(url).openConnection(); connection.setDoOutput(true);
	 * 
	 * String responseText = new String("");
	 * 
	 * responseCode = ((HttpURLConnection) connection).getResponseCode();
	 * logger.info("responseCode=" + responseCode); if (responseCode > 0 &&
	 * responseCode == 200) { logger.info("Project deleted successfully"); } } catch
	 * (Exception e) { logger.error("Exception is=", e); e.printStackTrace(); }
	 * return null; }
	 */
	public void writeDataToCsvFile(String[] data, CSVWriter writer) throws Exception {
		try {
			writer.writeNext(data);
		} catch (Exception e) {
			logger.error("Cannot writting to csv file");

		}
	}

	public void mergeDataWhenUsersAreSameSeparated() throws Exception {
		SolrServiceImpl solrServiceImpl = new SolrServiceImpl();
		ShortestDistAlgoServiceImpl shortestDistAlgoServiceImpl = new ShortestDistAlgoServiceImpl();
		// Creating solr client & connecting to server

		// SolrClient solrClient =
		// solrServiceImpl.connectToSolrServer("http://34.68.14.7:8983/solr/bwc_core");
		SolrClient solrClient = solrServiceImpl.connectToSolrServer("http://192.168.1.225:8983/solr/bavaria", "");
		BufferedReader reader;
		SolrInputDocument solrInputDocument = new SolrInputDocument();

		InputStream input = null;
		Properties properties = new Properties();
		input = new FileInputStream(Constant.PROPERTY_FILE_PATH);
		properties.load(input);

		// List<ScrappedDataDto> scrappedDataList = new ArrayList<>();
		List<SolrInputDocument> docs = new ArrayList<>();
		// File to be load into solr

		// String csvFile =
		// "D:/SVN_INSAAS/tags/dev/source/insaasApplication/insaasApp/xmlFiles/motorTalkFiltered_prediction2.csv";
		// String csvFile =
		// "D:/SVN_INSAAS/tags/dev/source/insaasApplication/insaasApp/xmlFiles/VHVData/motor_talk_vhv.csv";
		String csvFile = "D:\\M2_Pc\\InSaas\\insaasSouce\\tags\\dev\\solr\\UploadedCustomerData\\VHV\\motor_talk_vhv_For_ValidationCsv.csv";

		try {
			reader = new BufferedReader(new InputStreamReader(new FileInputStream(csvFile), "UTF-8"));
			String[][] data = (new CSVParser(reader, CSVStrategy.EXCEL_STRATEGY)).getAllValues();
			logger.info("data size==" + data.length);
			String solrInputDocFields = properties.getProperty(Constant.SOLR_INPUT_DOC_FIELDS);
			String[] defineColumns = solrInputDocFields.split(";");
			String userName, feedback, aspect, sentiment, source, date;
			String user = "";
			String columnName = "";
			int documentCounter = 0;
			int dataLength = data.length;
			userName = feedback = aspect = sentiment = source = date = "";
			for (int dataIndex = 1; dataIndex <= (data.length) - 1; dataIndex++) {

				for (int defineColumnIndex = 0; defineColumnIndex < defineColumns.length; defineColumnIndex++) {
					for (int dataColumnIndex = 0; dataColumnIndex < data[0].length; dataColumnIndex++) {

						if (data[0][dataColumnIndex].equalsIgnoreCase(defineColumns[defineColumnIndex])) {
							columnName = data[0][dataColumnIndex];
							// logger.info("columnName :: =" + columnName);
							if (StringUtils.isEmpty(user) && columnName.equals(ApplicationConstant.USER_NAME)) {
								userName = String.valueOf(data[dataIndex][dataColumnIndex]);
								// logger.info("user=" + userName + "\tusername=" + userName);
								user = userName;
							} else if (columnName.equals(ApplicationConstant.USER_NAME)) {
								userName = String.valueOf(data[dataIndex][dataColumnIndex]);
							}
							// logger.info("user="+user+"\tusername="+userName);
							if (user.equals(userName)) {
								switch (columnName) {
								case ApplicationConstant.FEEDBACK:
									feedback += " " + String.valueOf(data[dataIndex][dataColumnIndex]);
									// logger.info("feedback="+feedback);
									break;
								case ApplicationConstant.ASPECT:
									aspect += "," + String.valueOf(data[dataIndex][dataColumnIndex]);
									// logger.info("aspect="+aspect);
									break;
								case ApplicationConstant.DATE_TIME:
									date = String.valueOf(data[dataIndex][dataColumnIndex]);
									// scrappedDataDto.setDate(date);
									break;

								case ApplicationConstant.SENTIMENT:
									sentiment = String.valueOf(data[dataIndex][dataColumnIndex]);
									// scrappedDataDto.setSentiment(sentiment);
									break;

								case ApplicationConstant.SOURCE_OF_INFO:
									source = String.valueOf(data[dataIndex][dataColumnIndex]);
									// scrappedDataDto.setSourceOfInfo(source);
									break;
								}
								continue;
							} else {
								// logger.info("in else user="+user+"\tusername="+userName);

								if (user != "") {

									logger.info("user=" + user);
									String gender = shortestDistAlgoServiceImpl.getGender(user);
									// scrappedDataDto.setGender(gender);
									solrInputDocument.setField(ApplicationConstant.USER_NAME,
											String.valueOf(user.hashCode()));
									solrInputDocument.setField(ApplicationConstant.DATE_TIME, date);
									solrInputDocument.setField(ApplicationConstant.FEEDBACK, feedback);
									solrInputDocument.setField(ApplicationConstant.FEEDBACK_HASH_VALUE,
											String.valueOf(feedback.hashCode()));
									solrInputDocument.setField(ApplicationConstant.ASPECT, aspect);
									solrInputDocument.setField(ApplicationConstant.SENTIMENT, sentiment);
									solrInputDocument.setField(ApplicationConstant.SOURCE_OF_INFO, source);
									solrInputDocument.setField(ApplicationConstant.GENDER, gender);
									solrClient.add(solrInputDocument);
									solrClient.commit();

									user = "";
								}
							}

							switch (columnName) {
							case ApplicationConstant.FEEDBACK:
								feedback = String.valueOf(data[dataIndex][dataColumnIndex]);
								// scrappedDataDto.setFeedback(feedback);
								break;
							case ApplicationConstant.ASPECT:
								String aspectList[] = data[dataIndex][dataColumnIndex].split(",");
								solrInputDocument.setField(columnName, aspectList);
								// scrappedDataDto.setAspect(aspect);
								break;
							case ApplicationConstant.DATE_TIME:
								date = String.valueOf(data[dataIndex][dataColumnIndex]);
								// scrappedDataDto.setDate(date);
								break;

							case ApplicationConstant.SENTIMENT:
								sentiment = String.valueOf(data[dataIndex][dataColumnIndex]);
								// scrappedDataDto.setSentiment(sentiment);
								break;

							case ApplicationConstant.SOURCE_OF_INFO:
								source = String.valueOf(data[dataIndex][dataColumnIndex]);
								// scrappedDataDto.setSourceOfInfo(source);
								break;
							}

						}

					}

				}
				solrClient.add(solrInputDocument);
				documentCounter++;
				if (documentCounter >= 1000 || dataIndex == dataLength - 1) {
					solrClient.commit();
					documentCounter = 0;
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	// Method to load csv file data to solr
	public void loadSolrData() throws Exception {
		SolrServiceImpl solrServiceImpl = new SolrServiceImpl();
		ShortestDistAlgoServiceImpl shortestDistAlgoServiceImpl = new ShortestDistAlgoServiceImpl();
		InputStream input = null;
		Properties properties = new Properties();
		input = new FileInputStream(Constant.PROPERTY_FILE_PATH);
		properties.load(input);
		// autohaus , bavaria, insuranceAllReview
		SolrClient solrClient = solrServiceImpl
				.connectToSolrServer("http://35.234.68.118:8983/solr/insuranceAllReview", ""); // vhv
		// hannoversche
		// SolrClient solrClient =
		// solrServiceImpl.connectToSolrServer("http://192.168.1.225:8983/solr/bavaria");
		// D => Done, ND => NotDone
		// String csvFile =
		// "D:\\M2_Pc\\InSaas\\insaasSouce\\tags\\dev\\solr\\UploadedCustomerData\\VHV\\trustPilot_VHV1.csv";
		// motor_talk_vhv_For_ValidationCsv.csv diffrence total

		// vhv | vhv : CHECK24Updated300.csv :D, motor_talk_vhv_For_ValidationCsv.csv
		// :D, trustedshops_scrappedAllPages1.csv :D, trustPilot_VHV1.csv :D
		// vhv2 | bwc_core : motor_talk_vhv_For_ValidationCsv.csv :D,
		// trustedshops_scrappedAllPages1.csv :D, trustPilot_VHV1.csv :D,
		// FinalCheck247250Records1.csv :ND

		// hl1 | hannoversche : HLData250Records.csv, ekomi_data_200_pages1.csv :D,
		// EkomiRemaining.csv :D
		// hl2 | hl2 : check24_HLc.csv :D, ekomi_data_200_pages1.csv :D,
		// EkomiRemaining.csv :D

		String csvFile = "D:\\M2_Pc\\InSaas\\insaasSouce\\tags\\dev\\solr\\UploadedCustomerData\\VHV\\FinalCheck247250Records1.csv";

		// String csvFile =
		// "D:\\M2_Pc\\InSaas\\insaasSouce\\tags\\dev\\solr\\cleanedData\\motorTalk_prediction-2-1.csv";

		BufferedReader reader;

		// String line[];
		try {
//			//new InputStreamReader(
//            new FileInputStream(fileDir), "UTF8")); 
			// ekomi_predictioncleaned.csv : UTF8, motorTalk_prediction-2-1.csv : ISO-8859-1
			reader = new BufferedReader(new InputStreamReader(new FileInputStream(csvFile), "ISO-8859-1")); // ISO-8859-1
			reader.toString();
			String[][] data = (new CSVParser(reader, CSVStrategy.EXCEL_STRATEGY)).getAllValues();
			// logger.info("csvParser.getAllValues().length="+csvParser.getAllValues().length);
			String solrInputDocFields = properties.getProperty(Constant.SOLR_INPUT_DOC_FIELDS);
			// logger.info("solrInputDocFields=" + solrInputDocFields.toString());
			String[] defineColumns = solrInputDocFields.split(";");
			// logger.info("defineColumns" + defineColumns);
			SolrInputDocument solrInputDocument = new SolrInputDocument();
			List<SolrInputDocument> docs = new ArrayList<>();
			int documentCounter = 0;
			int dataLength = data.length;
			for (int dataIndex = 1; dataIndex < dataLength; dataIndex++) {

				for (int defineColumnIndex = 0; defineColumnIndex < defineColumns.length; defineColumnIndex++) {
					// logger.info("data[0][defineColumnIndex]="+data[0][defineColumnIndex]+"\t::::defineColumns[defineColumnIndex]="+defineColumns[defineColumnIndex]);
					for (int dataColumnIndex = 0; dataColumnIndex < data[0].length; dataColumnIndex++) {

						if (data[0][dataColumnIndex].equalsIgnoreCase(defineColumns[defineColumnIndex])) {
							String columnName = "";
							columnName = data[0][dataColumnIndex];
							switch (columnName) {
							case ApplicationConstant.USER_NAME:
								String gender = shortestDistAlgoServiceImpl.getGender(data[dataIndex][dataColumnIndex]);
								String value = String.valueOf(data[dataIndex][dataColumnIndex].hashCode());
								solrInputDocument.setField(columnName, value);
								solrInputDocument.setField(ApplicationConstant.GENDER, gender);
								break;
							case ApplicationConstant.FEEDBACK:
								value = String.valueOf(data[dataIndex][dataColumnIndex].hashCode());
								solrInputDocument.setField(ApplicationConstant.FEEDBACK_HASH_VALUE, value);
								solrInputDocument.setField(columnName, data[dataIndex][dataColumnIndex]);
								break;
							case ApplicationConstant.ASPECT:
								String aspectList[] = data[dataIndex][dataColumnIndex].split(",");
								solrInputDocument.setField(columnName, aspectList);
								break;
							default:
								solrInputDocument.setField(columnName, data[dataIndex][dataColumnIndex]);
							}
						}
					}
				}
				// solrInputDocument.setField("Source", "CHECK24");
				solrClient.add(solrInputDocument);
				documentCounter++;
				if (documentCounter >= 1000 || dataIndex == dataLength - 1) {
					solrClient.commit();
					documentCounter = 0;
				}

			}

		} catch (Exception e) {

			e.printStackTrace();
		}

	}

	public static void main(String args[]) throws Exception {
		ScrappingServiceImpl scrappingServiceImpl = new ScrappingServiceImpl();
		// scrappingServiceImpl.mergeDataWhenUsersAreSameSeparated();
		scrappingServiceImpl.loadSolrData();
	}
}
