//package com.insaas.insaasApp.serviceImpl;
//
//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;
//import org.apache.solr.client.solrj.SolrClient;
//import org.apache.solr.client.solrj.impl.HttpSolrClient;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//import com.insaas.insaasApp.common.Constant;
//import com.insaas.insaasApp.service.RunPythonScriptService;
//import com.insaas.insaasApp.service.SolrService;
//
//@Service
//public class RunPythonScriptServiceImpl implements RunPythonScriptService {
//	
//	@Autowired
//	SolrService solrService;
//	
//	private static final Logger logger = LogManager.getLogger(RunPythonScriptServiceImpl.class.getName());
//	
//	@Override
//	public String runPythonScript(String filePath) throws Exception {
//		Process p =null;
//		try {
//			logger.info("path of file to be anyalysis...."+filePath);
//	String urlString=Constant.URL_OF_SOLR; 
//	String csvFilePath=Constant.CSV_FILE_PATH;
//	String path=Constant.WORKING_DIR+Constant.PYTHON_FILE_PATH;
//	logger.info("path="+path);
//	  SolrClient Solr = new HttpSolrClient.Builder(urlString).build();
//	  //logger.info("Solr="+Solr.ping());
//	String command =Constant.PYTHON_COMMAND+path+" "+filePath;
//	String coreName="vaishaliTestCore";
//	logger.info("command="+command);
//	logger.info("urlString="+urlString);
//	
//	 p = Runtime.getRuntime().exec(command);
//	logger.info("process is running in wait state...");
//	p.waitFor();
//	return "run Successfully...";
//	
//		}
//		catch(Exception e) {
//			System.out.println("exception="+e);
//		}
//		return Integer.toString(p.exitValue());
//	}
//	 
//}
