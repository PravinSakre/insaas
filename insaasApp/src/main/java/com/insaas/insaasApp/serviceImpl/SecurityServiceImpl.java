package com.insaas.insaasApp.serviceImpl;

import java.io.FileReader;
import java.io.FileWriter;
import java.nio.file.attribute.UserPrincipalNotFoundException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

//import java.util.Base64;
import org.apache.commons.codec.binary.Base64;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.annotation.Resource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.response.FacetField;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.insaas.insaasApp.bean.SecurityInformationBean;
import com.insaas.insaasApp.common.Constant;
import com.insaas.insaasApp.common.MessagesConstant;
import com.insaas.insaasApp.config.PropertyFileConfig;
import com.insaas.insaasApp.dto.CustomerDto;
//import com.insaas.insaasApp.dto.QueryParamsDto;
import com.insaas.insaasApp.dto.UserDto;
import com.insaas.insaasApp.entity.AccessItemMaster;
import com.insaas.insaasApp.entity.ActivityTracking;
import com.insaas.insaasApp.entity.Customer;
import com.insaas.insaasApp.entity.CustomerDetails;
import com.insaas.insaasApp.entity.Role;
import com.insaas.insaasApp.entity.RoleAccess;
import com.insaas.insaasApp.entity.User;
import com.insaas.insaasApp.loginConfig.TokenUtil;
import com.insaas.insaasApp.pojo.SecurityContext;
import com.insaas.insaasApp.repository.AccessItemRepository;
import com.insaas.insaasApp.repository.ActivityTrackingRepository;
import com.insaas.insaasApp.repository.CustomerDetailsRepository;
import com.insaas.insaasApp.repository.CustomerRepository;
import com.insaas.insaasApp.repository.RoleAccessRepository;
import com.insaas.insaasApp.repository.RoleRepository;
import com.insaas.insaasApp.repository.UserRepository;
import com.insaas.insaasApp.service.ConfigurationService;
import com.insaas.insaasApp.service.MessageService;
import com.insaas.insaasApp.service.SecurityService;
import com.insaas.insaasApp.service.SolrService;

/**
 * This class will implements and defined all methods of SecurityService. it
 * contains business logic.
 * 
 * @author BrainWave Consulting
 * @since Dec 18, 2018
 */
@Configuration
@EnableScheduling
@Service
public class SecurityServiceImpl implements SecurityService {
	@Autowired
	private ConfigurationService configurationService;
	/** store login user details */
	private Map<String, String> loginUserDetailsMap = new HashMap<>();

	/** Logger for this class. */
	private static final Logger logger = LogManager.getLogger(SecurityServiceImpl.class.getName());
	@Autowired
	private MessageService messageService;
	@Autowired
	private ActivityTrackingRepository activityTrackingRepository;
	@Autowired
	private UserDetailsService customUserDetailsService;

	@Autowired
	private SolrService solrService;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private RoleRepository roleRepository;
	@Autowired
	private RoleAccessRepository roleAccessRepository;
	@Autowired
	private AccessItemRepository accessItemRepository;
	@Autowired
	private PropertyFileConfig propertyFileConfig;
	@Autowired
	private CustomerRepository customerRepository;
	@Autowired
	private CustomerDetailsRepository customerDetailsRepository;

	@Resource(name = "securityBean")
	private SecurityInformationBean securityInforBean;

	/**
	 * This method will execute authentication business logic. it will validate
	 * userName and password. and generate accessToken
	 * 
	 * @author BrainWave Consulting
	 * @since Dec 19, 2018
	 * @param userName
	 * @param password
	 * @return json String, which have Activity tracking object
	 */
	@Override
	public String authenticationUsingUserData(String userName, String password) throws Exception {
		byte[] byteArrayUserName = Base64.decodeBase64(userName.getBytes());
		byte[] byteArrayPassword = Base64.decodeBase64(password.getBytes());
		String decodedStringUsername = new String(byteArrayUserName);
		userName = decodedStringUsername;
		String decodedStringPassword = new String(byteArrayPassword);
		password = decodedStringPassword;
		if (userName == null || password == null) {
			logger.error("User name and Password cannot be null");
			throw new BadCredentialsException("User name and Password not found..");
		}
		List<Map<String, String>> accessMenuNames = new ArrayList<>();
		Date timeStamp = new java.util.Date();
		Map responseMap = new HashMap<>();
		ActivityTracking activityTracking = new ActivityTracking();
		// generate and maintain user security object for the application

		String accessToken = "";
		try {
			List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
			UserDetails userDetails = customUserDetailsService.loadUserByUsername(userName);
			if (userDetails == null) {
				logger.error("userDetails not found");
				throw new UserPrincipalNotFoundException("userDetails not found");
			}
			Collection<? extends GrantedAuthority> roles = userDetails.getAuthorities();
			if (roles == null) {
				logger.error("serDetails.getAuthorities() not found");
				throw new AuthenticationCredentialsNotFoundException("serDetails.getAuthorities() not found");
			}
			authorities.add(new SimpleGrantedAuthority(String.format("ROLE_%s", roles)));

			String pass = userDetails.getPassword().trim();
			String EncryPassword = Base64.encodeBase64String((password).getBytes());
			if (pass.equals(EncryPassword)) {
				if (logger.isDebugEnabled()) {
					logger.debug("authenticationUsingUserData(String, String) - {}", //$NON-NLS-1$
							"userDetails.getUsername()=" + userDetails.getUsername() //$NON-NLS-1$
									+ "TokenUtil.createToken(userDetails)=" + accessToken //$NON-NLS-1$
									+ "HttpStatus.OK=" + HttpStatus.OK); //$NON-NLS-1$
				}
				User user = userRepository.findByUserId(userName);
				if (user == null) {
					logger.error("user not found");
					throw new UserPrincipalNotFoundException("user not found");
				}
				int roleId = user.getUserRoleId();
				if (roleId <= 0) {
					logger.error("roleId not found");
					throw new AuthenticationCredentialsNotFoundException("roleId not found");
				}
				Role role = roleRepository.findByRoleId(roleId);
				if (role == null) {
					logger.error("role not found");
					throw new AuthenticationCredentialsNotFoundException("role not found");
				}

				accessToken = TokenUtil.createToken(userDetails);
				accessToken = accessToken.split(":")[1].trim();
				
				SecurityContext userSecurityObject = new SecurityContext("");
				userSecurityObject.setUserId(user.getUserId());
				userSecurityObject.setUserAutoGenId((user.getIdUserAutoGen()));
				userSecurityObject.setLoggedInTime(timeStamp);
				userSecurityObject.setLastAccessedTime(timeStamp);
				userSecurityObject.setRoleID(user.getUserRoleId());

				securityInforBean.addNewSecuirtyContext(accessToken, userSecurityObject);
				responseMap.put("userName", user.getUserId());
				responseMap.put("userId", user.getIdUserAutoGen());
				responseMap.put("defaultScreen", role.getScreenUrl());
				responseMap.put("accessToken", accessToken);
				responseMap.put("screenURL", role.getScreenUrl());
				responseMap.put("userRole", role.getUserRole());
				responseMap.put("status", HttpStatus.OK.toString());
				/* getting user login accesstoken */
				
				loginUserDetailsMap.put(accessToken, userDetails.getUsername());
				activityTracking.setUserId(userName);
				activityTracking.setAccessToken(accessToken);
				activityTracking.setLoggedInTime(timeStamp);

				activityTrackingRepository.save(activityTracking);
				accessMenuNames = getMenusByRoleId(roleId);
				responseMap.put("accessMenuNames", accessMenuNames);

				try {
					Customer customer = customerRepository.findByCustomerId(user.getOrgId());
					if (null == customer) {
						logger.error("customer record not found for org/customer Id :: " + user.getOrgId());
					}
					responseMap.put("core_name", customer.getCoreName());
					responseMap.put("analysisStatement", customer.getAnalysisStatement());
					List<Map<String, List<Integer>>> dataViewOrgIds = new ArrayList<>();

					CustomerDetails customerDetails = customerDetailsRepository.findByCustomerId(user.getOrgId());
					if (null == customerDetails) {
						logger.error("customerDetails records not found for org/customer Id :: " + user.getOrgId());
					}

//					responseMap.put("sourceOfInfo", customerDetails.getSourceOfInfo());
//					responseMap.put("dateFrom", getSolrResponse(customer.getCoreName(), "asc").split(":")[0]);
//					responseMap.put("dateTo", getSolrResponse(customer.getCoreName(), "desc").split(":")[0]);
//					responseMap.put("totalReviews", getSolrResponse(customer.getCoreName(), "desc").split(":")[1]);
//					responseMap.put("sourceOfInfo", getSolrResponse(customer.getCoreName(), "asc").split(":")[2]);
					logger.info("responseMap New :: " + responseMap.toString());
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else {
				responseMap.put("status", HttpStatus.NOT_ACCEPTABLE.toString());
				throw new BadCredentialsException("Password is incorrect......");
			}

		} catch (BadCredentialsException bce) {
			logger.error("Exception while authentication Using UserData" + bce);
			bce.printStackTrace();
			responseMap.put("status", HttpStatus.BAD_REQUEST.toString());
			throw new BadCredentialsException(bce.getMessage());
		} catch (Exception e) {
			logger.error("Exception while authentication Using UserData" + e);
			e.printStackTrace();
			responseMap.put("status", HttpStatus.BAD_REQUEST.toString());
			throw new BadCredentialsException(e.getMessage());
		}
		return getJsonOfObject(responseMap); // new ResponseEntity<UserTransfer>(HttpStatus.BAD_REQUEST);
	}

	private String getSolrResponse(String coreName, String sortOrder) throws Exception {
		return sortOrder;
//		System.out.println("coreName :: " + coreName);
//		QueryParamsDto queryParamsDto = new QueryParamsDto();
//		String facetFields[] = { "Source" };
//		String facetPivotFields[] = {};
//		String filterQueryParams[] = {};
//		String fieldList[] = { "date" };
//		queryParamsDto.setqParam("q");
//		queryParamsDto.setqValue("*:*");
//		queryParamsDto.setFacetFields(facetFields);
//		queryParamsDto.setFacetPivotFields(facetPivotFields);
//		queryParamsDto.setFilterQueryParams(filterQueryParams);
//		queryParamsDto.setFlafDistribute(true);
//		queryParamsDto.setFlagFacet(true);
//		queryParamsDto.setFlagHightlight(true);
//		queryParamsDto.setSimpleFields(fieldList);
//		queryParamsDto.setTermsField("");
//		queryParamsDto.setTimeAllowed(898);
//
//		SolrQuery solrQuery = new SolrQuery();
//		if (queryParamsDto.getqValue() == null || queryParamsDto.getqValue().isEmpty()) {
//			solrQuery.add(queryParamsDto.getqParam(), "*:*");
//		} else {
//			solrQuery.add(queryParamsDto.getqParam(), queryParamsDto.getqValue());
//			// logger.info("Set query to q=" + queryParamsDto.getqValue());
//		}
//		if ((queryParamsDto.getFacetFields() != null)) {
//			solrQuery.addFacetField(queryParamsDto.getFacetFields());
//		}
//		if ((queryParamsDto.getSimpleFields() != null)) {
//			solrQuery.setFields(queryParamsDto.getSimpleFields());
//		}
//		if ((queryParamsDto.getFacetPivotFields() != null)) {
//			// logger.info("Facet pivot fields are not empty...");
//			solrQuery.addFacetPivotField(queryParamsDto.getFacetPivotFields());
//		}
//		if ((queryParamsDto.getFacetQuery() != null)) {
//			// logger.info("Facet query is not null...=" + queryParamsDto.getFacetQuery());
//			solrQuery.addFacetQuery(queryParamsDto.getFacetQuery());
//		}
//		solrQuery.setTimeAllowed(queryParamsDto.getTimeAllowed());
//		// logger.info("facet...");
//		solrQuery.setHighlight(true);
//		// logger.info("highlight");
//		solrQuery.addHighlightField("Sentiment");
//		// logger.info("hight light field sentiment");
//		if ("asc".equals(sortOrder)) {
//			solrQuery.addSort("date", SolrQuery.ORDER.asc);
//		} else {
//			solrQuery.addSort("date", SolrQuery.ORDER.desc);
//		}
//
//		// logger.info("solrQuery=" + solrQuery);
//
//		// String url = "http://localhost:8983/solr/";
//
//		String solrUrl = propertyFileConfig.getSolrServerUrl() + "/" + coreName;
//		 logger.info("solrUrl :: " + solrUrl);
//		SolrClient client = solrService.connectToSolrServer(solrUrl);
//		// logger.info("client=" + client.ping());
//		// System.out.println("client=" + client.ping());
//		QueryResponse response = client.query(solrQuery);
//		SolrDocumentList results = response.getResults();
//		logger.info("full records :: " + results.getNumFound());
//		logger.info("results=" + results.get(0));
//		SolrDocument SolrDocument = results.get(0);
//		// logger.info("SolrDocument :: " + SolrDocument.get("date"));
//		//E MMM dd HH:mm:ss z uuuu
//		DateTimeFormatter f = DateTimeFormatter.ofPattern("E MMM dd HH:mm:ss z uuuu").withLocale(Locale.US);
//		logger.info("f="+f);
//		logger.info("SolrDocument.get(\"date\").toString()="+SolrDocument.get("date").toString());
//		ZonedDateTime zdt = ZonedDateTime.parse(SolrDocument.get("date").toString(), f);
//
//		LocalDate localDate = zdt.toLocalDate();
//		DateTimeFormatter fLocalDate = DateTimeFormatter.ofPattern("dd/MM/uuuu");
//		String date = localDate.format(fLocalDate);
//		
//		// Below three line for getting Source list from solr
//		List<FacetField> facet = response.getFacetFields();
//		String source = facet.get(0).getValues().toString().replaceAll("[()]", "").replaceAll("\\[|\\]", "").replaceAll("\\d","").replaceAll(" , ", ", ");// First remove Brackets then Square brackets and then number.
//		logger.info("Source List :: " + source);
//		
//		
//		date += ":" + String.valueOf(results.getNumFound())+":"+source;
//		return date;
//		// logger.info("output :: " + output);
	}

	/**
	 * This method will return String form of object.
	 * 
	 * @author BrainWave Consulting
	 * @since Dec 18, 2018
	 * @param Obj
	 * @return json String of object
	 */
	private String getJsonOfObject(Object Obj) {
		logger.debug("getJsonOfObject:Obj=" + Obj);
		ObjectMapper objectMapper = new ObjectMapper();
		String json = "";
		try {
			json = objectMapper.writeValueAsString(Obj);
		} catch (Exception e) {
			logger.error("Excption while getJsonOfObject" + e);
			e.printStackTrace();
			if (logger.isDebugEnabled()) {
				logger.debug("getJsonOfObject(Object) - {}", "Exception in getJsonOfObject:: " + e); //$NON-NLS-1$ //$NON-NLS-2$
			}
		}
		return json;
	}

	/**
	 * This method will used for getting an encryption form of any text string it
	 * used for encrypt password
	 * 
	 * @author BrainWave Consulting
	 * @since Dec 19, 2018
	 * @param userName
	 * @param password
	 * @return json String, which have Activity tracking object
	 * @throws Exception
	 */
	public String getEncryption(String textToEncrypt) throws Exception {
		String encryptedString = "";
		logger.debug("getEncription:textToEncrypt=" + textToEncrypt);
		if (textToEncrypt != null || !textToEncrypt.isEmpty()) {
			encryptedString = Base64.encodeBase64String(textToEncrypt.getBytes());
			return encryptedString;
		}
		throw new Exception();
	}

	@Override
	public String addForgotPassword(String accessToken, String password) throws Exception {
		try {
			logger.info("accessToken=" + accessToken);
			String encryptedPassword = getEncryption(password);
			SecurityContext userSecurityContext = securityInforBean.getSecuirtyContext(accessToken.split(":")[1]);
			String userId = userSecurityContext.getUserId();
			User user = userRepository.findByUserId(userId);
			if (user == null) {
				throw new RuntimeException("Email id is incorrect.Cannot find user with this email");
			}
			user.setPassword(encryptedPassword);
			userRepository.save(user);
		} catch (Exception e) {
			logger.error("Excption while changing password", e);
			e.printStackTrace();
			return "Not able to change password," + e.getMessage();
		}
		return "Password changed successfully";
	}

	@Override
	public Map<String, String> forgotPassword(String email) {
		logger.debug("forgotPassowrd:email=" + email);
		Map<String, String> responseMap = new HashMap<>();
		String accessToken = null;

		try {
			logger.info("propertyFileConfig.getXmlFilePath()=" + propertyFileConfig.getXmlFilePath());
			configurationService.prepareConfiguration(propertyFileConfig.getXmlFilePath());

			String messagingConfiguration = configurationService.getServerConfiguration("email");
			String mailFrom = propertyFileConfig.getMailFrom();
			String mailToAdmin = propertyFileConfig.getMailToAdmin();
			String subjectToAdmin = propertyFileConfig.getMailToAdminSubject();
			String messageToAdmin = email + "\t" + propertyFileConfig.getMailToAdminMessage();
			String configuration = messagingConfiguration;
			messageService.sendMail(configuration, mailFrom, mailToAdmin, subjectToAdmin, messageToAdmin);

			String mailToUser = propertyFileConfig.getMailToUser();
			String subjectToUser = propertyFileConfig.getMailToUserSubject();
			String messageToUser = propertyFileConfig.getMailToUserMessage();

			User user = new User();

			user = userRepository.findByEmailId(email);
			if (user == null) {
				logger.error("user not found for given emailId");
				throw new javax.security.sasl.AuthenticationException("user not found for given emailId");
			}
			String userId = user.getUserId();
			if (userId == null || userId.isEmpty()) {
				logger.error(MessagesConstant.USER_NOT_FOUND);
				throw new javax.security.sasl.AuthenticationException(MessagesConstant.USER_NOT_FOUND);
			}
			List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
			authorities.add(new SimpleGrantedAuthority(String.format("ROLE_%s", "admin")));
			accessToken = TokenUtil.createToken(
					new org.springframework.security.core.userdetails.User(userId, "fkdsu09!$", authorities));
			logger.info("accessToken=" + accessToken);
			responseMap.put("accessToken", accessToken);
			responseMap.put("Result", MessagesConstant.MSG_PASS_ADD);
			messageToUser = messageToUser + "/" + accessToken;
			messageService.sendMail(configuration, mailFrom, mailToUser, subjectToUser, messageToUser);
			SecurityContext userSecurityObject = new SecurityContext("");
			Date timeStamp = new java.util.Date();
			userSecurityObject.setUserId(user.getUserId());

			userSecurityObject.setLoggedInTime(timeStamp);
			userSecurityObject.setLastAccessedTime(timeStamp);
			securityInforBean.addNewSecuirtyContext((accessToken.split(":")[1]).trim(), userSecurityObject);
		} catch (Exception e) {
			responseMap.put("Result", MessagesConstant.MSG_EMAIL_NOT_EXIST);
			logger.error("Exception in forgotPassowrd method" + e);
		}
		return responseMap;
	}

	@Override
	public Map<String, String> changePassword(String accessToken, String password) throws Exception {

		logger.info("changePassword:accessToken=" + accessToken + "\t password=" + password);
		SecurityContext userSecurityContext = securityInforBean.getSecuirtyContext(accessToken);
		logger.info("userSecurityContext.getUserId()=" + userSecurityContext.getUserId());

		Map<String, String> response = new HashMap<>();
		try {
			if (userSecurityContext == null) {
				logger.error("Tracking Details not available..for accessToken :: " + accessToken);
				throw new javax.security.sasl.AuthenticationException("AccessToken invalid......");
			}
			String userId = userSecurityContext.getUserId();
			if (userId == null || userId.isEmpty()) {
				logger.error("userId not found ");
				throw new javax.security.sasl.AuthenticationException("userId not found ");
			}
			User user = new User();
			user = userRepository.findByUserId(userId);
			logger.info("user===" + user.getEmailId());
			if (user == null) {
				logger.error("user not found for userId=" + userId);
				throw new javax.security.sasl.AuthenticationException("user not found for userId=" + userId);
			}
			String encryptedPassword = Base64.encodeBase64String(password.getBytes());
			logger.info("encryptedPassword=" + encryptedPassword);
			if (encryptedPassword == null || encryptedPassword.isEmpty()) {
				logger.error("encryptedPassword not available ");
				throw new javax.security.sasl.AuthenticationException("encryptedPassword not available  ");
			}
			user.setPassword(encryptedPassword);
			userRepository.save(user);
			logger.info("after changing password..into database");
			// logoutPage(accessToken);
			response.put(MessagesConstant.RESULT_STRING, MessagesConstant.MSG_EXC_PASS_CHANGED);
		} catch (Exception e) {
			logger.error("Exception while changing password==" + e);
			e.printStackTrace();
		}
		return response;
	}

	private Boolean logoutPage(String accessToken) throws Exception {
		logger.debug("logoutPage:accessToken=" + accessToken);
		try {
			if (accessToken == null) {
				logger.error("AccessToken Invalid...");
				throw new javax.security.sasl.AuthenticationException("AccessToken invalid......");
			}

			logger.debug("accessToken=" + accessToken);
			Date timeStamp = new java.util.Date();
			ActivityTracking activityTracking = new ActivityTracking();
			activityTracking = activityTrackingRepository.findByAccessToken(accessToken);
			securityInforBean.removeSecurityContext(accessToken);
			activityTracking.setLoggedOutTime(timeStamp);
			logger.debug("activityTracking.gettime==" + timeStamp);
			activityTrackingRepository.save(activityTracking);
			loginUserDetailsMap.remove(accessToken);

		} catch (Exception e) {
			logger.error("Exception in logoutPage" + e);
			e.printStackTrace();
		} finally {
			loginUserDetailsMap.remove(accessToken);
		}
		logger.info("successfully logout");
		return true;

	}

	private List<Map<String, String>> getMenusByRoleId(int roleId) throws Exception {
		logger.info("getMenusByRoleId:roleId=" + roleId);
		List<Map<String, String>> screenDisplayMenusList = new ArrayList<>();
		try {
			Role role = roleRepository.findByRoleId(roleId);
			logger.info("role=" + role.getScreenUrl());
			if (role == null) {
				logger.error(MessagesConstant.ROLE_NOT_FOUND + roleId);
				throw new AuthenticationCredentialsNotFoundException(MessagesConstant.ROLE_NOT_FOUND + roleId);
			}
			List<RoleAccess> roleAccessList = roleAccessRepository.findRoleAccessByRoleId(roleId);
			if (roleAccessList == null) {
				logger.error(MessagesConstant.ROLE_ACCESS_LIST_NOT_FOUND + roleId);
				throw new AuthenticationCredentialsNotFoundException(
						MessagesConstant.ROLE_ACCESS_LIST_NOT_FOUND + roleId);
			}
			for (RoleAccess roleAccess : roleAccessList) {
				int accessItemId = roleAccess.getAccessItemId();
				if (accessItemId <= 0) {
					logger.error(MessagesConstant.ACCESS_ITEM_ID_NOT_FOUND);
					throw new AuthenticationCredentialsNotFoundException(MessagesConstant.ACCESS_ITEM_ID_NOT_FOUND);
				}
				// logger.info("MessagesConstant.ACCESS_ITEM_TYPE_MENU :: " +
				// MessagesConstant.ACCESS_ITEM_TYPE_MENU);
				AccessItemMaster accessItemMaster = accessItemRepository.findByAccessItemId(accessItemId);
				if (accessItemMaster == null) {
					logger.error(MessagesConstant.ACCESS_ITEM_MASTER_NOT_FOUND + accessItemId);
					throw new AuthenticationCredentialsNotFoundException(
							MessagesConstant.ACCESS_ITEM_MASTER_NOT_FOUND + accessItemId);
				}
				if (!MessagesConstant.ACCESS_ITEM_TYPE_MENU.equals(accessItemMaster.getAccessItemType()))
					continue;

				String screenDisplayMenus = accessItemMaster.getScreenDisplayMenus();
				logger.info("screenDisplayMenus=" + screenDisplayMenus);
				if (screenDisplayMenus == null) {
					logger.error("screenDisplayMenus are not available");
					throw new RuntimeException("screenDisplayMenus are not available");
				}
				if (((!screenDisplayMenus.contains("add") && !screenDisplayMenus.contains("update"))
						&& !screenDisplayMenus.contains("edit") && !screenDisplayMenus.contains("create"))) {
					Map<String, String> screenDisplayMenusMap = new HashMap<>();
					screenDisplayMenusMap.put("screenUrls", accessItemMaster.getScreenUrl());
					screenDisplayMenusMap.put("screenDisplayMenus", screenDisplayMenus);

					screenDisplayMenusList.add(screenDisplayMenusMap);
					logger.info("screenDisplayMenusList=" + screenDisplayMenusList);
				}
			}
			logger.info("screenDisplayMenusList=" + screenDisplayMenusList);
			return screenDisplayMenusList;

		} catch (Exception e) {
			logger.error("Exception while getMenusByRoleId" + e);
			throw new RuntimeException(e.getMessage());
		}

	}

	@Override
	public void updateSearchJsonWithCustomerCoreName(String coreName, String dashboardType) throws Exception {
		logger.info("coreName :: " + coreName);
		String jsonFilePath = propertyFileConfig.getInsaasDashboardFilePath();
		switch (dashboardType) {
		case Constant.DEFAULT_DASHBOARD:
			jsonFilePath += Constant.DEFAULT_DASHBOARD_FILE_NAME;
			break;
		case Constant.SEARCH_DASHBOARD:
			jsonFilePath += Constant.SEARCH_DASHBOARD_FILE_NAME;
			break;

		}
		logger.info("jsonFilePath=" + jsonFilePath);
		FileReader reader = new FileReader(jsonFilePath);
		JSONParser jsonParser = new JSONParser();
		Object object = jsonParser.parse(reader);
		JSONObject jsonObject = (JSONObject) object;
		Map<Object, Object> obj = (Map<Object, Object>) jsonObject.get("solr");
		obj.put("core_name", coreName);
		logger.info("solr detals :: " + obj.toString());
		jsonObject.put("solr", obj);
		logger.info("jsonObject :: " + jsonObject.toJSONString());

		FileWriter writer = new FileWriter(jsonFilePath);
		writer.write(jsonObject.toJSONString());
		writer.flush();
	}

	@Override
	public String addUser(UserDto userDto) throws Exception {
		// TODO Auto-generated method stub

		try {
			String accessToken = userDto.getAccessToken();
			logger.info("create new user", accessToken);

			logger.info("create new user");
			User user = new User();
			Integer roleId = userDto.getRoleId();
			logger.info("role=" + roleId);
//				String roleName=userDto.getUserRole();
//				Role role = roleRepository.findRoleByuserRole(roleName);
//				logger.info("role="+role);
//				Integer r=role.getRoleId();
//				logger.info("role="+r);
			user.setUserRoleId(roleId);
			Integer orgId = userDto.getOrgId();
			user.setOrgId(orgId);
//				String custName=userDto.getCustomerName();
//				Customer customer=customerRepository.findByCustomerName(custName);
//				Integer custId=customer.getCustomerId();
			// user.setOrgId(custId);
			// isNewUser(userDto.getUserFname());
			user.setUserFname(userDto.getUserFname());
			user.setUserLname(userDto.getUserLname());
			user.setEmailId(userDto.getEmailId());
			user.setMobNo(userDto.getMobNo());
			logger.info("adding");
			logger.info("msg after userRepository");
			user.setActive(userDto.getActive());
			user.setCreatedBy(userDto.getCreatedBy());
			user.setCreatedOn(userDto.getCreatedOn());
			user.setIdUserAutoGen(userDto.getIdUserAutoGen());
			String pass = userDto.getPassword();
			String encryptedPassword = Base64.encodeBase64String(pass.getBytes());
			logger.info("vfdsv", encryptedPassword);
			user.setPassword(encryptedPassword);
			// String EncryPassword = Base64.encodeBase64String((pass).getBytes());
			user.setPasswordValidity(userDto.getPasswordValidity());
			user.setUpdatedBy(userDto.getUpdatedBy());
			user.setUpdatedOn(userDto.getUpdatedOn());
			user.setUserId(userDto.getUserId());
			// user.setUserRoleId(userDto.getUserRoleId());
			user.setActive(true);
			userRepository.save(user);

			return "Added successfully.....";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("Eception while adding new user", e);
			return "Error while adding new  user";
		}

	}
	/*
	 * private void isNewUser(String userna) throws Exception {
	 * logger.info("in newuser method"); User
	 * user=userRepository.findByUserFnameAndActive(userna,true);
	 * logger.info("user1="+user.toString()); if (user != null) {
	 * logger.info("user name already exist.."); throw new
	 * Exception("Duplicate entry"); }
	 */

	@Override
	public List<UserDto> getUserData() throws Exception {
		// TODO Auto-generated method stub
		// List<User> userlist=userRepository.findAllByActive(true);
		List<UserDto> userDtoList = new ArrayList();
		List<User> userlist = userRepository.findAllByActive(true);
		/*
		 * List<String> userRoleList=roleRepository.findAllByuserRole();
		 * logger.info("userRoleList="+userRoleList);
		 */

		for (User user : userlist) {
			UserDto userDto = new UserDto();
			int orgId = user.getOrgId();
			int userRoleId = user.getUserRoleId();
			Role role = new Role();
			role = roleRepository.findByRoleId(userRoleId);
			logger.info("role=" + role);
			String Userrole = role.getUserRole();
			logger.info("Userrole=" + Userrole);
			Customer customer = customerRepository.findByCustomerId(orgId);
			logger.info("customer=" + customer);
			String custName = customer.getCustomerName();
			logger.info("custName=" + custName);
			userDto.setUserFname(user.getUserFname());
			userDto.setUserLname(user.getUserLname());
			userDto.setUserRole(Userrole);
			userDto.setCustomerName(custName);
			userDto.setMobNo(user.getMobNo());
			userDto.setEmailId(user.getEmailId());
			userDto.setPassword(user.getPassword());
			userDto.setIdUserAutoGen(user.getIdUserAutoGen());
			userDto.setUserId(user.getUserId());
			userDto.setUserRoleId(user.getUserRoleId());
			// userDto.setUserRole(user.get);

			userDtoList.add(userDto);

		}
		logger.info("User data=" + userDtoList.toString());

		return userDtoList;
	}

	@Override
	public Integer deleteUserData(int idUserAutoGen) throws Exception {
		// TODO Auto-generated method stub
		User user = new User();
		user = userRepository.findByIdUserAutoGen(idUserAutoGen);
		logger.info("user=" + user.toString());
		user.setActive(false);
		User u = userRepository.save(user);
		return 1;
	}

	@Override
	public UserDto getSingleUser(int idUserAutoGen) throws Exception {

		/*
		 * List<UserDto> userDtoList=new ArrayList(); List<User>
		 * userlist=userRepository.findAllByActive(true); for(User user:userlist) {
		 * UserDto userDto=new UserDto(); int orgId=user.getOrgId(); int
		 * userRoleId=user.getUserRoleId(); Role role=new Role();
		 * role=roleRepository.findByRoleId(userRoleId); logger.info("role="+role);
		 * String Userrole=role.getUserRole(); logger.info("Userrole="+Userrole);
		 * 
		 * Customer customer =customerRepository.findByCustomerId(orgId);
		 * logger.info("customer="+customer); String
		 * custName=customer.getCustomerName(); logger.info("custName="+custName);
		 * userDto.setUserRole(Userrole); userDto.setCustomerName(custName);
		 * userDto.setMobNo(user.getMobNo()); userDto.setEmailId(user.getEmailId());
		 * userDto.setPassword(user.getPassword());
		 * userDto.setIdUserAutoGen(user.getIdUserAutoGen());
		 * userDto.setUserId(user.getUserId()); //userDto.setUserRole(user.get);
		 * 
		 * userDtoList.add(userDto);
		 * 
		 * } logger.info("User data="+userDtoList.toString());
		 * 
		 * return userDtoList;
		 */

		// TODO Auto-generated method stub
		List<UserDto> userDtoList = new ArrayList();
		User user = new User();
		user = userRepository.findByIdUserAutoGen(idUserAutoGen);
		UserDto userDto = new UserDto();
		int orgId = user.getOrgId();
		int userRoleId = user.getUserRoleId();
		Role role = new Role();
		role = roleRepository.findByRoleId(userRoleId);
		logger.info("role=" + role);
		String Userrole = role.getUserRole();
		logger.info("Userrole=" + Userrole);

		Customer customer = customerRepository.findByCustomerId(orgId);
		logger.info("customer=" + customer);
		String custName = customer.getCustomerName();
		logger.info("custName=" + custName);
		logger.info("user=" + user.toString());
		userDto.setUserRole(Userrole);
		userDto.setCustomerName(custName);
		userDto.setUserRole(Userrole);
		userDto.setCustomerName(custName);
		userDto.setMobNo(user.getMobNo());
		userDto.setUserFname(user.getUserFname());
		userDto.setUserLname(user.getUserLname());
		userDto.setEmailId(user.getEmailId());
		userDto.setPassword(user.getPassword());
		userDto.setIdUserAutoGen(user.getIdUserAutoGen());
		userDto.setUserId(user.getUserId());
		userDtoList.add(userDto);
		return userDto;
	}

	@Override
	public String updateUser(UserDto userDto) throws Exception {
		// TODO Auto-generated method stub
		try {

			logger.info("update new user");
			User user = new User();

			String roleName = userDto.getUserRole();
			Role role = roleRepository.findRoleByuserRole(roleName);
			logger.info("role=" + role);
			Integer r = role.getRoleId();
			logger.info("role=" + r);
			user.setUserRoleId(r);
			String custName = userDto.getCustomerName();
			Customer customer = customerRepository.findByCustomerName(custName);
			Integer custId = customer.getCustomerId();
			user.setOrgId(custId);
			// isNewUser(userDto.getUserFname());
			user.setUserFname(userDto.getUserFname());
			user.setUserLname(userDto.getUserLname());

			user.setEmailId(userDto.getEmailId());
			user.setMobNo(userDto.getMobNo());
			user.setActive(userDto.getActive());
			user.setCreatedBy(userDto.getCreatedBy());
			user.setCreatedOn(userDto.getCreatedOn());
			user.setIdUserAutoGen(userDto.getIdUserAutoGen());
			user.setPassword(userDto.getPassword());
//			String pass=userDto.getPassword();
//			String encryptedPassword = Base64.encodeBase64String(pass.getBytes());
//			logger.info("vfdsv",encryptedPassword);
//			user.setPassword(encryptedPassword);
			user.setPasswordValidity(userDto.getPasswordValidity());
			user.setUpdatedBy(userDto.getUpdatedBy());
			user.setUpdatedOn(userDto.getUpdatedOn());
			user.setUserId(userDto.getUserId());
			// user.setUserRoleId(userDto.getUserRoleId());
			user.setActive(true);
			userRepository.save(user);

			return "Added successfully.....";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("Eception while adding new user", e);
			return "Error while adding new  user";
		}

	}

	@Override
	public List<Role> getRoleData() throws Exception {
		// TODO Auto-generated method stub
		List<Role> roleList = roleRepository.findAll();
		logger.info("roleList=" + roleList);
		return roleList;
	}

	@Override
	public boolean validateAccessToken(String accessToken) throws Exception {
		SecurityContext userSecurityObject = securityInforBean.getSecuirtyContext((accessToken).trim());
		logger.debug("validateAccessToken:accessToken=" + accessToken);
		if (accessToken == null) {
			logger.error("AccessToken Invalid...");
			throw new Exception("AccessToken invalid......");
		}
		Boolean result = true;
		try {
			logger.info("in validate AccessToken method......");
			// ActivityTracking activityTracking =
			// activityTrackingRepository.findByAccessToken(accessToken);
			if (userSecurityObject == null) {
				logger.error("Tracking Details not available..for accessToken :: " + accessToken);
				throw new javax.security.sasl.AuthenticationException("AccessToken invalid......");
			}
			Date lastAccessedTime = userSecurityObject.getLastAccessedTime();
			if (lastAccessedTime == null) {
				logger.error("loggedInTime not available :: ");
				throw new javax.security.sasl.AuthenticationException("loggedInTime not available :: ");
			}
			Date currentDate = new java.util.Date();
			int tokenValidity = propertyFileConfig.getAccessTokenValidity();
			if (tokenValidity <= 0) {
				logger.error("tokenValidity not found in properties file  ");
				throw new javax.security.sasl.AuthenticationException("tokenValidity not found in properties file");
			}
			long loginDuration = currentDate.getTime() - lastAccessedTime.getTime();
			long durationInMinutes = TimeUnit.MILLISECONDS.toMinutes(loginDuration);
			if (durationInMinutes <= tokenValidity) {
				userSecurityObject.setLastAccessedTime(currentDate);
				return true;
			} else {
				result = logoutPage(accessToken);
				return result;
			}
		} catch (Exception e) {
			logger.error("Exception while logout is " + e);
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public Object getUserId(String accessToken) throws Exception {
		validateAccessToken(accessToken);
		SecurityContext userSecurityContext = securityInforBean.getSecuirtyContext(accessToken);
		return userSecurityContext .getUserId();	}

//	@Override
//	public String addUser(UserDto userDto) throws Exception {
//		// TODO Auto-generated method stub
//		return null;
//	}

}