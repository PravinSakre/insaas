package com.insaas.insaasApp.serviceImpl;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.TreeMap;
import org.apache.commons.text.similarity.LevenshteinDistance;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

@Service
public class ShortestDistAlgoServiceImpl extends LevenshteinDistance{
	private static final Logger logger = LogManager.getLogger(ShortestDistAlgoServiceImpl.class.getName());

	
	public String  getGender(String inputUserName) throws Exception {
		logger.info("in ShortestDistAlgoService");
		ShortestDistAlgoServiceImpl shortestDistance = new ShortestDistAlgoServiceImpl();
		String gender = "";
		Integer minDistance=0;
		try {
			TreeMap<Integer, String> userAndDistMap = new TreeMap<>();	
			String txtFileMale = "D:/M2_Pc/InSaas/insaasSouce/tags/dev/source/insaasApplication/insaasApp/xmlFiles/maleUsers.txt";
			String txtFileFemale = "D:/M2_Pc/InSaas/insaasSouce/tags/dev/source/insaasApplication/insaasApp/xmlFiles/femaleUsers.txt";
			List<String> maleData=Files.readAllLines(Paths.get(txtFileMale));
			List<String> femaleData=Files.readAllLines(Paths.get(txtFileFemale));
			inputUserName = inputUserName.replaceAll( "[^a-zA-Z0-9 ]" , "" ); 	
			for (String userName : femaleData) {
				int distance = shortestDistance.apply(inputUserName, userName);
				userAndDistMap.put(distance, "Female");
			}
			logger.info("userAndDistMap size for female data=="+userAndDistMap.size());
			for (String userName : maleData) {
				int distance = shortestDistance.apply(inputUserName, userName);
				userAndDistMap.put(distance, "Male");
			}
			minDistance= userAndDistMap.firstKey();
			gender=userAndDistMap.get(minDistance);
			logger.info("minimum distance is="+minDistance+"\twith Gender="+gender+"\t for user="+inputUserName);

		} catch (Exception e) {
			logger.error("Exception is=", e);
			e.printStackTrace();
		}
		return gender;
	}
}
