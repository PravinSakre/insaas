/**
 * 
 */
package com.insaas.insaasApp.serviceImpl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.impl.XMLResponseParser;
import org.apache.solr.common.SolrInputDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.insaas.insaasApp.common.Constant;
import com.insaas.insaasApp.config.PropertyFileConfig;
import com.insaas.insaasApp.entity.Customer;
import com.insaas.insaasApp.entity.User;
import com.insaas.insaasApp.pojo.SolrSearchFields;
import com.insaas.insaasApp.repository.CustomerRepository;
import com.insaas.insaasApp.repository.SolrAdacDocumentRepository;
import com.insaas.insaasApp.repository.UserRepository;
import com.insaas.insaasApp.service.SecurityService;
import com.insaas.insaasApp.service.SolrService;
import com.insaas.insaasApp.solr.core.document.SolrAdacDocument;

/**
 * @author Administrator
 *
 */
@Service
public class SolrServiceImpl implements SolrService {
	private static final Logger logger = LogManager.getLogger(SolrServiceImpl.class.getName());

	protected class QESXMLResponseParser extends XMLResponseParser {
		public QESXMLResponseParser() {
			super();
		}

		@Override
		public String getContentType() {
			return "text/xml; charset=UTF-8";
		}
	}

	/**
	 * this will help for security services like authentication. accessToken
	 */
	@Autowired
	private SecurityService securityService;

	@Autowired
	private SolrAdacDocumentRepository solrAdacDocumentRepository;

	@Autowired
	private PropertyFileConfig propertyFileConfig;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private CustomerRepository customerRepository;

	@Override
	public SolrClient connectToSolrServer(String urlString, String accessToken) throws Exception {

		User user = userRepository.findByUserId(securityService.getUserId(accessToken).toString());
		if (null == user) {
			throw new Exception("user details not found for token :: " + accessToken);
		}
		HttpSolrClient httpSolrClient;
		httpSolrClient = new HttpSolrClient.Builder(urlString).build();
		if (null == httpSolrClient) {
			System.out.println("httpSolrClient ");
			throw new Exception();
		}
		httpSolrClient.setParser(new XMLResponseParser());
		System.out.println("Solr Client is :" + httpSolrClient);
		return httpSolrClient;
	}

	@Override
	public Object solrSearch(SolrSearchFields solrSearchFields) throws Exception {
		return getSolrResponse(prepareSolrQuery(solrSearchFields));
	}

	public String prepareSolrQuery(SolrSearchFields solrSearchFields) throws Exception {
		logger.info("prepare sor query..");
		User user = userRepository
				.findByUserId(securityService.getUserId(solrSearchFields.getAccessToken()).toString());
		if (null == user) {
			throw new Exception("user details not found for token :: " + solrSearchFields.getAccessToken());
		}

		Customer customer = customerRepository.findByCustomerId(user.getOrgId());
		String solrFields[] = { "date", "Rating", "Company", "Product", "Source", "Feedback", "Aspect", "Sentiment",
				"Modified" };
		// propertyFileConfig.getSolrServerUrl()
		// String url =
		// "http://localhost:8983/solr/adac/select?facet.field=Aspect&facet=on&facet.limit=10&&rows="+
		// solrSearchFields.getRows();
		logger.info("customer.getCoreName().trim() :: " + customer.getCoreName().trim());
		String url = propertyFileConfig.getSolrServerUrl() + customer.getCoreName().trim()
				+ "/select?facet.field=Aspect&facet=on&facet.limit=10&&rows=" + solrSearchFields.getRows();
		if (!solrSearchFields.getSearch().isEmpty() && !solrSearchFields.getSearch().equalsIgnoreCase("*:*")) {
			String searchValue = solrSearchFields.getSearch().replaceAll("\\s", "").trim().split(":")[0];
			List<String> solrFieldsList = Arrays.asList(solrFields);
			if (!solrFieldsList.isEmpty() && solrFieldsList.contains(searchValue)) {
				String query = solrSearchFields.getSearch().replaceAll("\\s", "").replaceAll(",", "%2C%20").trim();
				url += "&q=" + query + "";
			} else {
				url += "&q=*" + solrSearchFields.getSearch() + "*";
			}
		} else {
			url += "&q=*:*";
		}

		int count = 0;
		if (!solrSearchFields.getAspect().isEmpty()) {
			Set<String> uniqueAspects = solrSearchFields.getAspect().stream().collect(Collectors.toSet());
			for (String aspect : uniqueAspects) {
				if (aspect.trim().isEmpty()) {
					continue;
				}
				url += "&fq=Aspect='" + aspect + "'";
				if (count < solrSearchFields.getAspect().size()) {
					url += "&";
				}
				count++;
			}
		}

		if (solrSearchFields.getStart() != 0) {
			url += "&start=" + solrSearchFields.getStart();
		}
		return url;
	}

	@SuppressWarnings("unchecked")
	public Object getSolrResponse(String url) {
		StringBuilder responseText = null;
		try {
			HttpURLConnection connection = null;
			int responseCode = 0;
			responseText = new StringBuilder();
			BufferedReader bufferReader = null;
			System.out.println("Url11 :: " + url);
			url = url.replaceAll("\\s", "").trim();
			connection = (HttpURLConnection) new URL(url).openConnection();
			connection.setDoOutput(true);
			connection.setRequestProperty("Content-Type", "application/json; charset=utf-8");
			connection.setRequestProperty("Content-Language", "de");
			responseCode = ((HttpURLConnection) connection).getResponseCode();
			if (responseCode > 0 && responseCode == 200) {
				InputStream inpuntStream = connection.getInputStream();
				bufferReader = new BufferedReader(new InputStreamReader(inpuntStream, StandardCharsets.UTF_8));
				String text = "";
				while ((text = bufferReader.readLine()) != null) {
					if (null != text && !text.isEmpty()) {
						responseText.append(text);
					}
				}

				Map<Object, Object> responseMap = new HashMap<>();
				responseMap = (Map<Object, Object>) new ObjectMapper().readValue(responseText.toString(),
						new TypeReference<Object>() {
						});
				logger.info("Solr response" + responseMap);
				return responseMap;
			}
		} catch (Exception e) {
			System.out.println("Exception in doGet.." + e);
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String updateSolrDocuments(String documents, String accessToken) throws Exception {
		logger.info("in service:: " + documents);
		User user = userRepository.findByUserId(securityService.getUserId(accessToken).toString());
		if (null == user) {
			throw new Exception("user details not found for token :: " + accessToken);
		}

		Customer customer = customerRepository.findByCustomerId(user.getOrgId());

		SolrClient solrClient = connectToSolrServer(
				propertyFileConfig.getSolrServerUrl() + customer.getCoreName().trim(), accessToken);
		SolrInputDocument solrInputDocument = null;
		List<SolrInputDocument> docs = new ArrayList<>();

		Map<Object, Object> responseMap = new HashMap<>();
		responseMap = (Map<Object, Object>) new ObjectMapper().readValue(documents, new TypeReference<Object>() {
		});
		logger.info("responseMap :: " + responseMap);
		String userId = String.valueOf(user.getIdUserAutoGen());
		String id = "";
		String url = propertyFileConfig.getSolrServerUrl() + customer.getCoreName() + "/select?q=*:*&fq=id:\"";

		List<Map<Object, Object>> responseMapList = (List<Map<Object, Object>>) responseMap.get("data");
		String sentiment = "";
		for (Map<Object, Object> reviewMap : responseMapList) {
			id = reviewMap.get("documentId").toString();
			Object obj = getSolrResponse(url + id + "\"");
			id = "";
			logger.info("url + id :: " + url + id + "\"");
			ObjectMapper oMapper = new ObjectMapper();
			Map<String, Object> fields = oMapper.convertValue(obj, Map.class);
			solrInputDocument = new SolrInputDocument();
			Map<Object, Object> response = (Map<Object, Object>) fields.get("response");
			List<Map<Object, Object>> docsListMap = (List<Map<Object, Object>>) response.get("docs");
			for (Map<Object, Object> docsMap : docsListMap) {
				sentiment = "";
				try {
					solrInputDocument.setField("date", docsMap.get("date"));
					solrInputDocument.setField("documentId", docsMap.get("documentId"));
					solrInputDocument.setField("Rating", docsMap.get("Rating"));
					solrInputDocument.setField("Company", docsMap.get("Company"));
					solrInputDocument.setField("Company", docsMap.get("Company"));
					solrInputDocument.setField("Source", docsMap.get("Source"));
					solrInputDocument.setField("FeedbackHashValue", docsMap.get("FeedbackHashValue"));
					solrInputDocument.setField("Feedback", docsMap.get("Feedback"));
					List<String> aspectList = new ArrayList<>();
					List<Map<String, String>> mapAspectList = (List<Map<String, String>>) reviewMap.get("aspect");
					logger.info("aspectList11 :: " + mapAspectList.size());
					for (Map<String, String> mapAspect : mapAspectList) {
						if (!mapAspect.get("value").equalsIgnoreCase("undefined"))
							aspectList.add(mapAspect.get("value"));
					}
					logger.info("aspectList :: " + aspectList.size());
					if (aspectList != null && aspectList.size() > 0) {
						solrInputDocument.setField("Aspect", aspectList);
					}
//					logger.info("responseMap.get(\"sentiment\") :: " + reviewMap.get("sentiment").toString().replaceAll("[", ""));
					if (reviewMap.get("sentiment") != null && !reviewMap.get("sentiment").toString().trim().isEmpty()) {
						logger.info("true");
						sentiment = reviewMap.get("sentiment").toString().trim();
						sentiment = sentiment.replaceAll("\\[", "").replaceAll("\\]", "");
						logger.info("final saved setniment :: " + sentiment);
						solrInputDocument.setField("Sentiment", sentiment.trim());
					} else {
						solrInputDocument.setField("Sentiment", docsMap.get("Sentiment"));
					}
					solrInputDocument.setField("id", docsMap.get("id"));
					solrInputDocument.setField("Modified", "yes");
					solrInputDocument.setField("UpdatedBy", userId);
					solrInputDocument.setField("UpdatedOn", LocalDateTime.now());
				} catch (Exception e) {
					logger.error(e.getMessage());
				}
			}
			docs.add(solrInputDocument);
		}
		solrClient.add(docs);
		solrClient.commit();
		solrClient.close();
		logger.info("responsMap :: " + responseMap);
		// TODO Auto-generated method stub
		return "Records updated..";
	}

	@SuppressWarnings("unchecked")
	@Override
	public String updateMassSolrDocuments(String solrSearchFields, String accessToken) throws Exception {
		// securityService
		logger.info("in updateMassSolrDocuments service:: " + solrSearchFields);
		try {
			User user = userRepository.findByUserId(securityService.getUserId(accessToken).toString());
			if (null == user) {
				throw new Exception("user details not found for token :: " + accessToken);
			}

			Customer customer = customerRepository.findByCustomerId(user.getOrgId());
			SolrClient solrClient = connectToSolrServer(propertyFileConfig.getSolrServerUrl() + customer.getCoreName(),
					accessToken);
			ObjectMapper oMapper = new ObjectMapper();
			Gson g = new Gson();
			SolrSearchFields solrSearchField = g.fromJson(solrSearchFields, SolrSearchFields.class);
			String aspect = solrSearchField.getUpdatedValue();
			Object object = getSolrResponse(prepareSolrQuery(solrSearchField));
			oMapper = new ObjectMapper();
			Map<String, Object> fields = oMapper.convertValue(object, Map.class);
			Map<Object, Object> response = (Map<Object, Object>) fields.get("response");
			List<Map<Object, Object>> docsListMap = (List<Map<Object, Object>>) response.get("docs");
			SolrInputDocument solrInputDocument = null;
			oMapper = new ObjectMapper();
			List<SolrInputDocument> docs = new ArrayList<>();
			int documentCounter = 0;
			int listIndex = 0;
			for (Map<Object, Object> docsMap : docsListMap) {
				try {
					solrInputDocument = new SolrInputDocument();
					solrInputDocument.setField("date", docsMap.get("date"));
					solrInputDocument.setField("documentId", docsMap.get("documentId"));
					solrInputDocument.setField("Rating", docsMap.get("Rating"));
					solrInputDocument.setField("Company", docsMap.get("Company"));
					solrInputDocument.setField("Company", docsMap.get("Company"));
					solrInputDocument.setField("Source", docsMap.get("Source"));
					solrInputDocument.setField("FeedbackHashValue", docsMap.get("FeedbackHashValue"));
					solrInputDocument.setField("Feedback", docsMap.get("Feedback"));
					List<String> docAspectList = (List<String>) docsMap.get("Aspect");
					if (docAspectList == null) {
						docAspectList = new ArrayList<>();
					}

					switch (String.valueOf(solrSearchField.getUpdateOrDeleteflag())) {
					case Constant.APPLY_TO_ALL:
						docAspectList.add(aspect);
						break;
					case Constant.APPLY_TO_SELECTED:
						List<String> applyIds = solrSearchField.getUpdatedDocId();
						for (String id : applyIds) {
							if (id.trim().equalsIgnoreCase(docsMap.get("id").toString().trim())) {
								docAspectList.add(aspect);
							}
						}
						break;
					case Constant.DELETE_FROM_ALL:
						docAspectList.remove(aspect);
						break;
					case Constant.DELETE_FROM_SELECTED:
						logger.info("DELETE_FROM_SELECTED :: " + docAspectList);
						List<String> deleteIds = solrSearchField.getUpdatedDocId();
						for (String id : deleteIds) {
							logger.info("DELETE_FROM_SELECTED id:: " + id + ", doc :: "
									+ docsMap.get("id").toString().trim());
							if (id.trim().equalsIgnoreCase(docsMap.get("id").toString().trim())) {
								logger.info("DELETE_FROM_SELECTED id1:: " + docsMap.get("id") + ", id2 : " + id);
								docAspectList.remove(aspect);
							}
						}
						logger.info("After DELETE_FROM_SELECTED :: " + docAspectList);
						break;
					}
					solrInputDocument.setField("Aspect", docAspectList);
					solrInputDocument.setField("Sentiment", docsMap.get("Sentiment"));
					solrInputDocument.setField("id", docsMap.get("id"));
					solrInputDocument.setField("Modified", "yes");
					solrInputDocument.setField("UpdatedBy", user.getIdUserAutoGen());
					solrInputDocument.setField("UpdatedOn", LocalDateTime.now());
					docs.add(solrInputDocument);
					solrClient.add(docs);
					if (documentCounter >= 1000 || listIndex == docsListMap.size() - 1) {
						solrClient.commit();
						documentCounter = 0;
					}
					listIndex++;
					documentCounter++;

				} catch (Exception e) {
					logger.error(e.getMessage());
				}
			}
			solrClient.close();
			logger.info("no fo documents updated :: " + listIndex);
		} catch (Exception e) {
			logger.error(e);
			throw new Exception(e);
		}
		// TODO Auto-generated method stub
		return "Records updated..";
	}

	public static void main(String... strings) {
		class QESXMLResponseParser extends XMLResponseParser {
			public QESXMLResponseParser() {
				super();
			}

			@Override
			public String getContentType() {
				return "text/xml; charset=UTF-8";
			}
		}
		HttpSolrClient solrClient;
		// http://services.insaas.ai/bwcinsaasdbstorage/BG_thinktank
		String urlString = "";
		solrClient = new HttpSolrClient.Builder(urlString).build();
//		solrClient.setParser(processor);
//		solrClient.setParser(new XMLResponseParser());
		System.out.println("Solr Client is :" + solrClient);
		SolrServiceImpl impl = new SolrServiceImpl();
		String query = "";
		Object object = impl.getSolrResponse(query);

		/////////////
		ObjectMapper oMapper = new ObjectMapper();
		Gson g = new Gson();
		oMapper = new ObjectMapper();
		Map<String, Object> fields = oMapper.convertValue(object, Map.class);
		Map<Object, Object> response = (Map<Object, Object>) fields.get("response");
		List<Map<Object, Object>> docsListMap = (List<Map<Object, Object>>) response.get("docs");
		SolrInputDocument solrInputDocument = null;
		oMapper = new ObjectMapper();
		List<SolrInputDocument> docs = new ArrayList<>();
		int documentCounter = 0;
		int listIndex = 0;
		for (Map<Object, Object> docsMap : docsListMap) {
			try {
				solrInputDocument = new SolrInputDocument();
				solrInputDocument.setField("date", docsMap.get("date"));
				solrInputDocument.setField("documentId", docsMap.get("documentId"));
				solrInputDocument.setField("Rating", docsMap.get("Rating"));
				solrInputDocument.setField("Company", docsMap.get("Company"));
				solrInputDocument.setField("Company", docsMap.get("Company"));
				solrInputDocument.setField("Source", docsMap.get("Source"));
				solrInputDocument.setField("FeedbackHashValue", docsMap.get("FeedbackHashValue"));
				solrInputDocument.setField("Feedback", docsMap.get("Feedback"));
				List<String> docAspectList = (List<String>) docsMap.get("Aspect");
				if (docAspectList == null) {
					docAspectList = new ArrayList<>();
				}
				for(String aspect: docAspectList) {
					System.out.print(aspect+ " ,");
				}
				docAspectList.remove("undefined");
				solrInputDocument.setField("Aspect", docAspectList);
				solrInputDocument.setField("Sentiment", docsMap.get("Sentiment"));
				solrInputDocument.setField("id", docsMap.get("id"));
//				solrInputDocument.setField("Modified", "yes");
//				solrInputDocument.setField("UpdatedBy", "1");
//				solrInputDocument.setField("UpdatedOn", LocalDateTime.now());
				docs.add(solrInputDocument);
				solrClient.add(docs);
				if (documentCounter >= 10 || listIndex == docsListMap.size() - 1) {
					solrClient.commit();
					documentCounter = 0;
				}
				listIndex++;
				documentCounter++;

			} catch (Exception e) {
				logger.error(e.getMessage());
			}
		}
		try {
			solrClient.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		logger.info("no fo documents updated :: " + docsListMap.size());
		logger.info("no fo documents updated :: " + listIndex);
	}
}
