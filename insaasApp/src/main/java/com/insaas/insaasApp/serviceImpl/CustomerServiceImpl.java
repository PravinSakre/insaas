package com.insaas.insaasApp.serviceImpl;

import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.insaas.insaasApp.dto.CustomerDto;
import com.insaas.insaasApp.entity.Customer;
import com.insaas.insaasApp.entity.CustomerDetails;
import com.insaas.insaasApp.repository.CustomerDetailsRepository;
import com.insaas.insaasApp.repository.CustomerRepository;
import com.insaas.insaasApp.service.CustomerService;
@Service
public class CustomerServiceImpl implements CustomerService{

	private static final Logger logger = LogManager.getLogger(CustomerServiceImpl.class.getName());
	
	@Autowired
	private CustomerRepository customerRepository;
	@Autowired
	private CustomerDetailsRepository customerDetailsRepository;
	
	CustomerDetails customerDetails;


	
	@Override
	public List<CustomerDto> getAllCutomers() {
		List<CustomerDto>  customerDetailsDtoList=new ArrayList();	
		//logger.info("in serviceimplimentation");
		//List<Customer> customerlist=customerRepository.findAll();	
		List<Customer> customerlist=customerRepository. findAllByActive(true); 
		//List<CustomerDetails> customerlist=customerDetailsRepository. findAllByActive(true); 
		 
		for(Customer customer:customerlist)
		{
			int custId=customer.getCustomerId();
			//logger.info("CustomerId***"+custId);
			//List<CustomerDetails> customerDetailsList =customerDetailsRepository.findCustomerDetailsByCustomerId(custId);
			List<CustomerDetails> customerDetailsList =customerDetailsRepository.findCustomerDetailsByCustomerIdAndActive(custId,true);
			logger.info("customerDetailsList="+customerDetailsList);
				CustomerDto customerDto=new CustomerDto();
				customerDto.setCustomerId(customer.getCustomerId());
				customerDto.setCustomerName(customer.getCustomerName());
				customerDto.setCoreName(customer.getCoreName());
				customerDto.setCustomerDetailsList(customerDetailsList);				
				customerDetailsDtoList.add(customerDto);
		}
			
		logger.info("Customer data="+customerDetailsDtoList.toString());

		return customerDetailsDtoList;
	}
	
	
	
	
	@Override
	public String addCustomerData(CustomerDto customerDto) throws Exception {
		logger.info("customerDto at the add customer method=="+customerDto.toString());
		try {
		Customer customerExist=customerRepository.findByCustomerName(customerDto.getCustomerName());
		logger.info("customer Exist="+customerExist);
		Integer custId=customerExist.getCustomerId();
		logger.info("custId="+custId);
		if(custId!=null)
		{
			CustomerDetails customerDetails=customerDetailsRepository.findByCustomerId(custId);
			logger.info("customerDetails Id"+customerDetails);
			CustomerDetails customerDetails1=new CustomerDetails();
			customerDetails1.setCustomerId(custId);
			customerDetails1.setuRL(customerDto.getuRL());
			customerDetails1.setaPI_Key(customerDto.getaPI_Key());
			customerDetails1.setProject_Token(customerDto.getProject_Token());
			customerDetails1.setRun_Token(customerDto.getRun_Token());
			customerDetails1.setSourceOfInfo(customerDto.getSourceOfInfo());
			customerDetails1.setActive(true);
			customerDetailsRepository.save(customerDetails1);		
		}
	
		/*if(customerExist!=null) {
			logger.info("customerExist block customerDto.getCustomerName()="+customerDto.getCustomerName());
			customerExist.setCustomerName(customerDto.getCustomerName());
			customerExist.setCoreName(customerDto.getCoreName());
			customerExist.setAnalysisStatement(customerDto.getAnalysisStatement());
			customerRepository.save(customerExist);
			CustomerDetails customerDetails=new CustomerDetails();
			customerDetails.setProject_Token(customerDto.getProject_Token());
			customerDetails.setaPI_Key(customerDto.getaPI_Key());
			customerDetails.setRun_Token(customerDto.getRun_Token());
			customerDetails.setSourceOfInfo(customerDto.getSourceOfInfo());
			customerDetails.setCustomerId(customerExist.getCustomerId());
			customerDetails.setuRL(customerDto.getuRL());
			//customerDetails.setActive(true);
			customerDetailsRepository.save(customerDetails);
		}*/
		else {
			logger.info("creating new customer...");
			Customer customer=new Customer();
			logger.info("customerDto.getCustomerName()="+customerDto.getCustomerName());
			customer.setCustomerName(customerDto.getCustomerName());
			customer.setCoreName(customerDto.getCoreName());
			customer.setAnalysisStatement(customerDto.getAnalysisStatement());
			customer.setActive(true);
			customerRepository.save(customer);
			CustomerDetails customerDetails=new CustomerDetails();
			customerDetails.setProject_Token(customerDto.getProject_Token());
			customerDetails.setaPI_Key(customerDto.getaPI_Key());
			customerDetails.setRun_Token(customerDto.getRun_Token());
			customerDetails.setSourceOfInfo(customerDto.getSourceOfInfo());
			customerDetails.setCustomerId(customer.getCustomerId());
			customerDetails.setuRL(customerDto.getuRL());
			customerDetails.setActive(true);
			customerDetailsRepository.save(customerDetails);
		}
		
		
		return "Added Successfully...";
		}
		catch(Exception e) {
			logger.error("Eception while adding new customer",e);
			return "Error while adding new customer data";
		}
		
	}
	@Override
	public String updateCustomer(CustomerDto customerDto) throws Exception {
		logger.info("customerDto at the update customer method=="+customerDto.toString());
		try {
			CustomerDetails customerDetails=customerDetailsRepository.findByCustDetailsId(customerDto.getCustDetailsId());
			Integer custId=customerDetails.getCustomerId();
	    	
	    	logger.info(" im in custId imple : "+custId);
	    	Customer  customer=new Customer();
	    	customer=customerRepository.findByCustomerId(custId);
	    	logger.info("  customer  : "+customer);
	    	
	    	CustomerDto customerDto1 = new CustomerDto();
	    
	    	customer.setCustomerId(customerDto.getCustomerId());
	    	customer.setCustomerName(customerDto.getCustomerName());
	    	customer.setCoreName(customerDto.getCoreName());
	    	customer.setAnalysisStatement(customerDto.getAnalysisStatement());
			customerRepository.save(customer);
			customerDetails.setCustDetailsId(customerDto.getCustDetailsId());
			customerDetails.setuRL(customerDto.getuRL());
			customerDetails.setSourceOfInfo(customerDto.getSourceOfInfo());
			customerDetails.setaPI_Key(customerDto.getaPI_Key());
			customerDetails.setProject_Token(customerDto.getProject_Token());
			customerDetails.setRun_Token(customerDto.getRun_Token());
			customerDetailsRepository.save(customerDetails);
			
			
//		Customer customer=customerRepository.findByCustomerId(customerDto.getCustomerId());
//		customer.setCustomerName(customerDto.getCustomerName());
//		customer.setCoreName(customerDto.getCoreName());
//		customer.setAnalysisStatement(customerDto.getAnalysisStatement());
//		customerRepository.save(customer);
//
//			CustomerDetails customerDetails=customerDetailsRepository.findByCustDetailsId(customerDto.getCustDetailsId());
//			customerDetails.setProject_Token(customerDto.getProject_Token());
//			customerDetails.setaPI_Key(customerDto.getaPI_Key());
//			customerDetails.setRun_Token(customerDto.getRun_Token());
//			customerDetails.setSourceOfInfo(customerDto.getSourceOfInfo());
//			customerDetails.setCustomerId(customer.getCustomerId());
//			customerDetails.setuRL(customerDto.getuRL());
//			customerDetailsRepository.save(customerDetails);
//			logger.info("saving customer details..."+customerDetails);
//			
//		return "updated record Successfully...";
		}
		catch(Exception e) {
			logger.error("Eception while adding new customer",e);
			return "Error while adding new customer data";
		}
		return "";
		
	}

	 @Override
	    public CustomerDto getSingleCustomerData(Integer custDetailsId) throws Exception
	    {    	
	    	logger.info(" im in customerDto imple : "+custDetailsId);
	    	CustomerDetails customerDetails=new CustomerDetails();
	    	customerDetails=customerDetailsRepository.findByCustDetailsId(custDetailsId);
	    	logger.info(" customerDetails : "+customerDetails);
    	Integer custId=customerDetails.getCustomerId();
    	
    	logger.info(" im in custId imple : "+custId);
    	Customer  customer=new Customer();
    	customer=customerRepository.findByCustomerId(custId);
    	logger.info("  customer  : "+customer);
    	
    	CustomerDto customerDto = new CustomerDto();
    	customerDto.setCustDetailsId(customerDetails.getCustDetailsId());
    	customerDto.setCustomerId(customer.getCustomerId());
		customerDto.setCustomerName(customer.getCustomerName());
		customerDto.setCoreName(customer.getCoreName());
		customerDto.setAnalysisStatement(customer.getAnalysisStatement());
		customerDto.setuRL(customerDetails.getuRL());
		customerDto.setSourceOfInfo(customerDetails.getSourceOfInfo());
		customerDto.setaPI_Key(customerDetails.getaPI_Key());
		customerDto.setProject_Token(customerDetails.getProject_Token());
		customerDto.setRun_Token(customerDetails.getRun_Token());

		
//			//CustomerDetails customerList =customerDetailsRepository.findByCustomerId(custId);
//	    	CustomerDetails customerList =customerDetailsRepository.findAllByCustomerId(custId);
//	    	logger.info(" customerList : "+customerList);
//			CustomerDto customerDto = new CustomerDto();
//			customerDto.setCustomerId(customer.getCustomerId());
//			customerDto.setCustomerName(customer.getCustomerName());
//			customerDto.setCoreName(customer.getCoreName());
//			customerDto.setAnalysisStatement(customer.getAnalysisStatement());
//		
//			customerDto.setCustDetailsId(customerList.getCustDetailsId());
//			customerDto.setuRL(customerList.getuRL());
//			customerDto.setSourceOfInfo(customerList.getSourceOfInfo());
//			customerDto.setaPI_Key(customerList.getaPI_Key());
//			customerDto.setProject_Token(customerList.getProject_Token());
//			customerDto.setRun_Token(customerList.getRun_Token());
//			customerDto.setAnalysisStatement(customer.getAnalysisStatement());
//	         
//			logger.info("Single Customer data="+customerDto.toString());
			
			return customerDto;
	    }
	 
/*	 @Override
	    public CustomerDto getSingleCustomerData(Integer customerId) throws Exception
	    {    
		 List<CustomerDto>  customerDetailsDtoList=new ArrayList();	
	    	logger.info(" im in customerDto imple : "+customerId );
	    	Customer customer=customerRepository.findByCustomerId(customerId);
	    	Integer custId=customer.getCustomerId();
	    	CustomerDetails customerDetails=new CustomerDetails();
	    	logger.info(" im in custId imple : "+custId);
			//CustomerDetails customerList =customerDetailsRepository.findByCustomerId(custId);
	    	List<CustomerDetails> customerList =customerDetailsRepository.findAllByCustomerId(custId);
	    	for(CustomerDetails customerDetails1:customerList)
			{
	    		int custId1=customerDetails1.getCustomerId();
	    	logger.info(" customerList : "+customerList);
			CustomerDto customerDto = new CustomerDto();
			customerDto.setCustomerId(customer.getCustomerId());
			customerDto.setCustomerName(customer.getCustomerName());
			customerDto.setCoreName(customer.getCoreName());
			customerDto.setAnalysisStatement(customer.getAnalysisStatement());
			
			customerDto.setCustDetailsId(customerDetails1.getCustDetailsId());
			customerDto.setuRL(customerDetails1.getuRL());
			customerDto.setSourceOfInfo(customerDetails1.getSourceOfInfo());
			customerDto.setaPI_Key(customerDetails1.getaPI_Key());
			customerDto.setProject_Token(customerDetails1.getProject_Token());
			customerDto.setRun_Token(customerDetails1.getRun_Token());
			customerDto.setAnalysisStatement(customer.getAnalysisStatement());
			customerDetailsDtoList.add(customerDto);
			
						
			}
	    	logger.info("Single Customer data="+customerDetailsDtoList.toString());
	    	return customerDto;
	    }*/
	 
	 
	 
	 @Override
		public Integer deleteCustomerData(Integer custDetailsId) throws Exception {
			logger.info("customerId="+custDetailsId);
			// TODO Auto-generated method stub
			//Customer customer=new Customer();
			CustomerDetails customerDetails=new CustomerDetails();
//			customerDetails=customerDetailsRepository.findCustomerIdByCustDetailsId(custDetailsId);
//			logger.info("new customerDetails"+customerDetails);
			
			
			logger.info("in delete serimpl");
			//CustomerDetails customerDetails=new CustomerDetails();
			
			customerDetails=customerDetailsRepository.findByCustDetailsId(custDetailsId);
			int custId=customerDetails.getCustomerId();
			List<CustomerDetails> customerDetailslist=customerDetailsRepository.findAllByActiveAndCustomerId(true,custId);
			int size = customerDetailslist.size();
			logger.info("size="+size);
			if(customerDetailslist.size()==1)
			{
				logger.info("customer size is 1");
				for(CustomerDetails customerDetails1:customerDetailslist)
				{
					Integer custId1=customerDetails1.getCustomerId();
					logger.info("customer id1="+custId1);
					Customer customer1=new Customer();
					customer1=customerRepository.findByCustomerId(custId1);
					logger.info("customer1"+customer1);
					customer1.setActive(false);
					customerRepository.save(customer1);
					
				}	
			}
			
			
			/*for(CustomerDetails customerDetails1:customerDetailslist)
			{
				Integer custId1=customerDetails1.getCustomerId();
				logger.info("customer id1="+custId1);
				if(custId1==null)
				{
					Customer customer1=new Customer();
					customer1=customerRepository.findByCustomerId(custId1);
					logger.info("customer1="+customer1);
					//customer1.setActive(false);
					
				}
			}*/
			
			logger.info("CustomerDetailslist and customerId="+customerDetailslist);
			if(custId==0)
			{
				logger.info("in cust Id");
				
				
			}
			logger.info("custId..."+custId);
			Customer  customer=new Customer();
			customer=customerRepository.findByCustomerId(custId);
			//customer.setActive(false);
			//customerRepository.save(customer);
			logger.info("customerDetails..."+custDetailsId);
			logger.info("customerobject..."+customerDetails);
			// custId=customerRepository.findByCustomerIdAndActive(customerId,false);
			customerDetails.setActive(false);
			
			CustomerDetails cmr=customerDetailsRepository.save(customerDetails);
			logger.info("customerRepository..."+cmr);
			
			return 1;
		}

	@Override
	public CustomerDetails getCustomer() {
		// TODO Auto-generated method stub
		return null;
	}




	
//	@Override
//	public String updateCustomerData(CustomerDto customerDto) throws Exception {
//		// TODO Auto-generated method stub
//		return null;
//	}
	 
	 
//	@Override
//	public CustomerDetails getCustomer() {
//		// TODO Auto-generated method stub
//		return null;
//	}

	
	

}
