package com.insaas.insaasApp.serviceImpl;

import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.logging.log4j.LogManager;


import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.insaas.insaasApp.service.MessageService;
/*import com.twilio.Twilio;
import com.twilio.type.PhoneNumber;*/

/**
 * Represents an MessageServiceImpl will handles messaging service business logic
 * @author BrainWave Consulting
 * @since Dec 25, 2018
 */
@Service
public class MessageServiceImpl implements MessageService{

	/** Logger for this class. */
	private static final Logger logger = LogManager.getLogger(MessageServiceImpl.class.getName());
	/**
	 * This method is for getting map object
	 * @author Brainwave Consulting
	 * @since Dec 25, 2018
	 * @param configuration
	 * @return
	 */
	private Map<String, List<String>> getMapObject(String configuration) {
		logger.error("getMapObject:configuration="+configuration);
		Map<String, List<String>> propertiesList = new HashMap<>();
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			propertiesList = objectMapper.readValue(configuration, new TypeReference<Map<String, List<String>>>() {
			});
		} catch (JsonParseException e) {
			logger.error("Exception while get Map Object"+e);
			e.printStackTrace();
			if (logger.isDebugEnabled()) {
				logger.debug("getMapObject(String) - {}", "JsonParseException :: " + e); //$NON-NLS-1$ //$NON-NLS-2$
			}
		} catch (JsonMappingException e) {
			logger.error("Exception while get Map Object"+e);
			e.printStackTrace();
			if (logger.isDebugEnabled()) {
				logger.debug("getMapObject(String) - {}", "JsonMappingException :: " + e); //$NON-NLS-1$ //$NON-NLS-2$
			}
		} catch (IOException e) {
			logger.error("Exception while get Map Object"+e);
			e.printStackTrace();
			if (logger.isDebugEnabled()) {
				logger.debug("getMapObject(String) - {}", "IOException :: " + e); //$NON-NLS-1$ //$NON-NLS-2$
			}
		}
		return propertiesList;
	}
	/**
	 * This method is for getting properties
	 * @author Brainwave Consulting
	 * @since Dec 25, 2018
	 * @param configuration
	 * @return
	 */
	private Properties getProperties(String configuration) {
		logger.debug("getProperties:configuration"+configuration);
		Properties properties = System.getProperties();
		Map<String, List<String>> propertiesList = this.getMapObject(configuration);
		propertiesList.forEach((property, values) -> {
			properties.setProperty(property, values.get(0).trim());
		});
		return properties;
	}
/**
 * This method is for sending message
 * @author Brainwave Consulting
 * @since Dec 25, 2018
 * @param configuration
 * @param messageFrom
 * @param messageTo
 * @param message
 */
	public void sendMessage(String configuration, String messageFrom, String messageTo, String message) {
		logger.debug("sendMessage:configuration"+configuration+"\t messageFrom"+messageFrom+"\t messageTo"+messageTo+"\t message"+message);
		Properties properties = getProperties(configuration);

		final String ACCOUNT_SID = properties.getProperty("account.sid");

		final String AUTH_TOKEN = properties.getProperty("auth.token");

		logger.info("ACCOUNT_SID :: " + ACCOUNT_SID+" , AUTH_TOKEN :: "+ AUTH_TOKEN);
		/*
		 * Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
		 * 
		 * com.twilio.rest.api.v2010.account.Message messageObject =
		 * com.twilio.rest.api.v2010.account.Message .creator(new
		 * PhoneNumber(messageTo), // to new PhoneNumber(messageFrom), message) // from
		 * "helloooooooooo?") .create();
		 */
	}
	/**
	 * This method is for sending email
	 */
	public void sendMail(String configuration, String mailFrom, String mailTo, String subject, String message) {
		logger.debug("sendMail: configuration="+ configuration +"\t mailFrom"+ mailFrom +"\t mailTo"+ mailTo+"\t subject"+ subject +"\t message"+message);
		if (configuration == null ||  configuration.isEmpty()) {
			logger.info("sendMail configuration not found...::" + configuration);
			return;
		}
		if (mailFrom != null && mailFrom.isEmpty() && mailTo != null && mailTo.isEmpty()) {
			logger.info("sendMail MailFrom and mailTo not found...::" + mailFrom + ", :: " + mailTo);
			return;
		}

		if (message != null && message.isEmpty()) {
			logger.info("sendMail message not found...::" + message);
			return;
		}

		Properties properties = getProperties(configuration);
		try {
			Session session = Session.getDefaultInstance(properties, new Authenticator() {
				@Override
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(properties.getProperty("username"),
							properties.getProperty("password"));
				}
			});

			// -- Create a new message --
			Message mailMessage = new MimeMessage(session);
			// -- Set the FROM and TO fields --
			mailMessage.setFrom(new InternetAddress(mailFrom));
			mailMessage.setRecipients(Message.RecipientType.TO, InternetAddress.parse(mailTo, false));
			mailMessage.setSubject(subject);
			mailMessage.setText(message);
			mailMessage.setSentDate(new Date());
			Transport.send(mailMessage);
			logger.debug("endMail sent"); //$NON-NLS-1$ //$NON-NLS-2$
		} catch (Exception e) {
			logger.error("sendMail", "Exception in sending Email: "+ e); //$NON-NLS-1$ //$NON-NLS-2$
			e.printStackTrace();
		}
	}



}
