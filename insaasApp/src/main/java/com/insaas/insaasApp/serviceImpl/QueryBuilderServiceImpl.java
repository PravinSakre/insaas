//package com.insaas.insaasApp.serviceImpl;
//
//import java.io.IOException;
//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;
//import org.apache.solr.client.solrj.SolrClient;
//import org.apache.solr.client.solrj.SolrQuery;
//import org.apache.solr.client.solrj.SolrServerException;
//import org.apache.solr.client.solrj.impl.HttpSolrClient;
//import org.apache.solr.client.solrj.response.QueryResponse;
//import org.apache.solr.common.SolrDocumentList;
//import org.springframework.stereotype.Service;
//import com.insaas.insaasApp.dto.QueryParamsDto;
//import com.insaas.insaasApp.service.QueryBuilderService;
//
//@Service
//class QueryBuilderServiceImpl implements QueryBuilderService {
//	private static final Logger logger = LogManager.getLogger(QueryBuilderServiceImpl.class.getName());
//
//	/*
//	 * public static void main(String args[]) throws SolrServerException,
//	 * IOException { QueryParamsDto queryParamsDto = new QueryParamsDto(); String
//	 * facetFields[] = { "User_Name" }; String facetPivotFields[] = { "Sentiment",
//	 * "User_Name" }; String filterQueryParams[] = { "Sentiment:Positive" }; String
//	 * fieldList[] = { "id", "User_Name", "Feedback_Message", "Individual_Rating",
//	 * "Useful_for", "Aspect1_Sentiment", "Total_Rating", "Gender", "Feedback_Date"
//	 * }; queryParamsDto.setqParam("q"); queryParamsDto.setqValue("Gender:male");
//	 * queryParamsDto.setFacetFields(facetFields);
//	 * queryParamsDto.setFacetPivotFields(facetPivotFields);
//	 * queryParamsDto.setFilterQueryParams(filterQueryParams);
//	 * queryParamsDto.setFlafDistribute(true); queryParamsDto.setFlagFacet(true);
//	 * queryParamsDto.setFlagHightlight(true);
//	 * queryParamsDto.setSimpleFields(fieldList); queryParamsDto.setTermsField("");
//	 * queryParamsDto.setTimeAllowed(898); }
//	 */
//
//	@Override
//	public SolrDocumentList solrQuery(QueryParamsDto queryParamsDto) throws SolrServerException, IOException {
//		logger.info("in query builder service=" + queryParamsDto.toString());
//		SolrQuery solrQuery = new SolrQuery();
//		if (queryParamsDto.getqValue() == null || queryParamsDto.getqValue().isEmpty()) {
//			solrQuery.add(queryParamsDto.getqParam(), "*:*");
//		} else {
//			solrQuery.add(queryParamsDto.getqParam(), queryParamsDto.getqValue());
//			logger.info("Set query to q=" + queryParamsDto.getqValue());
//		}
//		if ((queryParamsDto.getFacetFields() != null)) {
//			solrQuery.addFacetField(queryParamsDto.getFacetFields());
//		}
//		if ((queryParamsDto.getSimpleFields() != null)) {
//			solrQuery.setFields(queryParamsDto.getSimpleFields());
//		}
//		if ((queryParamsDto.getFacetPivotFields() != null)) {
//			logger.info("Facet pivot fields are not empty...");
//			solrQuery.addFacetPivotField(queryParamsDto.getFacetPivotFields());
//		}
//		if ((queryParamsDto.getFacetQuery() != null)) {
//			logger.info("Facet query is not null...=" + queryParamsDto.getFacetQuery());
//			solrQuery.addFacetQuery(queryParamsDto.getFacetQuery());
//		}
//		solrQuery.setTimeAllowed(queryParamsDto.getTimeAllowed());
//		logger.info("facet...");
//		solrQuery.setHighlight(true);
//		logger.info("highlight");
//		solrQuery.addHighlightField("Sentiment");
//		logger.info("hight light field sentiment");
//		solrQuery.addSort("id", SolrQuery.ORDER.desc);
//		logger.info("solrQuery=" + solrQuery);
//		SolrDocumentList results = runQuery(solrQuery);
//		return results;
//	}
//
//	public SolrDocumentList runQuery(SolrQuery solrQuery) throws SolrServerException, IOException {
//		SolrClient client = new HttpSolrClient.Builder("http://18.128.33.248:8983/solr/bwc_core").build();
//		logger.info("client=" + client.ping());
//		QueryResponse response = client.query(solrQuery);
//		SolrDocumentList results = response.getResults();
//		logger.info("results=" + results);
//		for (int i = 0; i < results.size(); ++i) {
//			logger.info(results.get(i));
//		}
//		return results;
//	}
//}
