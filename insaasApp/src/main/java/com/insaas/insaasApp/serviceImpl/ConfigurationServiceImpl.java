package com.insaas.insaasApp.serviceImpl;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.springframework.stereotype.Service;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.insaas.insaasApp.common.MessagesConstant;
import com.insaas.insaasApp.service.ConfigurationService;
/**
 * Represents an ConfigurationServiceImpl will handles configurations required for application
 * @author BrainWave Consulting
 * @since Dec 25, 2018
 */
@Service
public class ConfigurationServiceImpl implements ConfigurationService {

	/** Logger for this class. */
	private static final Logger logger = LogManager.getLogger(ConfigurationServiceImpl.class.getName());

	/** The configuration map. */
	private Map<String, List<Map<String, List<Map<String, List<String>>>>>> configurationMap = new HashMap<>();

	
/**
 * This method is for getting file by xml path
 * @author Brainwave Consulting
 * @since Dec 25, 2018
 * @param xmlFilePath
 * @return
 */
	private File getXmlFile(String xmlFilePath) {
		
		logger.debug("getXmlFile="+xmlFilePath);
		File xmlFile = new File(xmlFilePath);
		try {
			if (xmlFile.exists())
				return xmlFile;
		} catch (Exception e) {
			logger.error("Exception in getXmlFile method "+ e);
			e.printStackTrace();
		}
		return xmlFile;
	}

	
/**
 * This method is for preparing configuration
 */
	@SuppressWarnings("unchecked")
	public void prepareConfiguration(String xmlFilePath) {
		if(xmlFilePath==null) {
			logger.error("xmlFilePath is not available");
			throw new RuntimeException("xmlFilePath is not available");
		}
		File file = getXmlFile(xmlFilePath);
		if(file==null) {
			logger.error("file is empty");
			throw new RuntimeException("file is empty");
		}
		try {
			SAXBuilder builder = new SAXBuilder();
			Document document = builder.build(file);
			if(document==null) {
				logger.error("document is empty");
				throw new RuntimeException("document is empty");
			}
			
			Element rootNode = document.getRootElement();
			if(rootNode==null) {
				logger.error("rootNode is not available");
				throw new RuntimeException("rootNode is not available");
			}
			List<Element> elements = rootNode.getChildren();
			if(elements==null) {
				logger.error("elements are not available");
				throw new RuntimeException("elements are not available");
			}

			List<Map<String, List<Map<String, List<String>>>>> elementsList = new ArrayList<>();

			for (Element element : elements) {
				List<Element> children = element.getChildren();
				if(children==null) {
					logger.error("children  not available");
					throw new RuntimeException("children  not available");
				}
				HashMap<String, List<String>> childMap = null;
				List<Map<String, List<String>>> childList = new ArrayList<>();

				for (Element child : children) {

					childMap = new HashMap<>();

					List<Element> subChildrens = child.getChildren();
					if(subChildrens==null) {
						logger.error("subChildrens  not available");
						throw new RuntimeException("subChildrens  not available");
					}
					for (Element subChildren : subChildrens) {
						List<String> childrenList = new ArrayList<>();

						if (!"".equals(subChildren.getText().trim()))
							childrenList.add(subChildren.getText().trim());

						if (!subChildren.getChildren().isEmpty()) {
							List<Element> subChilds = subChildren.getChildren();

							for (Element subChild : subChilds) {
								childrenList.add(subChild.getText().trim());
							}
						}
						childMap.put(subChildren.getName(), childrenList);
					}
					childList.add(childMap);
				}

				Map<String, List<Map<String, List<String>>>> map = new HashMap<>();
				map.put(element.getName().trim(), childList);
				elementsList.add(map);
			}
			configurationMap.put(rootNode.getName(), elementsList);
			logger.info("configurationMap while preparing===" + configurationMap.keySet());
		} catch (Exception e) {
			logger.error("Exception in prepareConfiguration method "+ e);
			e.printStackTrace();
			if (logger.isDebugEnabled()) {
				logger.debug("prepareConfiguration(String) - {}", "prepareConfiguration :: " + e); //$NON-NLS-1$ //$NON-NLS-2$
			}
		}
	}

	/**
	 * This method is for getting server configurations
	 */
	public String getServerConfiguration(String configurationName) {
		logger.debug("getServerConfiguration:configurationName="+configurationName);
		// configurationName="email";
		return this.getRecord(MessagesConstant.CONFIGURATIONS, MessagesConstant.NAME, configurationName);
	}

	private String getRecord(String parentElement, String childElement, String childValue) {
		logger.debug("getRecord:parentElement="+parentElement+"\t childElement"+childElement+"\t childValue"+childValue);
		logger.info(
				"parentElement==" + parentElement + "\tchildElement=" + childElement + "\tchildValue=" + childValue);
		HashMap<String, List<String>> recordMap = new HashMap<>();
		configurationMap.forEach((root, elementsList) -> {
			elementsList.forEach(element -> {
				if (element.containsKey(parentElement)) {
					element.forEach((child, subchildList) -> {
						subchildList.forEach(subchild -> {
							List<String> textList = subchild.get(childElement);
							textList.forEach(text -> {
								if (text.trim().equals(childValue.trim())) {
									logger.info("subchild=" + subchild);
									recordMap.putAll(subchild);
								}
							});

						});
					});
				}
			});
		});
		logger.info("recordMap keys==" + recordMap.keySet() + "\trecordMap values=" + recordMap.values());
		return getJsonOfObject(recordMap);
	}

	/**
	 * This method is for getting all records
	 * @author Brainwave Consulting
	 * @since Dec 25, 2018
	 * @param parentElement
	 * @return
	 */
	private String getAllRecords(String parentElement) {
		if(parentElement==null) {
			logger.error("parentElement  not available");
			throw new RuntimeException("parentElement  not available");
		}
		logger.debug("getAllRecords:parentElement="+parentElement);
		ArrayList<Map<String, List<String>>> recordsMap = new ArrayList<>();
		configurationMap.forEach((root, elementsList) -> {
			elementsList.forEach(element -> {
				if (element.containsKey(parentElement)) {
					element.forEach((child, subchildList) -> {
						recordsMap.addAll(subchildList);
					});
				}
			});
		});
		return getJsonOfObject(recordsMap);
	}

	/**
	 * This method is for getting JSON object
	 * @author Brainwave Consulting
	 * @since Dec 25, 2018
	 * @param Object
	 * @return
	 */
	private String getJsonOfObject(Object Object) {
		logger.debug("getJsonOfObject:Object="+Object);
		ObjectMapper objectMapper = new ObjectMapper();
		String json = "";
		try {
			json = objectMapper.writeValueAsString(Object);
		} catch (Exception e) {
			logger.error("Exception in getJsonOfObject method "+ e);
			e.printStackTrace();
			if (logger.isDebugEnabled()) {
				logger.debug("getJsonOfObject(Object) - {}", "Exception in getJsonOfObject method... :: " + e); //$NON-NLS-1$ //$NON-NLS-2$
			}
		}
		return json;
	}

}
