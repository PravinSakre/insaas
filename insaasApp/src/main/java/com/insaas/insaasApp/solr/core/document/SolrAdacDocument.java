/**
 * 
 */
package com.insaas.insaasApp.solr.core.document;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.solr.core.mapping.Indexed;
import org.springframework.data.solr.core.mapping.SolrDocument;

/**
 * @author User
 *
 */
@SolrDocument(collection = "adac")
public class SolrAdacDocument {
	@Id
    @Indexed(name = "id", type = "string")
    private String id;
 
    @Indexed(name = "Aspect", type = "text_general")
    private List<String> aspect;
    
    @Indexed(name = "ADJ_list", type = "text_general")
    private String ADJ_list;

    @Indexed(name = "AdjustedRating", type = "pdoubles")
    private String AdjustedRating;
    
    @Indexed(name = "Adjusted_Rating", type = "pdoubles")
    private String Adjusted_Rating;
    
    @Indexed(name = "AmazonRating", type = "pdoubles")
    private String AmazonRating;
    
    @Indexed(name = "AverageRating", type = "pdoubles")
    private String AverageRating;
    
    @Indexed(name = "Company", type = "text_general")
    private String Company;
    
    @Indexed(name = "date", type = "pdate")
    private String date;
    
    @Indexed(name = "DrRating", type = "text_general")
    private String DrRating;
    
    @Indexed(name = "Feedback", type = "text_general")
    private String feedback;
    
    @Indexed(name = "FeedbackHashValue", type = "text_general")
    private String FeedbackHashValue;
    
    @Indexed(name = "Gender", type = "text_general")
    private String Gender;
    
    @Indexed(name = "Normal_Rating", type = "plongs")
    private String Normal_Rating;
    
    @Indexed(name = "Product", type = "text_general")
    private String Product;
    
    @Indexed(name = "Rating", type = "pdoubles")
    private String Rating;
    
    @Indexed(name = "Sentiment", type = "text_general")
    private String sentiment;
    
    @Indexed(name = "Source", type = "text_general")
    private String Source;
    
    @Indexed(name = "UserId", type = "plongs")
    private String UserId;
    
    @Indexed(name = "url", type = "text_general")
    private String url;

	@Indexed(name = "Modified", type = "text_general")
    private String modified;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<String> getAspect() {
		return aspect;
	}

	public void setAspect(List<String> aspect) {
		this.aspect = aspect;
	}

	public String getADJ_list() {
		return ADJ_list;
	}

	public void setADJ_list(String aDJ_list) {
		ADJ_list = aDJ_list;
	}

	public String getAdjustedRating() {
		return AdjustedRating;
	}

	public void setAdjustedRating(String adjustedRating) {
		AdjustedRating = adjustedRating;
	}

	public String getAdjusted_Rating() {
		return Adjusted_Rating;
	}

	public void setAdjusted_Rating(String adjusted_Rating) {
		Adjusted_Rating = adjusted_Rating;
	}

	public String getAmazonRating() {
		return AmazonRating;
	}

	public void setAmazonRating(String amazonRating) {
		AmazonRating = amazonRating;
	}

	public String getAverageRating() {
		return AverageRating;
	}

	public void setAverageRating(String averageRating) {
		AverageRating = averageRating;
	}

	public String getCompany() {
		return Company;
	}

	public void setCompany(String company) {
		Company = company;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getDrRating() {
		return DrRating;
	}

	public void setDrRating(String drRating) {
		DrRating = drRating;
	}

	public String getFeedback() {
		return feedback;
	}

	public void setFeedback(String feedback) {
		this.feedback = feedback;
	}

	public String getFeedbackHashValue() {
		return FeedbackHashValue;
	}

	public void setFeedbackHashValue(String feedbackHashValue) {
		FeedbackHashValue = feedbackHashValue;
	}

	public String getGender() {
		return Gender;
	}

	public void setGender(String gender) {
		Gender = gender;
	}

	public String getNormal_Rating() {
		return Normal_Rating;
	}

	public void setNormal_Rating(String normal_Rating) {
		Normal_Rating = normal_Rating;
	}

	public String getProduct() {
		return Product;
	}

	public void setProduct(String product) {
		Product = product;
	}

	public String getRating() {
		return Rating;
	}

	public void setRating(String rating) {
		Rating = rating;
	}

	public String getSentiment() {
		return sentiment;
	}

	public void setSentiment(String sentiment) {
		this.sentiment = sentiment;
	}

	public String getSource() {
		return Source;
	}

	public void setSource(String source) {
		Source = source;
	}

	public String getUserId() {
		return UserId;
	}

	public void setUserId(String userId) {
		UserId = userId;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getModified() {
		return modified;
	}

	public void setModified(String modified) {
		this.modified = modified;
	}

	@Override
	public String toString() {
		return "SolrAdacDocument [ADJ_list=" + ADJ_list + ", AdjustedRating=" + AdjustedRating + ", Adjusted_Rating="
				+ Adjusted_Rating + ", AmazonRating=" + AmazonRating + ", AverageRating=" + AverageRating + ", Company="
				+ Company + ", DrRating=" + DrRating + ", FeedbackHashValue=" + FeedbackHashValue + ", Gender=" + Gender
				+ ", Normal_Rating=" + Normal_Rating + ", Product=" + Product + ", Rating=" + Rating + ", Source="
				+ Source + ", UserId=" + UserId + ", aspect=" + aspect + ", date=" + date + ", feedback=" + feedback
				+ ", id=" + id + ", modified=" + modified + ", sentiment=" + sentiment + ", url=" + url + "]";
	}

	
}
