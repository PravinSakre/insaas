//package com.insaas.insaasApp.excecutor;
//
//
//import org.apache.log4j.Logger;
//
//import com.insaas.insaasApp.helper.GetPropertyValues;
//import com.insaas.insaasApp.indexing.DocFileIndexer;
//import com.insaas.insaasApp.indexing.LegacySolrFileIndexer;
//import com.insaas.insaasApp.indexing.TikaFileIndexer;
//import com.insaas.insaasApp.indexing.ZipFileIndexer;
//import com.insaas.insaasApp.model.ExclusionParameters;
//import com.insaas.insaasApp.model.IndexingMetrics;
//
//import java.io.File;
//import java.util.Random;
//import java.util.concurrent.ExecutorService;
//import java.util.concurrent.Executors;
//import java.util.concurrent.TimeUnit;
//import java.util.zip.ZipEntry;
//
///**
// * Created with IntelliJ IDEA.
// * User: Suryapc
// * Date: 2/13/15
// * Time: 4:53 PM
// * To change this template use File | Settings | File Templates.
// */
//public abstract class SolrExecutor implements SolrExecutable {
//    private static Logger Log = Logger.getLogger(SolrExecutor.class);
//    protected ExecutorService executor;
//    private final int numberOfThreads;
//    private long waitTimeToComplete = 0;
//
//    public SolrExecutor(int numberOfThreads) {
//        this.numberOfThreads = numberOfThreads;
//    }
//
//    public long getWaitTimeToComplete() {
//        return waitTimeToComplete;
//    }
//
//    public void setWaitTimeToComplete(long waitTimeToComplete) {
//        this.waitTimeToComplete =this.waitTimeToComplete  + waitTimeToComplete;
//    }
//
//    public void setUpExecutor() {
//        if(numberOfThreads > 1)
//            executor = Executors.newFixedThreadPool(numberOfThreads);
//        else
//            executor = Executors.newSingleThreadExecutor();
//    }
//
//    public void index(File file,Object obj,String coreName, ExclusionParameters exclusionParameters) {
//        //create SolrIndexable
//        IndexingMetrics.SINGLETON.incrementFilesInQueue();
//        Runnable r;
//      //  if(null==obj)
//         // r = createTikaSolrIndexer(file,coreName, exclusionParameters);   // for Legacy call createSolrIndexer() method
//        //else
//        //  r= createSolrZipIndexer(file,obj,coreName, exclusionParameters);
//        //push it to executor
//       // pushToExecutor(r);
//    }
//
//    /*private Runnable createSolrZipIndexer(File file,Object obj,String coreName, ExclusionParameters exclusionParameters) {
//        return new ZipFileIndexer(file, 0, coreName, exclusionParameters, (ZipEntry)obj);
////        return new ZipLegacySolrFileIndexer(file,obj,coreName, exclusionParameters);
//    }
//*/
//   /* private Runnable createSolrIndexer(File file,String coreName, ExclusionParameters exclusionParameters) {
//        //tika or legacy
//        int wait=new Random().nextInt(500);
//        setWaitTimeToComplete(wait);
//        return new LegacySolrFileIndexer(file,wait,coreName, exclusionParameters);
//    }
//*/
//
//   /* private Runnable createTikaSolrIndexer(File file,String coreName, ExclusionParameters exclusionParameters) {
//        //tika or legacy
//        int wait=new Random().nextInt(500);
//        setWaitTimeToComplete(wait);
//        if(new GetPropertyValues().getBooleanProperty("files.split", false))
//            return new DocFileIndexer(file,wait,coreName, exclusionParameters);
//        return new TikaFileIndexer(file,wait,coreName, exclusionParameters);
//    }*/
//
//    protected void pushToExecutor(Runnable r) {
//        executor.execute(r);
//    }
//
//    public void shutdownExecutor() {
//        try {
//            Log.info("before shutdown call");
//            executor.shutdown();
//            Log.info("after shutdown call");
//            Thread.sleep(getWaitTimeToComplete());
//            executor.awaitTermination
//                    (getWaitTimeToComplete(), TimeUnit.MILLISECONDS);
//            Log.info("after awaitTermination call");
//        } catch (InterruptedException ignored) {
//            //todo put in logs
//        }
//    }
//}
