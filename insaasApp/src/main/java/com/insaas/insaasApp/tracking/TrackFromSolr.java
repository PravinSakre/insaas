//package com.insaas.insaasApp.tracking;
//
//
//import org.apache.commons.lang.StringUtils;
//import org.apache.solr.client.solrj.SolrQuery;
//import org.apache.solr.client.solrj.SolrClient;
//import org.apache.solr.client.solrj.response.QueryResponse;
//import org.apache.solr.client.solrj.util.ClientUtils;
//import org.apache.solr.common.SolrDocument;
//import org.apache.solr.common.SolrDocumentList;
//
//import com.insaas.insaasApp.dao.SolrChannel;
//import com.insaas.insaasApp.helper.UidHelper;
//
//import java.io.File;
//
///**
// * Created with IntelliJ IDEA.
// * User: Suryapc
// * Date: 3/11/15
// * Time: 5:43 PM
// * To change this template use File | Settings | File Templates.
// */
//public class TrackFromSolr implements Trackable {
//    public boolean hasBeenIndexed(File file,String coreName) {
//        //search for the file in solr using full file name as id
//        try{
//            //check if the file is already index
//            SolrClient localSolr = SolrChannel.getSolrServer(coreName);
//            SolrQuery solrQuery=new SolrQuery();
//            solrQuery.setStart(0);
//            solrQuery.setQuery("*:*");
//            solrQuery.addFilterQuery("hash_id:"+ getHashIdFromFile(file) +"");
//            String generatedId=file.getAbsolutePath();
//            QueryResponse queryResponse=localSolr.query(solrQuery);
//            SolrDocumentList solrDocumentList=queryResponse.getResults();
//            if(solrDocumentList.size()==1){
//                SolrDocument solrDocument = solrDocumentList.get(0);
//                String idFromIndex=(String) solrDocument.getFieldValue("id");
//                if(StringUtils.equals(idFromIndex, generatedId)){
//                    return true;
//                }
//            }else{
//             return false;
//            }
//
//            /*SolrClient solrServer = SolrChannel.getSolrServer();
//            SolrQuery query = new SolrQuery();
//            query.setQuery("a");
//            query.setParam("id","E:\\C-drive\\sample\\abc");
//            query.setRows(1);
//            QueryResponse response = solrServer.query(query);
//            Object docList = response.getResults();
//            solrServer.shutdown();*/
//        }catch (Exception e){
//            throw new RuntimeException("Exception id search in solr for "+file.getAbsolutePath()+e.getMessage(),e);
//        }
//        //if found return false
//        return false;
//    }
//
//    private String getHashIdFromFile(File file) {
//        return ClientUtils.escapeQueryChars(UidHelper.getHashIdFromString(file.getAbsolutePath()));
//    }
//}
