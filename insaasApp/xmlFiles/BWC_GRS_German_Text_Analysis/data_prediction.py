'''
Import following files
'''
import os
import nltk
import spacy
import pycrfsuite
import numpy as np
import pandas as pd
from langdetect import detect
from sklearn_crfsuite import metrics
from sklearn.model_selection import KFold
from sklearn.metrics import classification_report
from sklearn.model_selection import train_test_split


'''
Functions
'''
def read_csv(ip_path):
    print("\n# Read input raw file * * *")
    df = pd.read_csv(ip_path)
    print('Raw Dataframe:', df.shape)
    return df

def remove_english_feed(df):
    print("\n# Remove english feedbacks * * *")
    lang = []
    k = 1
    for i in df['Name_Feedback']:
        i = i.encode('utf-8')
        j = detect(str(i))
        lang.append(j)
        k += 1
    df['lang'] = lang
    i = df[df['lang']!='de'].index
    df_new = df.drop(df.index[i])
    df_new.reset_index(drop=True, inplace=True)
    df_new = df_new.drop(['lang'], axis=1)
    print("No of Rows:", df_new.shape[0])
    print("No of Columns:", df_new.shape[1])
    return df_new

def preprocess(df_new):
    # training data preparation ------------
    print('\n# Prepare Training data * * *')
    # replace 0 with nan
    df = df_new.replace('0', np.nan)
    df_new = df
    print("No of Rows:", df_new.shape[0])
    print("No of Columns:", df_new.shape[1])
    return df

def review_extraction(df):
    # Feature-Target split
    print('\n# Feedback Extraction * * * ')
    X = df['Name_Feedback']
    # lower case
    lower_feed = []
    for i in X:
        lower_feed.append(i.lower())
    X = lower_feed
    print("No of Rows:", len(X))
    return X

def Tagging(data):
    def POS_tag(data):
        tag = []
        for tokens in data:
            tag.append(nltk.pos_tag(tokens))
        return tag

    def IOB_Tagging(data):
        # nlp = spacy.load("de")
        labelled_data = []
        token_list = []
        for feed in data:
            feed = feed.lower()
            tokens = feed.split()
            tags = ['O'] * len(tokens)
            labelled_data.append((tokens, tags))
            token_list.append(tokens)
        return labelled_data, token_list

    # IOB Tagging
    print("\n\n# IOB Tagging Train * * *")
    labelled_data, token_list = IOB_Tagging(data)
    print("Labelled data:", labelled_data[0])
    print("Tokens:", token_list[0])

    # POS Tagging
    print("\n\n# POS Tagging Train * * *")
    pos_tag = POS_tag(token_list)
    print("POS tag:", pos_tag[0])

    # prepare data for training (tok, pos, label)
    print("\n# Processed Data * * * ")
    record_len = len(labelled_data)
    corpus_tag_label_list = []
    for i in range(record_len):
        sent_tag_label_list = []
        for idx, (tok, tag) in enumerate(pos_tag[i]):
            sent_tag_label_list.append((tok, tag, labelled_data[i][1][idx]))
        corpus_tag_label_list.append(sent_tag_label_list)
    print("Token-Tag-Label:", corpus_tag_label_list[0])
    return corpus_tag_label_list, token_list

def feature_generation(data):
    print("\n# Feature Generation * * *")
    def word2features(sent, i):
        word = sent[i][0]
        postag = sent[i][1]
        features = {
            'bias': 1.0,
            'word.lower()': word.lower(),
            'word[-3:]': word[-3:],
            'word[-2:]': word[-2:],
            'word.isupper()': word.isupper(),
            'word.istitle()': word.istitle(),
            'word.isdigit()': word.isdigit(),
            'postag': postag,
            'postag[:2]': postag[:2],
        }
        if i > 0:
            word1 = sent[i-1][0]
            postag1 = sent[i-1][1]
            features.update({
                '-1:word.lower()': word1.lower(),
                '-1:word.istitle()': word1.istitle(),
                '-1:word.isupper()': word1.isupper(),
                '-1:postag': postag1,
                '-1:postag[:2]': postag1[:2],
            })
        else:
            features['BOS'] = True

        if i < len(sent)-1:
            word1 = sent[i+1][0]
            postag1 = sent[i+1][1]
            features.update({
                '+1:word.lower()': word1.lower(),
                '+1:word.istitle()': word1.istitle(),
                '+1:word.isupper()': word1.isupper(),
                '+1:postag': postag1,
                '+1:postag[:2]': postag1[:2],
            })
        else:
            features['EOS'] = True
        return features

    def sent2features(sent):
        return [word2features(sent, i) for i in range(len(sent))]

    def sent2labels(sent):
        return [label for token, postag, label in sent]

    def sent2tokens(sent):
        return [token for token, postag, label in sent]

    X = [sent2features(s) for s in data]
    y = [sent2labels(s) for s in data]
    return X, y

def model(X, y):
    print("\n# Data Prediction Model * * *")
    def test_model(X_test):
        tagger = pycrfsuite.Tagger()
        tagger.open('pycrf.model')
        y_pred = [tagger.tag(xseq) for xseq in X_test]
        return y_pred

    # train-test fold
    test_pred = test_model(X)
    # kfold split
    # print("\n# K-Fold * * * ")
    # kfold(np.array(X), np.array(y))
    return test_pred

def stopword_remove(aspect_list):
    # nlp = spacy.load('de')
    asp_total = []
    for asp in aspect_list:
        asp_sub = []
        for word in asp:
            token = nlp(word)
            for tok in token:
                if tok.is_stop == False:
                    asp_sub.append(tok)
        asp_total.append(asp_sub)
    return asp_total

def extract_aspects(tokens, tags, df):
    print("\n# Aspect Extraction * * * ")
    aspect_list = []
    for i in range(len(tags)):
        sent_list = []
        for j in range(len(tags[i])):
            if tags[i][j] == 'B':
                sent_list.append(tokens[i][j])
        aspect_list.append(sent_list)

    # jj_aspect_list = []
    # for lst in aspect_list:
    #     tagged = nltk.pos_tag(lst)
    #     print(tagged)
    #     # print([(word, tag) for word, tag in tagged if tag in ('JJ')])
    #     asp_i = []
    #     for word, tag in tagged:
    #         if tag in ('JJ'):
    #             print(word)
    #             asp_i.append(word)
    #     jj_aspect_list.append(asp_i)
    # print(jj_aspect_list)

    # remove stopword
    # pred_aspect = stopword_remove(aspect_list) # change for nlp
    df['New_Aspect'] = aspect_list
    return df

def split_list_to_columns(df):
    print("\n# Split List to Columns * * * ")
    str_strip = []
    for i in df['New_Aspect']:
        i = str(i)
        j = i.lstrip('[')
        str_strip.append(j.rstrip(']'))
    s = pd.Series(str_strip)
    sp = s.str.split(pat = ',' , expand=True)
    for i in range(sp.shape[1]):
        name = os.path.join('Aspect_' + str(i))
        # print(i, name)
        df[name] = sp[i]
    return df

'''
main function
'''
def main():
    # provide path and file name
    file_path = '\home\xmlFiles\BWC_GRS_German_Text_Analysis\TrustPilotDataFinalCSV1'

    ip_path = os.path.join(file_path + '.csv')
    op_path = os.path.join(file_path + '_prediction' + '.csv')

    # read data
    df = read_csv(ip_path)
    print(df.columns)

    df = remove_english_feed(df)

    df = preprocess(df)

    features = review_extraction(df)

    corpus, tokens = Tagging(features)

    X, y = feature_generation(corpus)

    y = model(X, y)

    data = extract_aspects(tokens, y, df)

    df = split_list_to_columns(data)

    df.to_csv(op_path, index = False)
    print("\n# Prediction file generated..")
    return

if __name__ == '__main__':
    main()
                                                                                                                                                                                                                                                                                                                                                                                                                                             
