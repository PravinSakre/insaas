# README #
Insaas is a solution to analyze customer review and feedbacks regarding product or services and provides actionable dashboard which delivers consistent insights on the performance of products and services.
It has solution with unique machine intelligence, developed specifically for this task. it has used deep learning methods and supervised learning to analyze customer reviews coming from multiple sources to generate critical inputs for marketing purposes, market research and product development.

It contains 3 folder structures.
insaasApp - it contains java source code.
insaasEditUI - it contains angular2(i.e. angular 7) project files.
insaasUI - it contains angular1(i.e angular1.6) project files.

Prerequisites
 System must have this softwares.
 - Angular 1.6
 - Angular2 (this for edit UI)
 - Orcale Java8
 - Mariadb (database)
 - STS (Spring tool suit)
 - VS-Code or any Frond end editor who can run angular applications.
 
Installation
 insaasApp - you must have eclipse(with Springboot plugins) or STS(Spring tool suit) editor tool and then import & run project as Spring boot application.
 insaasEditUI - you must have VS code or any UI tool for running angular application. import and run following command using console.
                # npm start
				(note : this application is angular1(i.e 1.6) version.
 insaasUI -  you must have VS code or any UI tool for running angular application. import and run following command using console.
                # ng serve
				(note : this application is angular2(i.e 7) version.
 * Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions
