import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Constant } from '../constant';

@Injectable({
  providedIn: 'root'
})
export class SearchService {
  site: string = Constant.site;
  queryParams: string = '';

  constructor(private http: HttpClient) { }

  ngOnInit() {
  }

  getResult(): Observable<any>{
    // facet.field=Aspect&facet=on&facet.limit=10&q=*%3A*&rows=0&start=0
    let url1 = "http://localhost:8983/solr/adac/select?facet.field=Aspect&facet=on&facet.limit=10&q=*%3A*&rows=0&start=0";
    return this.http.get(url1);
  }

  getSolrResult(solrSearchFields): Observable<any> {
    const formData: FormData = new FormData();
    formData.append("solrSearchFields", solrSearchFields); 
    let url = this.site;
    url += Constant.solrSearch + "?solrSearchFields="+ encodeURIComponent(JSON.stringify(solrSearchFields));
    return this.http.get(url);
  }

  updateDocument(documents: any, accessToken: string): Observable<any> {
    // alert(JSON.stringify(documents))
    let url = this.site;
    url += Constant.updateDocument;
    const formData: FormData = new FormData();
    formData.append("accessToken", accessToken);
    formData.append('documents', JSON.stringify(documents));
    return this.http.post(url, formData);
  }

  massUpdateDocument(solrSearchFields, accessToken: string): Observable<any> {
    let url = this.site;
    url += Constant.massDocumentUpdate;
    const formData: FormData = new FormData();
    formData.append("solrSearchFields", JSON.stringify(solrSearchFields)); 
    formData.append("accessToken", accessToken); 
    // url += Constant.massDocumentUpdate;
    // alert(url);
    return this.http.post(url, formData);
  }


  getSearchFeedback(searchTerm: string, accessToken: string): Observable<any> {
    let url = this.site;
    url += Constant.searchFeedback + "?accessToken=" + accessToken + "&searchTerm=" + searchTerm;
    return this.http.get(url);
  }

}
