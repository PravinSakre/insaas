import { Component, OnInit } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { SearchService } from '../search/search.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})

export class SearchComponent {
  rowIndex = 1;
  previousPage = 0;
  firstPage = 1;
  secondPage = 2;
  thirdPage = 3;
  nextPage = 3;
  // lastPage = this.totalCount;
  lastFilterRecord = { filterValue: "", filterBy: "" };
  updatedRowIndex = 0;
  selectedColor = "black";
  neutralColorCode = "#ffc107";
  negativeColorCode = "red";
  positiveColorCode = "green";
  selectedListOfMassUpdateIds = [];
  MassUpdateIds = [];
  massUpdateCompleteFlag = true;
  accessToken = "";
  solrSearchQuery = "*:*";
  successId = true;
  errorId = true;
  responseMsg = "";
  filterAspectList = [];

  solrSearchFields = {
    accessToken: '',
    aspect: [],
    search: '',
    rows: 10,
    start: 0,
    updateOrDeleteflag:'',
    updatedValue:'',
    updatedDocId:[]
  }

  reponseDisplaySuccessMsg = "Records Updated Successfully";
  reponseDisplayErrorMsg = "Records Updated Failed!";
  reponseDisplaySessionExpireMsg = "Session has Expired! Login again";

  listSenti = [{
    value: "neutral",
    fontAwesom: "fa-meh-o",
    sentiment: "&#xf11a;",
    color: this.neutralColorCode

  }, {
    value: "negative",
    fontAwesom: "fa-frown-o",
    sentiment: "&#xf119;",
    color: this.negativeColorCode

  },
  {
    value: "positive",
    fontAwesom: "fa-smile-o",
    sentiment: "&#xf118;",
    color: this.positiveColorCode
  }];

  updatedAspect = "";
  successMsg = true;
  errorMsg = false;
  modifiedDocumentList = [];
  modifiedSentimentList = [];
  documentListForSolrUpdate = [];
  document = {
    documentId: '',
    srNo: '',
    review: '',
    aspect: [],
    sentiment: '',
    rating: ''
  };

  refreshPage() {
    try {
      window.location.reload();
      // this.massUpdateCompleteFlag = true;
      // this.successId = true;
      // this.errorId = true;
      // this.searchTerm = "";
      // this.updatedAspect = ""
      // this.selectedListOfMassUpdateIds = [];
      // this.MassUpdateIds = [];
      // this.modifiedDocumentList = [];
      // this.modifiedSentimentList = [];
      // this.documentListForSolrUpdate = [];
      // this.collection.data = [];
      // this.selectedAspect = "";
      // // this.filterAspectList = [];
      // this.solrSearchFields=null;
      // document.getElementById("SearchFeedbackInputBoxId").innerHTML = "";
      // this.getSolrResult();
    } catch (e) {
      console.log(e);
    }
  }

  expandRow(edit_aspect_div_id, edit_pensil_id, edit_close_id, aspect_span_id, reviewRecord, index) {
    if (!this.massUpdateCompleteFlag) {
      alert("Earilier update is running please wait for complete");
      return;
    }
    this.collection.data.forEach(element => {
      if (element.edit_pensil_id === edit_pensil_id) {
        document.getElementById(element.edit_aspect_div_id).hidden = false;
        document.getElementById(element.edit_pensil_id).hidden = true;
        document.getElementById(element.aspect_span_id).hidden = true;
        document.getElementById(element.edit_close_id).hidden = false;
        this.getRowRecord(reviewRecord, index);
      } else {
        this.setElementHiddenTrueProperty(element.edit_aspect_div_id, element.edit_close_id);
        this.setElementHiddenFalseProperty(element.edit_pensil_id, element.aspect_span_id);

        // document.getElementById(element.edit_aspect_div_id).hidden = true;
        // document.getElementById(element.edit_pensil_id).hidden = false;
        // document.getElementById(element.edit_close_id).hidden = true;
        // document.getElementById(element.aspect_span_id).hidden = false;
      }
    });
  }

  setElementHiddenTrueProperty(edit_aspect_div_id, edit_close_id) {
    if (document.getElementById(edit_aspect_div_id) != undefined) {
      document.getElementById(edit_aspect_div_id).hidden = true;
    }
    if (document.getElementById(edit_close_id) != undefined) {
      document.getElementById(edit_close_id).hidden = true;
    }
  }

  setElementHiddenFalseProperty(edit_pensil_id, aspect_span_id) {
    if (document.getElementById(edit_pensil_id) != undefined) {
      document.getElementById(edit_pensil_id).hidden = false;
    }
    if (document.getElementById(aspect_span_id) != undefined) {
      document.getElementById(aspect_span_id).hidden = false;
    }
  }

  collapseRow(id, edit_aspect_div_id, edit_pensil_id, edit_close_id, aspect_span_id) {
    document.getElementById(edit_aspect_div_id).hidden = true;
    document.getElementById(edit_pensil_id).hidden = false;
    document.getElementById(edit_close_id).hidden = true;
    document.getElementById(aspect_span_id).hidden = false;
    this.setSentimentIcon(id);
  }

  collapseAllRowAfterSave() {
    this.collection.data.forEach(element => {
      if (document.getElementById(element.edit_aspect_div_id) != undefined) {
        document.getElementById(element.edit_aspect_div_id).hidden = true;
      }
      if (document.getElementById(element.edit_pensil_id) != undefined) {
        document.getElementById(element.edit_pensil_id).hidden = false;
      }
      if (document.getElementById(element.edit_close_id) != undefined) {
        document.getElementById(element.edit_close_id).hidden = true;
      }
      if (document.getElementById(element.aspect_span_id) != undefined) {
        document.getElementById(element.aspect_span_id).hidden = false;
      }
    });
  }

  saveRecord() {
    try {
      this.searchService.updateDocument(this.prepareSolrUpdateDocuments(), this.accessToken).
        subscribe(
          (response: any) => {
            this.responseMsg = this.reponseDisplaySuccessMsg;
            console.log("response :: " + response);
            this.successId = false;
            // this.reloadAfterSave();
            window.setTimeout(function () {
              this.successId = true;
            }, 5000);
            // this.errorId = true;
          }, (error: HttpErrorResponse) => {
            // this.msgInterval("errorMsgFlag");
            console.error('login error :: ' + error);
            switch (error.status) {
              case 401:
                this.responseMsg = this.reponseDisplaySessionExpireMsg;
                break;
              case 420:
                this.responseMsg = this.reponseDisplaySessionExpireMsg;
                break;
              case 200:
                this.responseMsg = this.reponseDisplaySuccessMsg;
                this.reloadAfterSave();
                break;
              default:
                this.responseMsg = "";
            }
            this.errorId = false;
            document.getElementById("errorResponseId").hidden = false;
            document.getElementById("errorResponseId1").hidden = false;

            window.setTimeout(function () {
              document.getElementById("errorResponseId").hidden = true;
              document.getElementById("errorResponseId1").hidden = true;
              this.errorId = true;
            }, 5000);
          }
        );
    } catch (e) {
      // this.msgInterval("errorMsg");
      console.log("saveRecord fails :: " + e);
    }
    this.collapseAllRowAfterSave();
  }

  setSentimentIcon(review) {
    let index = 0;
    this.collection.data.forEach(element => {
      if (element.documentId == review.documentId) {
        this.collection.data[index].modified = "yes";
      }
      index++;
    });
  }

  onSelectSentiment(review, event) {
    if (!this.massUpdateCompleteFlag) {
      alert("Earilier update is running please wait for complete");
      return;
    }
    let isDocumentFound = false;
    review.sentiment = this.listSenti[event.target.selectedIndex].value;

    if (this.modifiedDocumentList.length == 0) {
      this.modifiedDocumentList.push(this.preparedocument(review));
      isDocumentFound = true;
    } else {
      let index = 0;
      this.modifiedDocumentList.forEach(element => {
        if (element.documentId === review.documentId) {
          this.modifiedDocumentList[index].sentiment = event.target.value;
          isDocumentFound = true;
        }
        index++;
      });
    }

    if (!isDocumentFound) {
      this.modifiedDocumentList.push(this.preparedocument(review));
    }

    // review.sentiment_select_id
    document.getElementById(review.sentiment_select_id).style.color = this.listSenti[event.target.selectedIndex].color;
    this.setSentimentIcon(review);
    // this.setModifiedFlag();
  }

  setModifiedFlag() {
    document.getElementById("modifiedCheckedFlag").hidden = false;
    document.getElementById("modifiedUnCheckedFlag").hidden = true;
  }

  prepareSolrUpdateDocuments() {
    let requestData = {
      "userId": window.sessionStorage.getItem("userId"),
      "data": this.modifiedDocumentList
    }
    this.reinitializeUpdatedList();
    this.modifiedDocumentList = [];
    this.modifiedSentimentList = [];
    return requestData;
  }

  addAspect(reviewRecord) {
    try {
      let value = ((<HTMLInputElement>document.getElementById(reviewRecord.add_aspect_input_box_id))).value;
      let index = 0;
      this.modifiedDocumentList.forEach(element => {
        if (element.documentId === reviewRecord.documentId) {
          if (this.modifiedDocumentList[index].aspect.length <= 9) {
            this.modifiedDocumentList[index].aspect.push(
              {
                updatedIndex: 'updated_' + this.modifiedDocumentList[index].aspect.length,
                value: value,
                parant_toggle: false,
                child_toggle: true,
                aspect_parent_id: 'aspect_parent_id_' + this.modifiedDocumentList[index].aspect.length,
                aspect_child_id: 'aspect_child_id_' + this.modifiedDocumentList[index].aspect.length

              }
            );
            this.updateCollectionDataListForAspect(this.modifiedDocumentList, index, reviewRecord.documentId);
            ((<HTMLInputElement>document.getElementById(reviewRecord.add_aspect_input_box_id))).value = " ";
          } else {
            alert("you can add only 10 Aspects..");
          }
        }
        index++;
      });
      this.setSentimentIcon(reviewRecord);
      // this.setModifiedFlag();
    } catch (e) {
      console.error(e);
    }
  }

  updateCollectionDataListForAspect(list, index, documentId) {
    let aspects = [];
    this.modifiedDocumentList[index].aspect.forEach(element => {
      aspects.push(element.value);
    });
    let rowIndex = 0;
    this.collection.data.forEach(element => {
      if (element.documentId === documentId) {
        this.collection.data[rowIndex].aspect = [];
        this.collection.data[rowIndex].aspect.push(aspects);
      }
      rowIndex++;
    });
  }

  updateAspect(documentId, indexValue, index) {
    let listIndex = 0;
    this.modifiedDocumentList.forEach(element => {
      if (element.documentId == documentId) {
        let updateValue = ((<HTMLInputElement>document.getElementById(indexValue))).value;
        this.modifiedDocumentList[listIndex].aspect[index].value = updateValue;
      }
    });
  }

  removeAspect(reviewRecord, index) {
    let listIndex = 0;
    this.modifiedDocumentList.forEach(element => {
      if (element.documentId == reviewRecord.documentId) {
        this.modifiedDocumentList[listIndex].aspect.splice(index, 1);
        this.updateCollectionDataListForAspect(this.modifiedDocumentList, listIndex, reviewRecord.documentId);
      }
      listIndex++;
    });
    this.setSentimentIcon(reviewRecord);
    // this.document.aspect.splice(index, 1);
    this.successMsg = true;
  }

  reinitializeUpdatedList() {
    this.document = {
      documentId: '',
      srNo: '',
      review: '',
      aspect: [],
      sentiment: '',
      rating: ''
    };
  }

  preparedocument(review) {
    this.reinitializeUpdatedList();
    this.document.documentId = review.documentId;
    this.document.srNo = review.id;
    this.document.review = review.review;
    this.document.sentiment = review.sentiment;
    this.document.rating = review.rating;
    let aspectList = [];
    let index = 0;
    review.aspectList.forEach(element => {
      aspectList.push(
        {
          updatedIndex: 'updated_' + index,
          value: element,
          parant_toggle: false,
          child_toggle: true,
          aspect_parent_id1: 'aspect_parent_id_1' + index,
          aspect_parent_id2: 'aspect_parent_id_2' + index,
          aspect_child_id1: 'aspect_child_id_1' + index,
          aspect_child_id2: 'aspect_child_id_2' + index
        }
      )
      index++;
    });
    this.document.aspect = aspectList;
    return this.document;
  }

  // edit_aspect_div_id
  getRowRecord(review, recordIndex) {
    this.updatedRowIndex = recordIndex;
    let isDocumentFound = false;
    if (this.modifiedDocumentList.length == 0) {
      this.modifiedDocumentList.push(this.preparedocument(review));
      isDocumentFound = true;
    } else {
      let index = 0;
      this.modifiedDocumentList.forEach(element => {
        if (element.documentId === review.documentId) {
          this.document = this.modifiedDocumentList[index];
          isDocumentFound = true;
        }
        index++;
      });
    }

    if (!isDocumentFound) {
      this.modifiedDocumentList.push(this.preparedocument(review));
    }

  }

  reviewDetails = [];
  config: any;
  totalCount = 0;
  totalAspectCount = 0;
  selectedAspect = "";
  searchTerm = "";

  // totalAspectCount = 0;
  collection = { count: this.totalCount, data: [] };
  facetList = [];

  constructor(private route: Router, private searchService: SearchService, private activateRoute: ActivatedRoute) {
    let webSite;
    let token;
    // this.accessToken = window.sessionStorage.getItem("accessToken");
    webSite = this.activateRoute.params.subscribe(params => {
      if (params['userId'] != undefined) {
        token = params['userId'];
        this.accessToken = token;
        window.sessionStorage.setItem("acceesToken", token);
      }
    });
    window.setTimeout(function () {
      // this.route.navigate(['edit']); 
    }, 5000);

    this.config = {
      itemsPerPage: 10,
      currentPage: 1,
      totalItems: this.collection.count
    };
  }

  ngOnInit() {
    document.getElementById("svgReviewLoadingImage").hidden = true;
    this.getSolrResult();
  }

  getAspectResult(event) {
    document.getElementById("svgReviewLoadingImage").hidden = false;
    this.prepareSolrUpdateDocuments();
    this.selectedAspect = "";
    let aspect = "";
    if (event.target === undefined) {
      aspect = event;
    } else {
      aspect = event.target.innerText;
    }
    this.putAspectInFilterAspectList(aspect);
    this.lastFilterRecord = null;
    this.lastFilterRecord = { filterValue: aspect, filterBy: "Aspect" };
    this.selectedAspect = aspect;
    this.updateSolrSearchFields();
  }

  putAspectInFilterAspectList(aspect) {
    let index = 0;
    let isAvailable = false;
    this.filterAspectList.forEach(element => {
      if (element === aspect) {
        isAvailable = true;
      }
      index++;
    });

    if (!isAvailable) {
      this.filterAspectList.push(aspect);
    }
  }

  clearSearch() {
    this.solrSearchQuery = "*:*";
    this.searchTerm = "";
    this.updateSolrSearchFields();
  }

  getSearchFeedback() {
    this.lastFilterRecord = null;
    this.lastFilterRecord = { filterValue: this.searchTerm, filterBy: "FeedbackSearch" };

    if (this.searchTerm === "" || this.searchTerm === undefined) {
      // alert("Search should not be empty");
      return;
    }
    this.solrSearchQuery = this.searchTerm;
    this.updateSolrSearchFields();
  }

  updateSolrSearchFields() {
    this.solrSearchFields.accessToken = this.accessToken;
    this.solrSearchFields.search = this.solrSearchQuery;
    this.putSolrSearchFieldsAspect(this.selectedAspect);
    this.solrSearchFields.start = 0;
    this.getResult(this.solrSearchFields);
  }

  putSolrSearchFieldsAspect(aspect) {
    let isAvailable = false;
    this.solrSearchFields.aspect.forEach(element => {
      if (element === aspect) {
        isAvailable = true;
      }
    });

    if (!isAvailable) {
      this.solrSearchFields.aspect.push(aspect);
    }
  }

  getResult(solrSearchFields) {
    this.collection.data = null;
    this.collection = { count: this.totalCount, data: [] };
    this.searchService.getSolrResult(solrSearchFields).
      subscribe(
        (object: any) => {
          // console.log(JSON.stringify(object))
          let responseObject = JSON.parse(JSON.stringify(object));
          this.prepareAspectList(responseObject);
          let reviewDetailsList = responseObject.response.docs;
          this.totalCount = responseObject.response.numFound;
          this.displayResponseObject(reviewDetailsList);

          // this.collection.data.sort((a, b) => a.modified_no > b.modified_no ? -1 : a.modified_no > b.modified_no ? 1 : 0);
          document.getElementById("svgReviewLoadingImage").hidden = true;
        }, (error: HttpErrorResponse) => {
          console.error(error.status);
          switch (error.status) {
            case 401:
              this.responseMsg = this.reponseDisplaySessionExpireMsg;
              break;
            case 420:
              this.responseMsg = this.reponseDisplaySessionExpireMsg;
              break;
            default:
              this.responseMsg = "";
          }
          this.errorId = false;
          document.getElementById("errorResponseId").hidden = false;
          document.getElementById("errorResponseId1").hidden = false;

          window.setTimeout(function () {
            document.getElementById("errorResponseId").hidden = true;
            document.getElementById("errorResponseId1").hidden = true;
            this.errorId = true;
          }, 5000);
        }

      );
    // alert();
  }


  getSolrResult() {
    this.solrSearchFields.accessToken = this.accessToken;
    this.solrSearchFields.search = this.solrSearchQuery;
    this.solrSearchFields.aspect;
    this.solrSearchFields.start = 0;

    // document.getElementById("svgLoadingImage").hidden=false;
    this.searchService.getSolrResult(this.solrSearchFields).
      subscribe(
        (object: any) => {
          // console.log(JSON.stringify(object))
          let responseObject = JSON.parse(JSON.stringify(object));
          this.prepareAspectList(responseObject);
          document.getElementById("svgLoadingImage").hidden = true;
        }, (error: HttpErrorResponse) => {
          console.error(error.status);
          switch (error.status) {
            case 401:
              this.responseMsg = this.reponseDisplaySessionExpireMsg;
              break;
            case 420:
              this.responseMsg = this.reponseDisplaySessionExpireMsg;
              break;
            default:
              this.responseMsg = "";
          }
          this.errorId = false;
          document.getElementById("errorResponseId").hidden = false;
          document.getElementById("errorResponseId1").hidden = false;

          window.setTimeout(function () {
            document.getElementById("errorResponseId").hidden = true;
            document.getElementById("errorResponseId1").hidden = true;
            this.errorId = true;
          }, 5000);
        }
      );
  }

  prepareAspectList(responseObject: any) {
    this.facetList = [];
    let aspectList = responseObject.facet_counts.facet_fields.Aspect;
    for (let index = 0; index < aspectList.length; index++) {
      this.facetList.push({
        value: aspectList[index],
        count: aspectList[index + 1],
        display: this.selectedAspectDisplay(aspectList[index])
      });
      index++;
    }
  }

  selectedAspectDisplay(aspect) {
    let returnValue = true;
    this.filterAspectList.forEach(element => {
      if (element === aspect) {
        returnValue = false;
      }
    });
    return returnValue;
  }

  removeSelected(aspect) {
    let index = 0;
    this.filterAspectList.forEach(element => {
      if (element === aspect) {
        this.filterAspectList.splice(index, 1);
        this.selectedAspect = "";
      }
      index++;
    });
    index = 0;
    this.solrSearchFields.aspect.forEach(element => {
      if (element === aspect) {
        this.solrSearchFields.aspect.splice(index, 1);
        this.selectedAspect = "";
      }
      index++;
    });
    this.updateSolrSearchFields();
  }

  setSentimentColor(sentiment, sentiment_select_id) {
    switch (sentiment[0]) {
      case 'positive':
        return this.positiveColorCode;
      case 'negative':
        return this.negativeColorCode;
      case 'neutral':
        return this.neutralColorCode;
      default:
        return this.neutralColorCode;
    }
  }

  pageChanged(event) {
    this.config.currentPage = event;
  }
  onClick(id: number) {
    let aspect = this.collection.data[id].aspect;
    console.log(" id : " + id + " aspect : " + aspect);
  }

  massUpdate(flag: any) {
    try {
      let value = this.updatedAspect;
      // (<HTMLInputElement>document.getElementById("massUpdateInput")).value;
      value = value.trim();
      let isAllupdate;
      if (value === undefined || value === "") {
        alert("Please input Aspects");
        return;
      } else {
        isAllupdate = false;
      }
      this.massUpdateCompleteFlag = false;
      // let requestData = {
      //   "userId": window.sessionStorage.getItem("userId"),
      //   "query": this.solrSearchFields,
      //   "aspect": value,
      //   "updateOrDeleteflag": flag
      // }
      this.solrSearchFields.updateOrDeleteflag = flag;
      this.solrSearchFields.updatedValue = value;
      this.solrSearchFields.updatedDocId = this.selectedListOfMassUpdateIds
      // return;
      this.searchService.massUpdateDocument(this.solrSearchFields, this.accessToken).
        subscribe(
          (response: any) => {
            console.log("response :: " + response);
            this.successId = false;
            window.setTimeout(function () {
              this.successId = true;
            }, 5000);
            this.responseMsg = this.reponseDisplaySuccessMsg;

            // this.errorId = true;
            // this.msgInterval("successMsgFlag");
          }, (error: HttpErrorResponse) => {
            console.error('login error :: ' + error);
            switch (error.status) {
              case 401:
                this.responseMsg = this.reponseDisplaySessionExpireMsg;
                break;
              case 420:
                this.responseMsg = this.reponseDisplaySessionExpireMsg;
                break;
              case 200:
                this.responseMsg = this.reponseDisplaySuccessMsg;
                this.reloadAfterSave();
                break;
              default:
                this.responseMsg = "";
            }
            this.massUpdateCompleteFlag = true;
            this.errorId = false;
            document.getElementById("errorResponseId").hidden = false;
            document.getElementById("errorResponseId1").hidden = false;

            window.setTimeout(function () {
              document.getElementById("errorResponseId").hidden = true;
              document.getElementById("errorResponseId1").hidden = true;
              this.errorId = true;
            }, 5000);
          }
        );
      this.MassUpdateIds = [];
      this.selectedListOfMassUpdateIds = [];
      // (<HTMLInputElement>document.getElementById("massUpdateInput")).innerHTML = " ";
    } catch (e) {
      // this.msgInterval("errorMsg");
      console.log("saveRecord fails :: " + e);
    }

  }

  applyToAll() {
    if (!this.massUpdateCompleteFlag) {
      alert("Earilier update is running please wait for complete");
      return;
    }
    this.massUpdate("applyToAll");
  }


  deleteFromAll() {
    if (!this.massUpdateCompleteFlag) {
      alert("Earilier update is running please wait for complete");
      return;
    }
    this.massUpdate("deleteFromAll");
  }

  prepareallMassUpateList() {
    this.collection.data.forEach(element => {
      this.MassUpdateIds.push(element.documentId);
    });
    return this.MassUpdateIds;
  }

  // select checkbox
  selectCheckBox(reviewRecord, event) {
    if (!this.massUpdateCompleteFlag) {
      alert("Earilier update is running please wait for complete");
      return;
    }
    // alert(event.target.checked);
    if (event.target.checked) {
      this.selectedListOfMassUpdateIds.push(reviewRecord.documentId);
    } else {
      let index = 0;
      this.selectedListOfMassUpdateIds.forEach(element => {
        if (element === reviewRecord.documentId) {
          this.selectedListOfMassUpdateIds.splice(index, 1);
        }
        index++;
      });
    }
  }

  applyToSelected() {
    if (!this.massUpdateCompleteFlag) {
      alert("Earilier update is running please wait for complete");
      return;
    }
    this.massUpdate("applyToSelected");
  }


  deleteFromSelected() {
    if (!this.massUpdateCompleteFlag) {
      alert("Earilier update is running please wait for complete");
      return;
    }
    this.massUpdate("deleteFromSelected");
  }

  reloadAfterSave() {
    this.updateSolrSearchFields();
  }

  getAspectRecordAfterSave() {
    if (this.lastFilterRecord.filterBy == " " || this.lastFilterRecord.filterBy == undefined) {
      return;
    } else {
      this.getAspectResult(this.lastFilterRecord.filterValue);
    }
  }

  getSearchRecordAfterSave() {
    if (this.lastFilterRecord.filterBy == " " || this.lastFilterRecord.filterBy == undefined) {
      return;
    } else {
      this.searchTerm = this.lastFilterRecord.filterValue;
      this.getSearchFeedback();
    }
  }


  getPreviousPage() {
    if (this.firstPage != 1) {
      this.firstPage = + this.firstPage - 3;
      this.secondPage = + this.secondPage - 3;
      this.thirdPage = + this.thirdPage - 3;
      this.rowIndex = this.firstPage * 10;
      this.prepareSolrSearchFields(this.firstPage * 10);
    } else {
      this.rowIndex = 1;
      this.prepareSolrSearchFields(0);
    }
  }
  getFirstPage() {
    this.prepareSolrSearchFields(this.firstPage * 10);
  }

  getSecondPage() {
    this.prepareSolrSearchFields(this.secondPage * 10);
  }
  getThirdPage() {
    this.prepareSolrSearchFields(this.thirdPage * 10);
  }

  getLastPage() {
    this.firstPage = + this.thirdPage + 1;
    this.secondPage = + this.thirdPage + 2;
    this.thirdPage = + this.thirdPage + 3;
    this.rowIndex = this.thirdPage * 10;
    this.prepareSolrSearchFields(this.thirdPage * 10);
  }

  prepareSolrSearchFields(pageCount) {
    this.solrSearchFields.accessToken = this.accessToken;
    this.solrSearchFields.search = this.solrSearchQuery;
    this.solrSearchFields.aspect.push(this.selectedAspect);
    // this.solrSearchFields.rows = 10;
    this.solrSearchFields.start = pageCount;
    if (pageCount != 0) {
      this.rowIndex = pageCount;
    }
    this.getResult(this.solrSearchFields);
  }

  displayResponseObject(reviewDetailsList: any) {
    // this.rowIndex++;
    for (var i = 0; i < reviewDetailsList.length; i++) {
      let aspectStr = undefined;
      let sentiment = undefined;
      let modified = undefined;

      aspectStr = new String(reviewDetailsList[i].Aspect);
      sentiment = reviewDetailsList[i].Sentiment;
      modified = reviewDetailsList[i].Modified;

      for (let index = 0; index < this.modifiedDocumentList.length; index++) {
        if (reviewDetailsList[i].id === this.modifiedDocumentList[index].documentId) {
          for (let aspectIndex = 0; aspectIndex < this.modifiedDocumentList[index].aspect.length; aspectIndex++) {
            aspectStr += this.modifiedDocumentList[index].aspect[aspectIndex].value;
            if (aspectIndex < this.modifiedDocumentList[index].aspect.length) {
              aspectStr += ",";
            }
          }
          aspectStr = new String(aspectStr);
          sentiment = this.modifiedDocumentList[index].sentiment;
          modified = 'yes';
        }
      }

      let aspectList = aspectStr.split(",");
      let aspectDisplayList = [];
      for (var aspectIndex = 0; aspectIndex <= 10; aspectIndex++) {
        if (aspectIndex < aspectList.length) {
          aspectDisplayList.push(aspectList[aspectIndex]);
        }
      }

      this.collection.data.push(
        {
          id: this.rowIndex + i,
          documentId: reviewDetailsList[i].id,
          review: reviewDetailsList[i].Feedback,
          aspect: reviewDetailsList[i].Aspect,
          aspectList: aspectDisplayList,
          rating: reviewDetailsList[i].Rating != undefined ? parseInt(reviewDetailsList[i].Rating) : '',
          sentiment: sentiment,
          edit_aspect_div_id: "edit_aspect_div_id_" + reviewDetailsList[i].id,
          add_aspect_input_box_id: "add_aspect_input_box_id_" + reviewDetailsList[i].id,
          edit_pensil_id: "edit_pensil_id_" + reviewDetailsList[i].id,
          edit_close_id: "edit_close_id_" + reviewDetailsList[i].id,
          aspect_span_id: "aspect_span_id_" + reviewDetailsList[i].id,
          modified: modified != undefined ? "yes" : "no",
          modified_no: modified != undefined ? 1 : 0,
          sentiment_select_id: "sentiment_select_id_" + reviewDetailsList[i].id,
          sentiment_color: this.setSentimentColor(sentiment, "sentiment_select_id_" + reviewDetailsList[i].id)
        }
      );
    }
  }
}

