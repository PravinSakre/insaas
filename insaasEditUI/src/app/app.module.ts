import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HashLocationStrategy, LocationStrategy, DatePipe } from '@angular/common'
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { LoginComponent } from './login/login.component';
import { LoginModel } from './login/login.model';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ServerServicesService } from './server-services.service';
import { AngularWebStorageModule } from 'angular-web-storage';
import { MenuComponent } from './menu/menu.component';
import { ChangePasswordComponent } from './menu/change-password/change-password.component';
import { ForgotPasswordComponent } from './login/forgot-password/forgot-password.component';
import { MenuServices } from './menu/menu.services';
import { AddForgotPasswordComponent } from './login/forgot-password/add-forgot-password/add-forgot-password.component';
import { SearchComponent } from './search/search.component';
import { ReviewsComponent } from './reviews/reviews.component';
import { KeyphrasesComponent } from './keyphrases/keyphrases.component';
import { SentimentsComponent } from './sentiments/sentiments.component';
import { CategoriesComponent } from './categories/categories.component';
import { EntitiesComponent } from './entities/entities.component';
import { SyntaxComponent } from './syntax/syntax.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { AgGridModule } from 'ag-grid-angular';
import { TooltipComponent } from './tooltip/tooltip.component';
import { TableComponent } from '../app/table/table.component';
//import { MomentModule } from 'ngx-moment';
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    MenuComponent,
    ChangePasswordComponent,
    ForgotPasswordComponent,
    AddForgotPasswordComponent,
    SearchComponent,
    ReviewsComponent,
    KeyphrasesComponent,
    SentimentsComponent,
    CategoriesComponent,
    EntitiesComponent,
    SyntaxComponent,
    HeaderComponent,
    FooterComponent,
    TooltipComponent,
    TableComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    AngularWebStorageModule,
    NgxPaginationModule,
    AgGridModule

  ],
  entryComponents: [
    TooltipComponent
  ],
  providers: [ServerServicesService, MenuServices, DatePipe, { provide: LocationStrategy, useClass: HashLocationStrategy }],
  bootstrap: [AppComponent]
})
export class AppModule { }
