import { Component, OnInit } from '@angular/core';
// import { ITooltipAngularComp } from '@ag-grid-community/angular';


@Component({
  selector: 'app-tooltip',
  templateUrl: './tooltip.component.html',
  styleUrls: ['./tooltip.component.css']
})
export class TooltipComponent implements OnInit {
  params: any;
  data: any;

  agInit(params): void {
    this.params = params;
    this.data = params.api.getDisplayedRowAtIndex(params.rowIndex).data;
    // this.data = "test pop" //params.api.getDisplayedRowAtIndex(params.rowIndex).data;
    this.data.color = this.params.color || 'white';
    // this.data.color = 'white';
  }
  ngOnInit(){

  }
}
