import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs'; 
import { environment } from './../environments/environment';
import { Constant } from './constant';
import 'rxjs';

@Injectable()
export class ServerServicesService {
    site: string = Constant.site;
    url: string = this.site+ 'login/authenticate'; 
    queryParams:string='';
    constructor(private http: HttpClient) {
    }

    ngOnInit() {
     }

    doLogin(userName: string, userPassword: string):Observable<any> {
        const formData: FormData = new FormData();
        formData.append('userName', userName);
        formData.append('password', userPassword);
        alert(this.url);
        return this.http.post(this.url, formData);
    }

    getUserList(accessToken: string):Observable<any> {
        
        return 
    }
    onChangeFilterValue(queryParams: any):Observable<any> {
        const formData: FormData = new FormData();
        formData.append('queryParams', queryParams);
        alert("queryParams="+queryParams);
        let url1=this.site+"queryBuilder/queryData";
        alert("url1="+url1);
        return this.http.post(url1,formData);
    }
}

