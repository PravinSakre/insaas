import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ChangePasswordComponent } from './menu/change-password/change-password.component';
import { ForgotPasswordComponent } from './login/forgot-password/forgot-password.component';
import { AddForgotPasswordComponent } from './login/forgot-password/add-forgot-password/add-forgot-password.component';
import { SearchComponent } from './search/search.component';
import { ReviewsComponent } from './reviews/reviews.component';
import { KeyphrasesComponent } from './keyphrases/keyphrases.component';
import { SentimentsComponent } from './sentiments/sentiments.component';
import { CategoriesComponent } from './categories/categories.component';
import { EntitiesComponent } from './entities/entities.component';
import { SyntaxComponent } from './syntax/syntax.component';
import { CategoriesService } from './categories/categories.service';
import { DashboardService } from './dashboard/dashboard.service';
import { EntitiesService } from './entities/entities.service';
import { KeyphrasesService } from './keyphrases/keyphrases.service';
import { ReviewsService } from './reviews/reviews.service';
import { SearchService } from './search/search.service';
import { SentimentsService } from './sentiments/sentiments.service';
import { SyntaxService } from './syntax/syntax.service';
import { TableComponent } from '../app/table/table.component';

const routes: Routes = [
  { path: '', redirectTo: '/edit/:userId', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'table', component: TableComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: '***', component: LoginComponent },
  { path: 'changePassword', component: ChangePasswordComponent },
  { path: 'forgotPassword', component: ForgotPasswordComponent },
  { path: 'addForgotPassword/:accessToken', component: AddForgotPasswordComponent },
  // { path: 'edit', component: SearchComponent },
  { path: 'edit/:userId', component: SearchComponent},
  { path: 'reviews', component: ReviewsComponent },
  { path: 'keyphrases', component: KeyphrasesComponent },
  { path: 'sentiments', component: SentimentsComponent },
  { path: 'categories', component: CategoriesComponent },
  { path: 'entities', component: EntitiesComponent },
  { path: 'syntax', component: SyntaxComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
