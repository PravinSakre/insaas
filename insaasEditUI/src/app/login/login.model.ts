export class LoginModel {
    accessMenuNames: string[];
    accessToken: string;
    userRole: string;
    userId:string;
    defaultScreen:string;
}