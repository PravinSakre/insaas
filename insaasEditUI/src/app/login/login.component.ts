import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServerServicesService } from '../server-services.service';
import { LoginModel } from './login.model';
import { SessionStorageService, SessionStorage } from 'angular-web-storage';
import 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';
import { Constant } from './../constant'; 
import { interval } from 'rxjs/';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit { 
  userName: string;
  userPassword: string;
  errorMessage: string;
  msgUserName:string;
  msgPassword:string;
  user={
    userName: '',
    userPassword:''

  }
 
  constructor(
               private serverService: ServerServicesService, 
               private route: Router,
               private sessionStorageService: SessionStorageService
             ) { }
  ngOnInit() {
   
  }

  doLogin(user:any) {
    this.route.navigate(['dashboard']);
    //alert("user="+JSON.stringify(user));
    //alert("password="+user.password);
     if (!this.validationOnLogin(user)){
       return;
    }
    
    this.serverService.doLogin(user.userName, user.userPassword).
    subscribe(
               (loginModel: LoginModel) => 
               {
              //  alert("loginModel="+JSON.stringify(loginModel));
                 console.log(loginModel.accessMenuNames)
                  this.sessionStorageService.set('loginModel', loginModel);
                //  alert("loginModel.userRole="+loginModel.userRole);
                  this.route.navigate(['dashboard']);
                  
                  
               }, (error: HttpErrorResponse) => {
                 console.error('login error :: '+ error.error.message + ' :: '+ error.status);
                 switch (error.status) {
                  case 422:
                    this.errorMessage = error.error.message;
                    const source = interval(5000);
                    const subscribe = source.subscribe(() => {
                      this.route.navigate(['login']);
                      this.sessionStorageService.clear();
                      window.location.reload();
                    });
                  default:
                    this.errorMessage = error.error.message;
                }
               }
             ); 
  }

  // temporory user role routing
 
  onChangePassword(passwordValue:string){
    if(''!=passwordValue)
    {
      this.msgPassword="";
    }
  }
  onChangeUserName(userNameValue:string){
    if(''!=userNameValue)
    {
      this.msgUserName="";
    }
  }
  validationOnLogin(user){
    let retVal =  true;
    if(user == undefined || ""== user.userName){
       this.msgUserName= "Please enter valid User Name";
      retVal = false;
   }
   
    if(user == undefined || "" == user.userPassword){
      this.msgPassword= "Invalid Username or Password";
      retVal = false;
    }
   
    return retVal;
  }
}
