import { Component, OnInit, ANALYZE_FOR_ENTRY_COMPONENTS } from '@angular/core';
import { Router } from '@angular/router';
import { SessionStorageService, SessionStorage } from 'angular-web-storage';
import { HttpErrorResponse } from '@angular/common/http';
import {LoginModel} from '../../../login/login.model';
import { MenuServices } from '../../../menu/menu.services';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-add-forgot-password',
  templateUrl: './add-forgot-password.component.html',
  styleUrls: ['./add-forgot-password.component.css']
})
export class AddForgotPasswordComponent implements OnInit {
 
  email='';
  password='';
  confirmPassword='';
  webSite:any;
  token:any;
  msgAddPass='';
  constructor( private route: Router,private menuServices:MenuServices,private sessionStorageService:SessionStorageService ,private activateRoute:ActivatedRoute) { }

  ngOnInit() {
  }
  addForgotPassword(password,confirmPassword)
  {
    if(password==confirmPassword)
    {
       this.webSite= this.activateRoute.params.subscribe(params => {
        this.token = params['accessToken']; 
  
     });
      this.menuServices.addForgotPassword(this.token,password).
      subscribe
      (
        (response) => 
        {
          this.msgAddPass=response.Result;
            this.route.navigate(['login']);
        },
       
         (error: HttpErrorResponse) => {
          this.msgAddPass= error.error.message
           console.error('error :: '+ error.error.message + ' :: '+ error.status);
         
         }
         
     
      );
    }
    else{
     this.msgAddPass="Both passwords should be same...";
    }
 
  }
}
