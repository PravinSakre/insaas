import { Component, OnInit, ANALYZE_FOR_ENTRY_COMPONENTS } from '@angular/core';
import { Router } from '@angular/router';
import { SessionStorageService, SessionStorage } from 'angular-web-storage';
import { HttpErrorResponse } from '@angular/common/http';
import {LoginModel} from '../../login/login.model';
import { MenuServices } from '../../menu/menu.services';



@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {
 
  email='';
  msgForgot='';
  constructor( private route: Router,private menuServices:MenuServices,private sessionStorageService:SessionStorageService) { }

  ngOnInit() {
    
    
  }
 
  
  forgotPassword(email)
  {
      this.menuServices.forgotPassword(email).
      subscribe
      (
        (response) => 
        {
          this.msgForgot=response.Result;
          //alert(response.Result);
         // alert("Adding password url is sent to your email...");
          let loginModel: LoginModel;
          loginModel.accessToken=response.accessToken;
        //  alert("accessToken="+loginModel.accessToken);
         this.sessionStorageService.set('loginModel', loginModel);
        },
        //error => console.error(error)
         (error: HttpErrorResponse) => {
          this.msgForgot=error.error.message;
           console.error('error :: '+ error.error.message + ' :: '+ error.status);
           //this.errorMessage=error.error.message;
         }
         
      // error => console.error('login error :: '+ error.error.message + ' :: '+ error.status)
        
      );
    }
  

}
