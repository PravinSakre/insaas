import { Component, OnInit } from '@angular/core';
import { ServerServicesService } from './../server-services.service';
import { HttpErrorResponse } from '@angular/common/http';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  queryParams:string="";
  msgForgot:string="";
  constructor(private serverServicesService:ServerServicesService) {

   }

  ngOnInit() {
  }
 
  onChangeFilterValue(queryParams)
  {

  this.serverServicesService.onChangeFilterValue(queryParams).
      subscribe
      (
        (response) => 
        {
          alert("response="+JSON.stringify(response));
         console.log("response=="+JSON.stringify(response));
        },
        //error => console.error(error)
         (error: HttpErrorResponse) => {
       //   this.msgForgot=error.error.message;
         //  console.error('error :: '+ error.error.message + ' :: '+ error.status);
           //this.errorMessage=error.error.message;
         }
         
      // error => console.error('login error :: '+ error.error.message + ' :: '+ error.status)
        
      );
    }
}
