import { TestBed } from '@angular/core/testing';

import { KeyphrasesService } from './keyphrases.service';

describe('KeyphrasesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: KeyphrasesService = TestBed.get(KeyphrasesService);
    expect(service).toBeTruthy();
  });
});
