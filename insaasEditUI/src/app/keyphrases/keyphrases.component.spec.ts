import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KeyphrasesComponent } from './keyphrases.component';

describe('KeyphrasesComponent', () => {
  let component: KeyphrasesComponent;
  let fixture: ComponentFixture<KeyphrasesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KeyphrasesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KeyphrasesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
