import { TestBed } from '@angular/core/testing';

import { SyntaxService } from './syntax.service';

describe('SyntaxService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SyntaxService = TestBed.get(SyntaxService);
    expect(service).toBeTruthy();
  });
});
