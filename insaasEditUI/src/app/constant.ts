export const Constant : any = {
    'site' : 'http://services.insaas.ai/app/',
    // var host = "services.insaas.ai/app/";
    // 'site' : 'http://192.168.43.68:9090/',
    'solrSearch': 'solrController/solrSearch',
    'searchFeedback': 'solrController/searchFeedback',
    'updateDocument':'solrController/updateDocument',
    'massDocumentUpdate':'solrController/massDocumentUpdate',
    'sessionExpireErrorMessage': 'Session Expired..',
    'requestFailureErrorMessage' : 'Failed to service please contact administrator..'
}