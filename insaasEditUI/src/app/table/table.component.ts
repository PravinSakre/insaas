import { Component, OnInit, Input } from '@angular/core';
@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {
  // @Input() columnDefs;
  // @Input() rowData;

  constructor() {
    // alert("table :: "+ this.testParentcount);
   }
   
  columnDefs = [
    { headerName: 'Make', field: 'make', sortable: true },
    { headerName: 'Model', field: 'model' },
    { headerName: 'Price', field: 'price' }
  ];

  rowData : any = [
    { make: 'Toyota', model: 'Celica', price: 35000 },
    { make: 'Ford', model: 'Mondeo', price: 32000 },
    { make: 'Porsche', model: 'Boxter', price: 72000 }
  ];
  ngOnInit() {
  }

}
