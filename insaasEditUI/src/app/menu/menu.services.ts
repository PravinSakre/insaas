import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import 'rxjs';
import { LoginModel } from '../login/login.model';
import { formatDate } from '@angular/common';
import { Constant } from '../constant';


@Injectable()

export class MenuServices 
{
    site: string = Constant.site;;
    constructor(private http: HttpClient) { }
   
    ngOnInit() {
     }

     changePassword(accessToken:string,password:any,oldPassword:any): Observable<any> {
        // alert("in add vehicle service="+JSON.stringify(vehicleMaster));
          let url: string = this.site + 'login/changePassword'+'?accessToken='+ accessToken + '&password=' + password + '&oldPassword=' + oldPassword;
          return this.http.get(url);
      }
      forgotPassword(email:any): Observable<any> {
        // alert("in add vehicle service="+JSON.stringify(vehicleMaster));
          let url: string = this.site + 'login/forgotPassword'+'?email='+ email;
          return this.http.get(url);
      }
      addForgotPassword(accessToken:any,password:any): Observable<any> {
        // alert("in add vehicle service="+JSON.stringify(vehicleMaster));
          let url: string = this.site + 'login/addForgotPassword'+'?accessToken='+ accessToken + '&password=' + password;
          return this.http.get(url);
      }
 } 