import { Component, OnInit } from '@angular/core';
import { LoginModel } from '../login/login.model';
import { SessionStorageService, SessionStorage } from 'angular-web-storage';
import { Router } from '@angular/router';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { MenuServices } from './menu.services';
@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
  Username:string;
  menuList=[];
  constructor(private sessionStorageService: SessionStorageService, private route: Router) { }

  ngOnInit() {
    let loginModel: LoginModel
    
    loginModel = this.sessionStorageService.get('loginModel');
    //alert("loginModel="+loginModel);
    this.Username=loginModel.userId;
    this.menuList = loginModel.accessMenuNames;
   // alert("this.menuList="+JSON.stringify(this.menuList));
  }
  logout(){
    this.sessionStorageService.remove('loginModel');
    this.route.navigate(['login']);
  }
}
