import { Component, OnInit, ANALYZE_FOR_ENTRY_COMPONENTS } from '@angular/core';
import { Router } from '@angular/router';
import { SessionStorageService, SessionStorage } from 'angular-web-storage';
import { HttpErrorResponse } from '@angular/common/http';
import {LoginModel} from '../../login/login.model';
import { MenuServices } from '../../menu/menu.services';



@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {
  accessToken: string;
  newPassword = "";
  confirmPassword = "";
  password="";
  oldPassword = "";
  constructor(private sessionStorageService: SessionStorageService, private route: Router,private menuServices:MenuServices) { }

  ngOnInit() {
    let loginModel: LoginModel
    loginModel = this.sessionStorageService.get('loginModel');
    this.accessToken = loginModel.accessToken.split(':')[1];
    
    
  }

  changePassword(oldPassword,newPassword,confirmPassword)
  {
//alert("in change password...");
    if(newPassword==confirmPassword)
    {
     // alert("pasword changing....");
      this.menuServices.changePassword(this.accessToken,newPassword,oldPassword).
      subscribe
      (
        (response) => 
        {
         // alert(response);
          if(response.result=="PASSWORD CHANGED SUCCESSFULLY...")
          {
            this.route.navigate(['login']);
          }
        },
        //error => console.error(error)
         (error: HttpErrorResponse) => {
           console.error('error :: '+ error.error.message + ' :: '+ error.status);
           //this.errorMessage=error.error.message;
         }
         
      // error => console.error('login error :: '+ error.error.message + ' :: '+ error.status)
        
      );
    }
  }

}
