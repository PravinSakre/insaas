define([
    'angular',
    'underscore',
    'config'
],
    function (angular, _, config) {
        'use strict';

        var module = angular.module('kibana.controllers');

        module.controller('customerDetails', function ($scope,RESOURCES, $http, timer, dashboard, alertSrv) {
            var self = this;
            $scope.screenURL='';

            $scope.msgCustomerName='';
         
            
            // Solr and Fusion uses different field names for their schema.
            // Solr uses banana-int collection, and Fusion uses system_banana collection.
            self.TITLE_FIELD = 'title';
            self.DASHBOARD_FIELD = 'dashboard';
            self.USER_FIELD = 'user';
            self.GROUP_FIELD = 'group';
            $scope.isLogin = false;
           

            // NOTES: Fusion uses Blob Store API now, so it does not need TITLE_FIELD for querying dashboards.
            // If USE_FUSION, change the schema field names and banana_index setting.
            // Also, get the login username and store it.
            if (config.USE_FUSION) {
                config.banana_index = 'system_banana';
                self.TITLE_FIELD = 'banana_title_s';
                self.DASHBOARD_FIELD = 'banana_dashboard_s';
                self.USER_FIELD = 'banana_user_s';
                self.GROUP_FIELD = 'banana_group_s';
            }
            
            $scope.init = function () {
              
                $scope.gist_pattern = /(^\d{5,}$)|(^[a-z0-9]{10,}$)|(gist.github.com(\/*.*)\/[a-z0-9]{5,}\/*$)/;
                $scope.gist = $scope.gist || {};
                $scope.elasticsearch = $scope.elasticsearch || {};
                $scope.resetNewDefaults();
               // $scope.customerData= $scope.getCustomerData();
               
                $scope.loadMenu = {
                    currentPage: 1,    // Current active page in the pager.
                    firstPageShown: 1, // First page number that is shown in the pager.
                    lastPageShown: 5,  // Last page number that is shown in the pager.
                    totalPages: 5,     // total number of pages needed to display all saved dashboards:
                    // = Math.ceil(total_num_of_saved_dashboards / dashboard.current.loader.load_elasticsearch_size)
                    maxShownPages: 5,  // Hard coded value. The maximum number of pages to be shown in the pager.
                    pages: [],  // Example pages obj => {offset: 0, number: 1, state: 'active'}
                    backwardButtonState: 'disabled',
                    forwardButtonState: 'disabled'
                };

                $scope.elasticsearch.query = '';  // query for filtering the dashboard list 
            };

         
            $scope.getCustomerData=function(){
               // alert("in getUser vfrom customer details.. ");
                $http({
                    url : RESOURCES.GET_CUSTOMER_DATA,
                    method : 'GET',
                 //   params : params,
                    headers : {
                        'Authorization' : 'Bearer'
                    }
                }).then(
                        function(response) {
                            console.log("json=" +JSON.stringify(response.data));

                             $scope.customerData = response.data;
                          //  alert("response="+$scope.customerData);

                        },
                        function(error) {
                            $('html, body').css('cursor', 'default');
                            alert("Error : " + error.data.error + ","
                                    + error.data.exception + "," + error.data.message);
                        });
                    };
                
             
            $scope.scrapper = function scrapper(custDetailsId) {
               // alert("scrapper..");
               var params = {};
               params.custDetailsId = custDetailsId;
               $http({
                   url: RESOURCES.SCRAPPER+"?custDetailsId="+custDetailsId,
                    method: 'GET',
                    //   params : params,
                    headers: {
                        'Authorization': 'Bearer'
                    }
                }).then(
                    function (response) {
                       // alert("Scapped successfully");
                       // alert("response=" + JSON.stringify(response.data));
                    },
                    function (error) {
                        $('html, body').css('cursor', 'default');
                        alert("Error : " + error.data.error + ","
                            + error.data.exception + "," + error.data.message);
                    });
            };
            $scope.clearCoreData = function clearCoreData(custDetailsId) {
                // alert("scrapper..");
                var params = {};
                params.custDetailsId = custDetailsId;
                $http({
                    url: RESOURCES.CLEAR_CORE+"?custDetailsId="+custDetailsId,
                     method: 'GET',
                     //   params : params,
                     headers: {
                         'Authorization': 'Bearer'
                     }
                 }).then(
                     function (response) {
                        // alert("Scapped successfully");
                        // alert("response=" + JSON.stringify(response.data));
                     },
                     function (error) {
                         $('html, body').css('cursor', 'default');
                         alert("Error : " + error.data.error + ","
                             + error.data.exception + "," + error.data.message);
                     });
             };
             $scope.addCustomer = function(customer) {
                alert("add customer ="+JSON.stringify(customer));
                var params = {};
                params.customerDto = customer;
                $http({
                    url: RESOURCES.ADD_CUSTOMER_DATA,
                     method: 'POST',
                     data : params.customerDto,
                     headers: {
                         'Authorization': 'Bearer'
                     }
                 }).then(
                     function (response) {
                        alert("data added successfully...");
                     },
                     function (error) {
                         $('html, body').css('cursor', 'default');
                         alert("Error : " + error.data.error + ","
                             + error.data.exception + "," + error.data.message);
                     });
             };
             $scope.updateCustomerData = function updateCustomerData(customer) {
                // alert("scrapper..");
                var params = {};
                params.customerDto = customer;
                $http({
                    url: RESOURCES.UPDATE_CUSTOMER_DATA,
                     method: 'POST',
                     params : params,
                     headers: {
                         'Authorization': 'Bearer'
                     }
                 }).then(
                     function (response) {
                        alert("data added successfully...");
                     },
                     function (error) {
                         $('html, body').css('cursor', 'default');
                         alert("Error : " + error.data.error + ","
                             + error.data.exception + "," + error.data.message);
                     });
             };
 
 
        });
    });

